package app;

import java.awt.EventQueue;

import common.MainUI;

public class Main {
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainUI.getInstance().setView(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
