package entity;

public class ChiTietKiHoc {
	private int kihoc;
	private long giaTienTinChi;

	public int getKihoc() {
		return kihoc;
	}

	public void setKihoc(int kihoc) {
		this.kihoc = kihoc;
	}

	public long getGiaTienTinChi() {
		return giaTienTinChi;
	}

	public void setGiaTienTinChi(long giaTienTinChi) {
		this.giaTienTinChi = giaTienTinChi;
	}

}
