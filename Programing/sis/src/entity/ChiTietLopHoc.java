package entity;

import common.DiemHelper;

/**
 * Class này để quản lý thông tin về chi tiết lớp học của lớp học
 * 
 * @author TuanNguyen
 *
 */
public class ChiTietLopHoc {

	/** mã sinh viên */
	private String maSinhVien;

	/** tên sinh viên tương ứng */
	private String tenSinhVien;

	/** điểm quá trình môn học */
	private float diemQt;

	/** điểm thi môn học */
	private float diemThi;

	/** điểm chữ môn học */
	private String diemChu;

	public ChiTietLopHoc() {

	}

	public ChiTietLopHoc(String maSinhVien, String tenSinhVien, float diemQt, float diemThi) {
		super();
		this.maSinhVien = maSinhVien;
		this.tenSinhVien = tenSinhVien;
		this.diemQt = diemQt;
		this.diemThi = diemThi;

	}

	/**
	 * Hàm này để gán điểm chữ môn học
	 * 
	 * @param trongSo
	 *            là trọng số của môn học
	 */
	public void setDiemChu(float trongSo) {
		float diemSo = this.diemQt * (1 - trongSo) + this.diemThi * trongSo;
		this.diemChu = DiemHelper.parseToDiemChu(diemSo);
	}

	/**
	 * Hàm này để lấy mã sinh viên
	 * 
	 * @return mã sinh viên
	 */
	public String getMaSinhVien() {
		return maSinhVien;
	}

	/**
	 * Hàm này để gán mã sinh viên
	 * 
	 * @param maSinhVien
	 *            là mã sinh viên
	 */
	public void setMaSinhVien(String maSinhVien) {
		this.maSinhVien = maSinhVien;
	}

	/**
	 * Hàm này để lấy tên sinh viên
	 * 
	 * @return tên sinh viên
	 */
	public String getTenSinhVien() {
		return tenSinhVien;
	}

	/**
	 * Hàm này để gán tên sinh viên
	 * 
	 * @param tenSinhVien
	 */
	public void setTenSinhVien(String tenSinhVien) {
		this.tenSinhVien = tenSinhVien;
	}

	/**
	 * Hàm này để lấy điểm quá trình
	 * 
	 * @return điểm quá trình
	 */
	public float getDiemQt() {
		return diemQt;
	}

	/**
	 * Hàm này để gán điểm quá trình
	 * 
	 * @param diemQt
	 *            là điểm quá trình
	 */
	public void setDiemQt(float diemQt) {
		this.diemQt = diemQt;
	}

	/**
	 * Hàm này để lấy điểm thi
	 * 
	 * @return điểm thi
	 */
	public float getDiemThi() {
		return diemThi;
	}

	/**
	 * Hàm này để gán điểm thi
	 * 
	 * @param diemThi
	 *            là điểm thi
	 */
	public void setDiemThi(float diemThi) {
		this.diemThi = diemThi;
	}

	/**
	 * Hàm này để lấy điểm chữ
	 * 
	 * @return điểm chữ
	 */
	public String getDiemChu() {
		return diemChu;
	}

	/**
	 * Hàm này để gán điểm chữ
	 * 
	 * @param diemChu
	 *            là điểm chữ
	 */
	public void setDiemChu(String diemChu) {
		this.diemChu = diemChu != null ? diemChu : "";
	}
}
