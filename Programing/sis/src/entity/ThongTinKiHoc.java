package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import common.ConnectionHelper;

/**
 * Class để quản lý thông tin của kì học trong cơ sở dữ liệu
 * 
 * @author TuanNguyen
 *
 */
public class ThongTinKiHoc {

	/** kì học tương ứng */
	private int kihoc;

	/** giá tiền tín chỉ tương ứng */
	private long giaTienTinChi;

	/** câu lệnh sql để lấy thong tin kì học theo thứ tự giảm dần */
	private final String SEARCH_KIHOC = "select * from thongtinkihoc order by kihoc desc";

	/** câu lệnh sql để cập nhật giá tiền tín chỉ theo kì học */
	private final String UPDATE_HOCPHI = "update thongtinkihoc set hocphi = ? where kihoc = ?";

	/** câu lệnh sql để thêm học phí theo kì học */
	private final String ADD_HOCPHI = "insert into thongtinkihoc (kihoc, hocphi) values (?, ?)";

	/** câu lệnh sql để lấy thông tin của 1 kì học */
	private final String GET_ONE_KIHOC = "select * from thongtinkihoc where kihoc = ?";

	/**
	 * Hàm này lấy thông tin của các kì học
	 * 
	 * @return thông tin các kì học
	 * @throws SQLException
	 */
	public List<ThongTinKiHoc> getKiHoc() throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SEARCH_KIHOC);
		List<ThongTinKiHoc> kihocs = new ArrayList<>();
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			ThongTinKiHoc chitiet = new ThongTinKiHoc();
			chitiet.setKihoc(rs.getInt("kihoc"));
			chitiet.setGiaTienTinChi(rs.getLong("hocphi"));
			kihocs.add(chitiet);
		}

		ConnectionHelper.closeDB();
		return kihocs;
	}

	/**
	 * Hàm này lấy thông tin của 1 kì học
	 * 
	 * @param kihoc
	 *            cần lấy thông tin
	 * @return thông tin kì học
	 * @throws SQLException
	 */
	public ThongTinKiHoc getOne(int kihoc) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(GET_ONE_KIHOC);
		ps.setInt(1, kihoc);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			ThongTinKiHoc thongTinKiHoc = new ThongTinKiHoc();
			thongTinKiHoc.setKihoc(rs.getInt("kihoc"));
			thongTinKiHoc.setGiaTienTinChi(rs.getLong("hocphi"));
			ConnectionHelper.closeDB();
			return thongTinKiHoc;
		}
		ConnectionHelper.closeDB();
		return null;
	}

	/**
	 * Hàm này cập nhật học phí kì học
	 * 
	 * @param kihoc
	 *            là kì học cần cập nhật
	 * @param hocPhi
	 *            là học phí cập nhật
	 * @return true nếu cập nhật thành công và ngược lại
	 */
	public boolean updateHocPhi(int kihoc, long hocPhi) {
		try {
			Connection conn = ConnectionHelper.connectDb();
			PreparedStatement ptmt = conn.prepareStatement(UPDATE_HOCPHI);
			ptmt.setLong(1, hocPhi);
			ptmt.setInt(2, kihoc);
			int result = ptmt.executeUpdate();
			ConnectionHelper.closeDB();
			if (result > 0)
				return true;
		} catch (SQLException e) {
			return false;
		}
		return false;
	}

	/**
	 * Hàm này để thêm mới học phí
	 * 
	 * @param kihoc
	 *            cần thêm mới
	 * @param hocPhi
	 *            thêm mới
	 * @return true nếu thành công và ngược lại
	 */
	public boolean insertHocPhi(int kihoc, long hocPhi) {
		try {
			Connection conn = ConnectionHelper.connectDb();
			PreparedStatement ptmt = conn.prepareStatement(ADD_HOCPHI);
			ptmt.setInt(1, kihoc);
			ptmt.setLong(2, hocPhi);
			int result = ptmt.executeUpdate();
			ConnectionHelper.closeDB();
			if (result > 0)
				return true;
		} catch (SQLException e) {
			return false;
		}
		return false;
	}

	/**
	 * Hàm này lấy kì học
	 * 
	 * @return kì học
	 */
	public int getKihoc() {
		return kihoc;
	}

	/**
	 * Hàm này gán kì học
	 * 
	 * @param kihoc
	 *            là kì học
	 */
	public void setKihoc(int kihoc) {
		this.kihoc = kihoc;
	}

	/**
	 * Hàm này lấy giá tiền tín chỉ
	 * 
	 * @return giá tiền tín chỉ
	 */
	public long getGiaTienTinChi() {
		return giaTienTinChi;
	}

	/**
	 * Hàm này gán giá tiền tín chỉ
	 * 
	 * @param giaTienTinChi
	 *            là giá tiền tín chỉ
	 */
	public void setGiaTienTinChi(long giaTienTinChi) {
		this.giaTienTinChi = giaTienTinChi;
	}
}
