package entity;

import java.util.*;

import common.ConnectionHelper;

import java.sql.*;

/**
 * Thực hiện các truy vấn với database thông qua đối tượng sinhvien
 * 
 * @author PhamTuan
 *
 */
public class SinhVien {
	private int id;
	/**
	 * Các tham số đầu vào mã tài khoản, họ tên sinh viên, ngày sinh, lớp học,
	 * chương trình học, hệ học trạng thái, email, địa chỉ
	 */
	private String taikhoan, hoten, sdt, ngaysinh, lop, chuongtrinhhoc, hehoc, trangthai, email, diachi;

	private List<SinhVien> chitietList;

	/** Truy vấn thông tin sinh viên theo mã tài khoản */
	private final String SEARCH_USER_INFO = "select * from sis.sinhvien where  taikhoan=?";

	/** Update thông tin sinh viên gồm email, số đt, địa chỉ theo mã tài khoản */
	private final String UPDATE_USER_INFO = " update sis.sinhvien set email =? , sdt = ? , diachi=?"
			+ "where taikhoan =?";

	/** Truy vấn thông tin sinh viên theo họ tên */
	private final String SEARCH_USER_BY_NAME = "select * from sis.sinhvien where hoten=?";

	/**
	 * Lấy thông tin sinh viên theo mã tài khoản
	 * 
	 * @param taikhoan:
	 *            mã tài khoản sinh viên
	 * @return danh sách thông tin sinh viên
	 */
	public SinhVien searchInfoUser(String taikhoan) {

		List<SinhVien> chitietList = new ArrayList<>();
		SinhVien sinhvien = new SinhVien();

		try {
			Connection conn = ConnectionHelper.connectDb();
			PreparedStatement ptmt = conn.prepareStatement(SEARCH_USER_INFO);
			ptmt.setString(1, taikhoan);
			ResultSet rs = ptmt.executeQuery();

			while (rs.next()) {
				String taikhoansv = rs.getString("taikhoan");
				String sdt = rs.getString("sdt");
				String hoten = rs.getString("hoten");
				String lop = rs.getString("lop");
				String diachi = rs.getString("diachi");
				String chuongtrinh = rs.getString("chuongtrinhhoc");
				String hehoc = rs.getString("hehoc");
				String ngaysinh = rs.getString("ngaysinh");
				String email = rs.getString("email");
				String trangthai = rs.getString("trangthai");

				sinhvien.setTaikhoan(taikhoansv);
				sinhvien.setSdt(sdt);
				sinhvien.setHoten(hoten);
				sinhvien.setLop(lop);
				sinhvien.setDiachi(diachi);
				sinhvien.setChuongtrinhhoc(chuongtrinh);
				sinhvien.setHehoc(hehoc);
				sinhvien.setNgaysinh(ngaysinh);
				sinhvien.setEmail(email);
				sinhvien.setTrangthai(trangthai);

				chitietList.add(sinhvien);

			}
			sinhvien.setTaikhoan(taikhoan);
			sinhvien.setChitietList(chitietList);
			ConnectionHelper.closeDB();
		} catch (SQLException e) {
			System.out.println("Error search User by taikhoan: " + e);
		}

		return sinhvien;
	}

	/**
	 * Thực hiện update thông tin sinh viên, gồm email, số điện thoại, địa chỉ theo
	 * mã tài khoản
	 * 
	 * @param email
	 * @param sdt:
	 *            số điện thoại
	 * @param diachi:
	 *            địa chỉ
	 * @param taikhoan:
	 *            mã tài khoản
	 * @return true nếu cập nhật thành công, false nếu thất bại
	 */
	public boolean UpdateInfoUser(String email, String sdt, String diachi, String taikhoan) {

		try {
			Connection conn = ConnectionHelper.connectDb();
			PreparedStatement ptmt = conn.prepareStatement(UPDATE_USER_INFO);
			ptmt.setString(1, email);
			ptmt.setString(2, sdt);
			ptmt.setString(3, diachi);
			ptmt.setString(4, taikhoan);
			int result = ptmt.executeUpdate();
			if (result > 0)
				return true;

		} catch (SQLException e) {
			System.out.println("Error updateInfoUser: " + e);
		}

		return false;

	}

	/**
	 * Lấy danh sách thông tin sinh viên theo họ tên
	 * 
	 * @param hoten:
	 *            họ tên sinh viên
	 * @return danh sách thông tin sinh viên
	 */
	public List<SinhVien> getInfoUserbyName(String hoten) {

		List<SinhVien> list = new ArrayList<SinhVien>();

		try {
			Connection conn = ConnectionHelper.connectDb();
			PreparedStatement ptmt = conn.prepareStatement(SEARCH_USER_BY_NAME);
			ptmt.setString(1, hoten);
			ResultSet rs = ptmt.executeQuery();

			while (rs.next()) {
				SinhVien user = new SinhVien();

				String taikhoansv = rs.getString("taikhoan");
				String sdt = rs.getString("sdt");
				String ht = rs.getString("hoten");
				String lop = rs.getString("lop");
				String diachi = rs.getString("diachi");
				String chuongtrinh = rs.getString("chuongtrinhhoc");
				String hehoc = rs.getString("hehoc");
				String ngaysinh = rs.getString("ngaysinh");
				String email = rs.getString("email");
				String trangthai = rs.getString("trangthai");

				user.setTaikhoan(taikhoansv);
				user.setSdt(sdt);
				user.setHoten(ht);
				user.setLop(lop);
				user.setDiachi(diachi);
				user.setChuongtrinhhoc(chuongtrinh);
				user.setHehoc(hehoc);
				user.setNgaysinh(ngaysinh);
				user.setEmail(email);
				user.setTrangthai(trangthai);

				list.add(user);

			}
		} catch (SQLException e) {
			System.out.println("Error search User by name: " + e);
		}

		return list;
	}

	/**
	 * Lấy họ tên sinh viên theo mã tài khoản
	 * 
	 * @param taikhoan:
	 *            mã tài khoản sinh viên
	 * @return họ tên sinh viên
	 */
	public String getNameSV(String taikhoan) {
		String name = "";
		String sql = "select * from sis.sinhvien where taikhoan=? ";

		try {
			Connection conn = ConnectionHelper.connectDb();
			PreparedStatement ptmt = conn.prepareStatement(sql);
			ptmt.setString(1, taikhoan);
			ResultSet rs = ptmt.executeQuery();

			while (rs.next()) {
				name = rs.getString("hoten");
			}
		} catch (SQLException e) {
			System.out.println("Error search name: " + e);
		}

		return name;
	}

	/**
	 * Lấy ngày sinh của sinh viên theo mã tài khoản
	 * 
	 * @param taikhoan:
	 *            mã tài khoản
	 * @return ngày sinh sinh viên
	 */
	public String getNgaySinhSV(String taikhoan) {
		String ngaysinh = "";

		String sql = "select * from sis.sinhvien where taikhoan=? ";

		try {
			Connection conn = ConnectionHelper.connectDb();
			PreparedStatement ptmt = conn.prepareStatement(sql);
			ptmt.setString(1, taikhoan);
			ResultSet rs = ptmt.executeQuery();

			while (rs.next()) {
				ngaysinh = rs.getString("ngaysinh");
			}
		} catch (SQLException e) {
			System.out.println("Error search ngaysinh: " + e);
		}

		return ngaysinh;
	}

	/**
	 * Lấy id của sinh viên
	 * 
	 * @return id sinh viên
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gán id cho sinh viên
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Lấy thông tin số điện thoại sinh viên
	 * 
	 * @return số điện thoại
	 */
	public String getSdt() {
		return sdt;
	}

	/**
	 * Gán giá trị số điện thoại cho sinh viên
	 * 
	 * @param sdt:
	 *            số điện thoại
	 */
	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	/**
	 * Lấy thông tin mã tài khoản sinh viên
	 * 
	 * @return mã tài khoản sinh viên
	 */
	public String getTaikhoan() {
		return taikhoan;
	}

	/**
	 * Gán mã tài khoản cho sinh viên
	 * 
	 * @param taikhoan:
	 *            mã tài khoản
	 */
	public void setTaikhoan(String taikhoan) {
		this.taikhoan = taikhoan;
	}

	/**
	 * Lấy họ tên sinh viên
	 * 
	 * @return họ tên sinh viên
	 */
	public String getHoten() {
		return hoten;
	}

	/**
	 * Gán họ tên cho sinh viên
	 * 
	 * @param hoten:
	 *            họ tên sinh viên
	 */
	public void setHoten(String hoten) {
		this.hoten = hoten;
	}

	/**
	 * Lấy ngày sinh của sinh viên
	 * 
	 * @return ngày sinh sinh viên
	 */
	public String getNgaysinh() {
		return ngaysinh;
	}

	/**
	 * Gán ngày sinh cho sinh viên
	 * 
	 * @param ngaysinh:
	 *            ngày sinh sinh viên
	 */
	public void setNgaysinh(String ngaysinh) {
		this.ngaysinh = ngaysinh;
	}

	/**
	 * Lấy thông tin lớp của sinh viên
	 * 
	 * @return lớp sinh viên
	 */
	public String getLop() {
		return lop;
	}

	/**
	 * Gán thông tin lớp sinh viên
	 * 
	 * @param lop:
	 *            lớp sinh viên
	 */
	public void setLop(String lop) {
		this.lop = lop;
	}

	/**
	 * Lấy thông tin chương trình học sinh viên
	 * 
	 * @return chương trình học
	 */
	public String getChuongtrinhhoc() {
		return chuongtrinhhoc;
	}

	/**
	 * Gán thông tin chương trình học sinh viên
	 * 
	 * @param chuongtrinhhoc
	 */
	public void setChuongtrinhhoc(String chuongtrinhhoc) {
		this.chuongtrinhhoc = chuongtrinhhoc;
	}

	/**
	 * Lấy thông tin hệ học sinh viên
	 * 
	 * @return hệ học
	 */
	public String getHehoc() {
		return hehoc;
	}

	/**
	 * Gán thông tin hệ học sinh viên
	 * 
	 * @param hehoc:
	 *            hệ học
	 */
	public void setHehoc(String hehoc) {
		this.hehoc = hehoc;
	}

	/**
	 * Lấy thông tin trạng thái sinh viên
	 * 
	 * @return trạng thái
	 */
	public String getTrangthai() {
		return trangthai;
	}

	/**
	 * Gán trạng thái cho sinh viên
	 * 
	 * @param trangthai:
	 *            trạng thái
	 */
	public void setTrangthai(String trangthai) {
		this.trangthai = trangthai;
	}

	/**
	 * Lấy thông tin email sinh viên
	 * 
	 * @return email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gán thông tin email cho sinh viên
	 * 
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Lấy thông tin địa chỉ sinh viên
	 * 
	 * @return địa chỉ sinh viên
	 */
	public String getDiachi() {
		return diachi;
	}

	/**
	 * Gán địa chỉ sinh viên
	 * 
	 * @param diachi:
	 *            địa chỉ
	 */
	public void setDiachi(String diachi) {
		this.diachi = diachi;
	}

	public List<SinhVien> getChitietList() {
		return chitietList;
	}

	public void setChitietList(List<SinhVien> chitietList) {
		this.chitietList = chitietList;
	}
}
