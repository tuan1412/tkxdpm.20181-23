package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import common.ConnectionHelper;
import common.Role;

/**
 * Class này quản lý các thông tin của user trong cơ sở dữ liệu
 * 
 * @author TuanNguyen
 *
 */
public class User {

	/**
	 * Tài khoản user để đăng nhập, user là sinh viên thì tương ứng mã số sinh viên
	 */
	private String taikhoan;

	/** Mật khẩu tương ứng với user */
	private String matkhau;

	/** Role của user */
	private Role role;

	/** Câu lệnh truy vấn user với tài khoản và mật khẩu */
	private final String SEARCH_USER = "select * from User where taikhoan=? and matkhau=?";

	/** Câu lệnh cập nhật password */
	private final String UPDATE_PASSWORD = "update sis.User set matkhau=? where taikhoan=?";

	/**
	 * Hàm này kiểm tra có tồn tại user với tài khoản và tên đăng nhập tương ứng
	 * không
	 * 
	 * @param taikhoan
	 *            là tài khoản đăng nhập
	 * @param matkhau
	 *            là mật khẩu tương ứng với tài khoản
	 * @return đối tượng User nếu tồn tại, nếu không trả về null
	 * @throws SQLException
	 */
	public User searchUser(String taikhoan, String matkhau) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SEARCH_USER);
		ps.setString(1, taikhoan);
		ps.setString(2, matkhau);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			int role = rs.getInt("role");
			User user = new User();
			user.setTaikhoan(taikhoan);
			user.setMatkhau(matkhau);
			user.setRole(role);
			ConnectionHelper.closeDB();
			return user;
		}
		ConnectionHelper.closeDB();
		return null;
	}

	/**
	 * Hàm này để cập nhật lại mật khẩu khi thay đổi mật khẩu
	 * 
	 * @param taikhoan
	 *            là tài khoản
	 * @param matkhau
	 *            là mật khẩu
	 * @throws SQLException
	 */
	public void updatePassword(String taikhoan, String matkhau) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(UPDATE_PASSWORD);
		ps.setString(1, matkhau);
		ps.setString(2, taikhoan);
		ps.executeUpdate();
	}

	/**
	 * Hàm này để lấy ra tài khoản của User
	 * 
	 * @return tài khoản user
	 */
	public String getTaikhoan() {
		return taikhoan;
	}

	/**
	 * Hàm này để gán tài khoản cho User
	 * 
	 * @param taikhoan
	 *            là tài khoản cần gán
	 */
	public void setTaikhoan(String taikhoan) {
		this.taikhoan = taikhoan;
	}

	/**
	 * Hàm này để lấy mật khẩu của User
	 * 
	 * @return mật khẩu User
	 */
	public String getMatkhau() {
		return matkhau;
	}

	/**
	 * Hàm này để gán mật khẩu cho User
	 * 
	 * @param matkhau
	 *            là mật khẩu cần gán
	 */
	public void setMatkhau(String matkhau) {
		this.matkhau = matkhau;
	}

	/**
	 * Hàm này lấy role cho User
	 * 
	 * @return role của user
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * Hàm này để set role cho User
	 * 
	 * @param role
	 *            là role của User. Role = 1 là Admin. Role = 2 là User
	 */
	public void setRole(int role) {
		if (Role.ADMIN.getRole() == role) {
			this.role = Role.ADMIN;
		} else {
			this.role = Role.SINHVIEN;
		}
	}
}
