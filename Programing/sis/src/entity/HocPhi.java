package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import common.ConnectionHelper;

/**
 * Class này để quản lý thông tin về học phí
 * 
 * @author TuanNguyen
 *
 */
public class HocPhi {

	/** giá tiền một tín chỉ */
	private long giaTienTinChi;

	/** học kì mới nhất của sinh viên */
	private int hocki;

	/** tài khoản (mã số sinh viên) của sinh viên */
	private String taikhoan;

	/** danh sách thông tin các môn học của sinh viên */
	private List<ChiTietHocPhi> chiTietList;

	/** tổng số học phí */
	private long hocphi;

	/** câu lệnh sql để lấy thông tin học phí */
	private final String SEARCH_HOCPHI = "select mssv, mahp, malop, thoiLuong, soTinChi, tinChiHocPhi, thoiLuong, lophoc_view.kihoc, tenhp, hocphi "
			+ "from lophoc_view inner join thongtinkihoc on lophoc_view.kihoc = thongtinkihoc.kihoc "
			+ "where lophoc_view.mssv = ? "
			+ "and lophoc_view.kihoc in (select max(lophoc_view.kihoc) from lophoc_view)";

	/**
	 * Hàm này để lấy thông tin học phí của sinh viên
	 * 
	 * @param taikhoan
	 *            là mã số sinh viên
	 * @return thông tin học phí
	 * @throws SQLException
	 */
	public HocPhi searchHocPhi(String taikhoan) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SEARCH_HOCPHI);
		ps.setString(1, taikhoan);
		HocPhi hocPhi = new HocPhi();
		List<ChiTietHocPhi> chitietList = new ArrayList<>();
		int hocki = 0;
		long giaTienTinChi = 0l;
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {

			ChiTietHocPhi chiTiet = new ChiTietHocPhi();
			chiTiet.setHocphan(rs.getString("tenhp"));
			chiTiet.setMahp(rs.getString("mahp"));
			chiTiet.setMalop(rs.getString("malop"));
			chiTiet.setThoiluong(rs.getString("thoiluong"));
			chiTiet.setSoTinChi(rs.getInt("soTinChi"));
			chiTiet.setTinChiHocPhi(rs.getFloat("tinChiHocPhi"));
			hocki = rs.getInt("kihoc");
			giaTienTinChi = rs.getLong("hocphi");
			chitietList.add(chiTiet);
		}
		hocPhi.setTaikhoan(taikhoan);
		hocPhi.setChitiet(chitietList);
		hocPhi.setHocki(hocki);
		hocPhi.setGiaTienTinChi(giaTienTinChi);
		hocPhi.setHocPhi();
		ConnectionHelper.closeDB();

		return hocPhi;
	}

	/**
	 * Hàm này để tính tổng học phí sinh viên cần đóng
	 */
	public void setHocPhi() {
		float soTinChiHocPhi = chiTietList.stream().map(ChiTietHocPhi::getTinChiHocPhi).reduce(0f, Float::sum);
		this.hocphi = (long) (this.giaTienTinChi * soTinChiHocPhi);
	}

	/**
	 * Hàm này để lấy tổng học phí
	 * 
	 * @return tổng học phí
	 */
	public long getHocphi() {
		return hocphi;
	}

	/**
	 * Hàm này để gán tổng học phí
	 * 
	 * @param hocphi
	 *            là tổng học phí
	 */
	public void setHocphi(long hocphi) {
		this.hocphi = hocphi;
	}

	/**
	 * Hàm này để lấy kì học
	 * 
	 * @return kì học
	 */
	public int getHocki() {
		return hocki;
	}

	/**
	 * Hàm này để gán kì học
	 * 
	 * @param hocki
	 *            là kì học
	 */
	public void setHocki(int hocki) {
		this.hocki = hocki;
	}

	/**
	 * Hàm này để lấy tài khoản (mã số sinh viên)
	 * 
	 * @return tài khoản
	 */
	public String getTaikhoan() {
		return taikhoan;
	}

	/**
	 * Hàm này để gán tài khoản
	 * 
	 * @param taikhoan
	 *            là tài khoản
	 */
	public void setTaikhoan(String taikhoan) {
		this.taikhoan = taikhoan;
	}

	/**
	 * Hàm này để lấy danh sách chi tiết học phí
	 * 
	 * @return danh sách chi tiết
	 */
	public List<ChiTietHocPhi> getChitiet() {
		return chiTietList;
	}

	/**
	 * Hàm này để gán danh sách chi tiết
	 * 
	 * @param chitietList
	 *            là danh sách chi tiết
	 */
	public void setChitiet(List<ChiTietHocPhi> chitietList) {
		this.chiTietList = chitietList;
	}

	/** Hàm này để lấy giá tiền tín chỉ */
	public long getGiaTienTinChi() {
		return giaTienTinChi;
	}

	/** Hàm này để gán giá tiền tín chỉ */
	public void setGiaTienTinChi(long giaTienTinChi) {
		this.giaTienTinChi = giaTienTinChi;
	}
}
