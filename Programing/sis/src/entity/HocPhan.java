package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import common.ConnectionHelper;

/**
 * Class này quản lý các thông tin của học phần trong cơ sở dữ liệu
 * 
 * @author TungNguyen
 *
 */
public class HocPhan {

	/** mã học phần của học phần */
	private String maHP;

	/** tên học phần của học phần */
	private String tenHP;

	/** số tín chỉ của học phần */
	private int soTinChi;

	/** tín chỉ học phí của học phần */
	private float tinChiHocPhi;

	/** thời lượng của học phần */
	private String thoiLuong;

	/** trọng số của học phần */
	private float trongSo;

	/** câu lệnh sql để tìm kiếm thông tin học phần */
	private final String SEARCH_HOCPHAN = "select hocphan.mahp, hocphan.tenhp, hocphan.soTinChi from hocphan where mahp = ?";

	/** câu lệnh sql để lấy mã học phần trong bảng đăng ký học phần */
	private final String SELECT_MAHP = "SELECT mahp FROM sis.hocphan WHERE bangdangkihocphan.mahp = ?";

	/**
	 * Hàm này để lấy thông tin học phần
	 * 
	 * @param maHP
	 *            là mã học phần cần tra cứu
	 * @return thông tin của học phần
	 * @throws SQLException
	 */
	public HocPhan searchHocPhan(String maHP) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SEARCH_HOCPHAN);
		ps.setString(1, maHP);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {

			setMaHP(rs.getString("mahp"));
			setTenHP(rs.getString("tenhp"));
			setSoTinChi(rs.getInt("soTinChi"));
		}
		System.out.println(getMaHP() + " " + getTenHP() + " " + getSoTinChi());
		ConnectionHelper.connectDb();

		return this;
	}

	/**
	 * Hàm này để lấy mã học phần
	 * 
	 * @return thông tin của mã học phần
	 * @throws SQLException
	 */
	public HocPhan selectMaHP() throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SELECT_MAHP);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			setMaHP(rs.getString("mahp"));
			System.out.println(rs.getString("mahp"));
		}
		ConnectionHelper.connectDb();
		return this;
	}

	/**
	 * Hàm này để lấy mã học phần của học phần
	 * 
	 * @return maHP là mã học phần của học phần
	 */
	public String getMaHP() {
		return maHP;
	}

	/**
	 * Hàm này để gán mã học phần của học phần
	 * 
	 * @param maHP
	 *            là mã học phần cần gán
	 */
	public void setMaHP(String maHP) {
		this.maHP = maHP;
	}

	/**
	 * Hàm này để lấy tên học phần của học phần
	 * 
	 * @return tenHP là tên học phần của học phần
	 */
	public String getTenHP() {
		return tenHP;
	}

	/**
	 * Hàm này để gán tên học phần của học phần
	 * 
	 * @param tenHP
	 *            là tên học phần cần gán
	 */
	public void setTenHP(String tenHP) {
		this.tenHP = tenHP;
	}

	/**
	 * Hàm này để lấy số tín chỉ của học phần
	 * 
	 * @return soTinChi là số tín chỉ của học phần
	 */
	public int getSoTinChi() {
		return soTinChi;
	}

	/**
	 * Hàm này để gán số tín chỉ của học phần
	 * 
	 * @param soTinChi
	 *            là số tín chỉ cần gán
	 */
	public void setSoTinChi(int soTinChi) {
		this.soTinChi = soTinChi;
	}

	/**
	 * Hàm này để lấy tín chỉ học phí của học phần
	 * 
	 * @return tinChiHocPhi là tín chỉ học phí của học phần
	 */
	public float getTinChiHocPhi() {
		return tinChiHocPhi;
	}

	/**
	 * Hàm này để gán tín chỉ học phí của học phần
	 * 
	 * @param tinChiHocPhi
	 *            là tín chỉ học phí cần gán
	 */
	public void setTinChiHocPhi(float tinChiHocPhi) {
		this.tinChiHocPhi = tinChiHocPhi;
	}

	/**
	 * Hàm này để lấy thời lượng của học phần
	 * 
	 * @return thoiLuong là thời lượng của học phần
	 */
	public String getThoiLuong() {
		return thoiLuong;
	}

	/**
	 * Hàm này để gán thời lượng của học phần
	 * 
	 * @param thoiLuong
	 *            là thời lượng cần gán
	 */
	public void setThoiLuong(String thoiLuong) {
		this.thoiLuong = thoiLuong;
	}

	/**
	 * Hàm này để lấy trọng số của học phần
	 * 
	 * @return trongSo là trọng số của học phần
	 */
	public float getTrongSo() {
		return trongSo;
	}

	/**
	 * Hàm này để gán trọng số của học phần
	 * 
	 * @param trongSo
	 *            là trọng số cần gán
	 */
	public void setTrongSo(float trongSo) {
		this.trongSo = trongSo;
	}

}
