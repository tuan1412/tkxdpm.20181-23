package entity;

import java.sql.*;

import java.util.*;

import common.ConnectionHelper;
import controller.SinhVienController;

/**
 * Class thực hiện các truy vấn liên quan đến đối tượng Toeic
 * 
 * @author PhamTuan
 *
 */
public class Toeic {

	/**

	 * Tham số đầu vào mã tài khoản, điểm nghe, điểm đọc, điểm tổng, học kì, ngàythi, ghi chú

	 * Tham số đầu vào mã tài khoản, điểm nghe, điểm đọc, điểm tổng, học kì, ngày
	 * thi, ghi chú
	 */
	private int toeicid, taikhoan, diemnghe, diemdoc, diemtong;
	private String hocki, ngaythi, ghichu;

	/** Truy vấn thông tin Toeic theo mã tài khoản, tất cả học kì */
	private final String SEARCH_TOEIC_BY_TK_ALL = " select * from sis.bangdiemtoeic where taikhoan=? ORDER BY hocki";

	/** Truy vấn thông tin Toeic theo mã tài khoản và học kì */
	private final String SEARCH_TOEIC_BY_TK_ONE = " select * from sis.bangdiemtoeic where taikhoan=? and hocki=? ORDER BY hocki";

	/**

	 * Lấy danh sách thông tin Toeic của sinh viên theo mã tài khoản hoặc họ tên sinh viên
	 * 
	 * @param hk: học kì
	 * @param taikhoan:mã tài khoản
	 * @param ht:họ tên sinh viên

	 * Lấy danh sách thông tin Toeic của sinh viên theo mã tài khoản hoặc họ tên
	 * sinh viên
	 * 
	 * @param hk:
	 *            học kì
	 * @param taikhoan:
	 *            mã tài khoản
	 * @param ht:
	 *            họ tên sinh viên

	 * @return danh sách thông tin điểm thi Toeic
	 */
	public List<Toeic> searchToiec(String hk, String taikhoan, String ht) {
		Connection conn = null;
		try {
			conn = ConnectionHelper.connectDb();
		} catch (SQLException e1) {
			System.out.println("Error: " + e1);
		}
		List<Toeic> list = new ArrayList<Toeic>();
		String sql = "";

		// Tìm kiếm tất cả học kì
		if (hk.equals("Tất cả")) {

			// tìm kiến theo tên tài khoản
			if (!taikhoan.equals("") && ht.equals("")) {
				sql = SEARCH_TOEIC_BY_TK_ALL;
			}
			// tìm kiếm theo tên tài khoản + họ tên
			if (!taikhoan.equals("") && !ht.equals("")) {
				// kiểm tra tên tài khoản + họ tên có cùng tồn tại trong table sinhvien
				String hotensinhvien = SinhVienController.getInstance().getNameSV(taikhoan);
				if (ht.matches(hotensinhvien)) {
					sql = SEARCH_TOEIC_BY_TK_ALL;
				}
			}

			// tìm kiếm theo họ tên
			if (taikhoan.equals("") && !ht.equals("")) {
				try {
					List<SinhVien> listByName = SinhVienController.getInstance().getListUserByName(ht);

					for (SinhVien sv : listByName) {

						PreparedStatement ptmt = conn.prepareStatement(SEARCH_TOEIC_BY_TK_ALL);
						ptmt.setString(1, sv.getTaikhoan());

						System.out.println("sql excute:" + ptmt.toString());
						ResultSet rs = ptmt.executeQuery();

						while (rs.next()) {
							Toeic rsToeic = new Toeic();

							int tk = rs.getInt("taikhoan");
							String hocki = rs.getString("hocki");
							String ngaythi = rs.getString("ngaythi");
							int diemnghe = rs.getInt("diemnghe");
							int diemdoc = rs.getInt("diemdoc");
							int diemtong = rs.getInt("diemtong");
							String ghichu = rs.getString("ghichu");

							rsToeic.setTaikhoan(tk);
							rsToeic.setHocki(hocki);
							rsToeic.setNgaythi(ngaythi);
							rsToeic.setDiemnghe(diemnghe);
							rsToeic.setDiemdoc(diemdoc);
							rsToeic.setDiemtong(diemtong);
							rsToeic.setGhichu(ghichu);

							list.add(rsToeic);

						}
					}

					conn.close();

				} catch (SQLException e) {
					System.out.println(sql);
					System.out.println("Error search Toeic: " + e);
				}

				return list;

			}

		}

		// tìm kiếm chỉ 1 học kì duy nhất
		if (!hk.equals("Tất cả")) {
			// tìm kiếm theo tên tài khoản
			if (!taikhoan.equals("") && ht.equals("")) {
				sql = SEARCH_TOEIC_BY_TK_ONE;
			}
			// tìm kiếm theo tên tài khoản + họ tên
			if (!taikhoan.equals("") && !ht.equals("")) {

				// kiểm tra tên tài khoản + họ tên có cùng tồn tại trong table sinhvien
				String hotensinhvien = SinhVienController.getInstance().getNameSV(taikhoan);
				if (ht.matches(hotensinhvien)) {
					sql = SEARCH_TOEIC_BY_TK_ONE;
				}

			}
			// tìm kiếm theo họ tên
			if (taikhoan.equals("") && !ht.equals("")) {
				try {

					List<SinhVien> listByName = SinhVienController.getInstance().getListUserByName(ht);

					for (SinhVien sv : listByName) {

						PreparedStatement ptmt = conn.prepareStatement(SEARCH_TOEIC_BY_TK_ONE);
						ptmt.setString(1, sv.getTaikhoan());
						ptmt.setString(2, hk);

						System.out.println("sql excute:" + ptmt.toString());

						ResultSet rs = ptmt.executeQuery();

						while (rs.next()) {
							Toeic rsToeic = new Toeic();

							int tk = rs.getInt("taikhoan");
							String hocki = rs.getString("hocki");
							String ngaythi = rs.getString("ngaythi");
							int diemnghe = rs.getInt("diemnghe");
							int diemdoc = rs.getInt("diemdoc");
							int diemtong = rs.getInt("diemtong");
							String ghichu = rs.getString("ghichu");

							rsToeic.setTaikhoan(tk);
							rsToeic.setHocki(hocki);
							rsToeic.setNgaythi(ngaythi);
							rsToeic.setDiemnghe(diemnghe);
							rsToeic.setDiemdoc(diemdoc);
							rsToeic.setDiemtong(diemtong);
							rsToeic.setGhichu(ghichu);

							list.add(rsToeic);

						}
					}

					conn.close();

				} catch (SQLException e) {
					System.out.println(sql);
					System.out.println("Error search Toeic: " + e);
				}

				return list;

			}

		}

		if (sql.equals(SEARCH_TOEIC_BY_TK_ONE)) {
			try {

				PreparedStatement ptmt = conn.prepareStatement(SEARCH_TOEIC_BY_TK_ONE);
				ptmt.setString(1, taikhoan);
				ptmt.setString(2, hk);

				System.out.println("sql excute:" + ptmt.toString());

				ResultSet rs = ptmt.executeQuery();

				while (rs.next()) {
					Toeic rsToeic = new Toeic();

					int tk = rs.getInt("taikhoan");
					String hocki = rs.getString("hocki");
					String ngaythi = rs.getString("ngaythi");
					int diemnghe = rs.getInt("diemnghe");
					int diemdoc = rs.getInt("diemdoc");
					int diemtong = rs.getInt("diemtong");
					String ghichu = rs.getString("ghichu");

					rsToeic.setTaikhoan(tk);
					rsToeic.setHocki(hocki);
					rsToeic.setNgaythi(ngaythi);
					rsToeic.setDiemnghe(diemnghe);
					rsToeic.setDiemdoc(diemdoc);
					rsToeic.setDiemtong(diemtong);
					rsToeic.setGhichu(ghichu);

					list.add(rsToeic);

				}
				conn.close();
			}

			catch (SQLException e) {
				System.out.println(sql);
				System.out.println("Error search Toeic: " + e);
			}

			return list;

		} else {
			try {

				PreparedStatement ptmt = conn.prepareStatement(SEARCH_TOEIC_BY_TK_ALL);
				ptmt.setString(1, taikhoan);

				System.out.println("sql excute:" + ptmt.toString());

				ResultSet rs = ptmt.executeQuery();

				while (rs.next()) {
					Toeic rsToeic = new Toeic();

					int tk = rs.getInt("taikhoan");
					String hocki = rs.getString("hocki");
					String ngaythi = rs.getString("ngaythi");
					int diemnghe = rs.getInt("diemnghe");
					int diemdoc = rs.getInt("diemdoc");
					int diemtong = rs.getInt("diemtong");
					String ghichu = rs.getString("ghichu");

					rsToeic.setTaikhoan(tk);
					rsToeic.setHocki(hocki);
					rsToeic.setNgaythi(ngaythi);
					rsToeic.setDiemnghe(diemnghe);
					rsToeic.setDiemdoc(diemdoc);
					rsToeic.setDiemtong(diemtong);
					rsToeic.setGhichu(ghichu);

					list.add(rsToeic);

				}

				conn.close();

			}

			catch (SQLException ex) {
				System.out.println("Error search Toeic: " + ex);
			}

			return list;
		}

	}

	/**
	 * Lấy id của Toeic
	 * 
	 * @return id
	 */
	public int getToeicid() {
		return toeicid;
	}

	/**
	 * Gán id cho Toeic
	 * 
	 * @param toeicid
	 */
	public void setToeicid(int toeicid) {
		this.toeicid = toeicid;
	}

	/**
	 * Lấy thông tin mã tài khoản sinh viên
	 * 
	 * @return mã tài khoản
	 */
	public int getTaikhoan() {
		return taikhoan;
	}

	/**
	 * Gán thông tin mã tài khoản sinh viên
	 * 

	 * @param taikhoan:mã tài khoản

	 * @param taikhoan:
	 *            mã tài khoản

	 */
	public void setTaikhoan(int taikhoan) {
		this.taikhoan = taikhoan;
	}

	/**
	 * Lấy thông tin điểm nghe bài thi Toeic
	 * 
	 * @return điểm nghe
	 */
	public int getDiemnghe() {
		return diemnghe;
	}

	/**
	 * Gán điểm nghe cho bài thi toeic
	 * 

	 * @param diemnghe:điểm nghe

	 * @param diemnghe:
	 *            điểm nghe

	 */
	public void setDiemnghe(int diemnghe) {
		this.diemnghe = diemnghe;
	}

	/**
	 * Lấy thông tin điểm đọc bài thi Toeic
	 * 
	 * @return điểm đọc
	 */
	public int getDiemdoc() {
		return diemdoc;
	}

	/**
	 * Gán điểm tổng cho bài thi toeic
	 * 

	 * @param diemdoc: điểm đọc

	 * @param diemdoc:
	 *            điểm đọc

	 */
	public void setDiemdoc(int diemdoc) {
		this.diemdoc = diemdoc;
	}

	/**
	 * Lấy thông tin điểm tổng bài thi Toeic
	 * 
	 * @return điểm tổng
	 */
	public int getDiemtong() {
		return diemtong;
	}

	/**
	 * Gán điểm tổng cho bài thi toeic
	 * 

	 * @param diemtong:điểm tổng

	 * @param diemtong:
	 *            điểm tổng

	 */

	public void setDiemtong(int diemtong) {
		this.diemtong = diemtong;
	}

	/**
	 * Lấy thông tin học kì thi toeic
	 * 
	 * @return học kì
	 */
	public String getHocki() {
		return hocki;
	}

	/**
	 * Gán học kì thi Toeic
	 * 

	 * @param hocki:học kì

	 * @param hocki:học
	 *            kì

	 */

	public void setHocki(String hocki) {
		this.hocki = hocki;
	}

	/**
	 * Lấy thông tin ngày thi Toeic
	 * 
	 * @return ngày thi
	 */
	public String getNgaythi() {
		return ngaythi;
	}

	/**
	 * Gán thông tin ngày thi Toeic
	 * 

	 * @param ngaythi ngày thi

	 * @param ngaythi:
	 *            ngày thi

	 */
	public void setNgaythi(String ngaythi) {
		this.ngaythi = ngaythi;
	}

	/**
	 * Lấy thông tin ghi chú
	 * 
	 * @return ghi chú
	 */
	public String getGhichu() {
		return ghichu;
	}

	/**
	 * Gán thông tin ghi chú
	 * 
	 * @param ghichu
	 */
	public void setGhichu(String ghichu) {
		this.ghichu = ghichu;
	}

}
