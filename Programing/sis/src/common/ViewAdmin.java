package common;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

/**
 * View của Admin
 * 
 * @author Nhóm23
 *
 */
public class ViewAdmin extends ViewRoleInfo {
	/** List hiện thị các nhóm có trong menu theo thứ tự của người dùng admin */
	private List<Group> orderGroup = Arrays.asList(Group.TAI_KHOAN, Group.QUAN_LY, Group.KE_HOACH_HOC_TAP);

	@Override
	JPanel getMainView() {
		return new JPanel();
	}

	@Override
	public Map<Group, List<Function>> getMenuFunctions() {
		Map<Group, List<Function>> mapFunctions = new LinkedHashMap<>();
		orderGroup.forEach(group -> {
			mapFunctions.put(group, super.getFunctionByRoleAndGroup(Role.ADMIN, group));
		});
		return mapFunctions;
	}

	public List<Group> getOrderGroup() {
		return orderGroup;
	}
}
