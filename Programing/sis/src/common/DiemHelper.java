package common;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Class này chứa các phương thức định dạng cho các tính năng liên quan tới điểm
 * 
 * @author TuanNguyen
 *
 */
public class DiemHelper {

	/**
	 * Hàm này để làm tròn giá trị với các số sau dấu phẩu cho trước
	 * 
	 * @param value
	 *            là giá trị cần làm tròn
	 * @param places
	 *            là số thập phân sau dấu phẩy
	 * @return số sau khi làm tròn
	 */
	public static BigDecimal round(float value, int places) {
		if (places < 0)
			throw new IllegalArgumentException();
		BigDecimal bd = new BigDecimal(Float.toString(value));
		bd = bd.setScale(places, RoundingMode.HALF_UP);
		return bd;
	}

	/**
	 * Hàm này để làm tròn số thập phân đến .5 Ví dụ: <blockquote>
	 * 
	 * <pre>
	 * DiemHelper.roundNearestFive(9.75) returns 10
	 * DiemHelper.roundNearestFive(9.6) returns 9.5
	 * DiemHelper.roundNearestFive(9.3) returns 9.5
	 * DiemHelper.roundNearestFive(9.25) returns 9.0
	 * </pre>
	 * 
	 * </blockquote>
	 * 
	 * @param value
	 *            là giá trị cần làm tròn
	 * @return số sau khi làm tròn
	 */
	public static float roundNearestFive(float value) {
		BigDecimal bd = new BigDecimal(Float.toString((value * 2)));
		bd = bd.setScale(0, RoundingMode.HALF_UP);
		bd = new BigDecimal(bd.floatValue() / 2f);
		bd = bd.setScale(1, RoundingMode.HALF_UP);
		return bd.floatValue();

	}

	/**
	 * Hàm này để chuyển điểm số thành điểm chữ
	 * 
	 * @param value
	 *            là điểm số
	 * @return điểm chữ
	 */
	public static String parseToDiemChu(float value) {
		if (value < 0 || value > 10)
			throw new IllegalArgumentException();
		if (value < 4)
			return "F";

		value = round(value, 1).floatValue();
		if (value <= 4.9f)
			return "D";
		if (value <= 5.4f)
			return "D+";
		if (value <= 6.4f)
			return "C";
		if (value <= 6.9f)
			return "C+";
		if (value <= 7.9f)
			return "B";
		if (value <= 8.4f)
			return "B+";
		if (value <= 9.4f)
			return "A";
		return "A+";
	}

	/**
	 * Hàm này chuyển điểm chữ thành điểm số
	 * 
	 * @param diemChu
	 *            là điểm chữ
	 * @return điểm số sau khi được chuyển
	 */
	public static float parseToDiemSo(String diemChu) {
		if ("A+".equals(diemChu) || "A".equals(diemChu))
			return 4.0f;
		if ("B+".equals(diemChu))
			return 3.5f;
		if ("B".equals(diemChu))
			return 3f;
		if ("C+".equals(diemChu))
			return 2.5f;
		if ("C".equals(diemChu))
			return 2f;
		if ("D+".equals(diemChu))
			return 1.5f;
		if ("D".equals(diemChu))
			return 1f;
		if ("F".equals(diemChu))
			return 0f;
		throw new IllegalArgumentException();
	}

}
