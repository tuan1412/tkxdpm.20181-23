package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class này để tạo và đóng kết nối tới cơ sở dữ liệu
 * 
 * @author Nhom23
 *
 */
public class ConnectionHelper {

	private static String dbName = "sis";
	private static String url = "localhost:3306";
	private static String username = "root";
	private static String password = "12345678";
	private static String dbPath = "jdbc:mysql://" + url + "/" + dbName;
	private static Connection conn = null;

	/**
	 * Hàm này để kết nối với cơ sở dữ liệu
	 * 
	 * @return đối tượng connection để kết nối cơ sở dữ liệu
	 * @throws SQLException
	 */
	public static Connection connectDb() throws SQLException {
		conn = DriverManager.getConnection(dbPath, username, password);
		return conn;
	}

	/**
	 * Hàm này để đóng kết nối với cơ sở dữ liệu
	 * 
	 * @throws SQLException
	 */
	public static void closeDB() throws SQLException {
		if (conn != null)
			conn.close();
	}
}
