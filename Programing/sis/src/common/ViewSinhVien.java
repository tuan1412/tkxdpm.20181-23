package common;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

/**
 * View của Sinh viên
 * 
 * @author Nhóm23
 *
 */
public class ViewSinhVien extends ViewRoleInfo {

	/** List hiện thị các nhóm có trong menu theo thứ tự của người dùng sinh viên */
	private List<Group> orderGroup = Arrays.asList(Group.TAI_KHOAN, Group.TRA_CUU, Group.DANG_KI,
			Group.KE_HOACH_HOC_TAP);

	@Override
	public JPanel getMainView() {
		return new JPanel();
	}

	@Override
	public Map<Group, List<Function>> getMenuFunctions() {
		Map<Group, List<Function>> mapFunctions = new LinkedHashMap<>();
		orderGroup.forEach(group -> {
			mapFunctions.put(group, super.getFunctionByRoleAndGroup(Role.SINHVIEN, group));
		});
		return mapFunctions;
	}

	public List<Group> getOrderGroup() {
		return orderGroup;
	}
}
