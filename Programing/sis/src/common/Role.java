package common;

/**
 * Class để liệt kê role của ứng dụng
 * 
 * @author Nhom23
 *
 */
public enum Role {

	/** Role admin */
	ADMIN(1),

	/** Role sinh viên */
	SINHVIEN(2),

	/** Role khách */
	KHACH(3);

	/** Số tương ứng với role trong csdl */
	private final int role;

	Role(int role) {
		this.role = role;
	}

	/**
	 * Hàm này để lấy số nguyên chỉ role
	 * 
	 * @return số nguyên chỉ role tương ứng
	 */
	public int getRole() {
		return role;
	}
}
