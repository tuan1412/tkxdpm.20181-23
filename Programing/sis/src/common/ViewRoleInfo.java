package common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

/**
 * Class này để chứa thông tin của view tương ứng theo role
 * 
 * @author Nhóm23
 *
 */
public abstract class ViewRoleInfo {

	/**
	 * Đưa ra màn hình chính để hiện thị
	 * 
	 * @return màn hình hiện thị
	 */
	abstract JPanel getMainView();

	/**
	 * Đưa ra các chức năng trên thanh menu
	 * 
	 * @return các chức năng tương ứng với nhóm của chúng
	 */
	abstract Map<Group, List<Function>> getMenuFunctions();

	/**
	 * Nhóm các chức năng theo role và group
	 * 
	 * @param role
	 *            là role của các chức năng
	 * @param group
	 *            là nhóm của các chức năng
	 * @return mảng chứa các chức năng và role
	 */
	protected List<Function> getFunctionByRoleAndGroup(Role role, Group group) {
		ArrayList<Function> functions = new ArrayList<>();
		for (Function function : Function.values()) {
			boolean hasRole = function.getRoles().contains(role);
			boolean inGroup = function.getGroup().equals(group);
			if (hasRole && inGroup) {
				functions.add(function);
			}
		}
		return functions;
	}
}
