﻿package common;

import java.util.Arrays;
import java.util.List;
/**
 * Class này để liệt kê các chức năng có trong chương trình
 * 
 * @author Nhom23
 *
 */
public enum Function {
	
	/** Usecase Đăng nhập */
	LOGIN(
			"Đăng nhập",
			Group.TAI_KHOAN,
			Arrays.asList(Role.KHACH),
			"boundary.LoginForm"
	),
	
	/** Usecase Đổi mật khẩu */
	CHANGE_PASS(
			"Đổi mật khẩu", 
			Group.TAI_KHOAN,
			Arrays.asList(Role.ADMIN, Role.SINHVIEN),
			"boundary.DoiMatKhau"
	),
	
	/** Usecase Cập nhật thông tin cá nhân sinh viên */
	UPDATE_INFO_USER(
			"Cập nhật thông tin",
			Group.TAI_KHOAN,
			Arrays.asList(Role.SINHVIEN),
			"boundary.UserUpdate"),
	
	/** Usecase Đăng xuất */
	LOG_OUT(
			"Đăng xuất",
			Group.TAI_KHOAN, 
			Arrays.asList(Role.ADMIN, Role.SINHVIEN),
			"boundary.LoginForm"
	),
	
	/** Usecase Tra cứu học phí */
	SEARCH_TUITION(
			"Tra cứu học phí",
			Group.TRA_CUU, 
			Arrays.asList(Role.SINHVIEN),
			"boundary.TraCuuHocPhi"
	),
	
	/** Usecase Tra cứu bảng điểm cá nhân */
	SEARCH_BANGDIEM(
			"Tra cứu bảng điểm cá nhân",
			Group.TRA_CUU, 
			Arrays.asList(Role.SINHVIEN),
			"boundary.TraCuuBangDiem"
	),
	
	/** Usecase Đăng kí học phần */
	DANGKY_HP(
			"Đăng ký học phần",
			Group.DANG_KI, 
			Arrays.asList(Role.SINHVIEN),
			"boundary.DangKyHoc"
	),
	
	/** Usecase Tra cứu điểm thi Toeic */
	SEARCH_TOEIC(
			"Điểm thi Toeic",
			Group.TRA_CUU,
			Arrays.asList(Role.SINHVIEN),
			"boundary.Toeic"
	),
	
	/** Usecase Cập nhật điểm sinh viên */
	UPDATE_MARK(
			"Cập nhật điểm",
			Group.QUAN_LY,
			Arrays.asList(Role.ADMIN),
			"boundary.CapNhatDiem"
	),
	
	/** Usecase Thời khóa biểu */
	TIME_TABLE(
			"Thời khóa biểu",
			Group.KE_HOACH_HOC_TAP,
			Arrays.asList(Role.SINHVIEN, Role.ADMIN, Role.KHACH),
			"boundary.ThoiKhoaBieu"
			
	),
	
	/** Usecase Cập nhật học phí */
	UPDATE_TUITION(
			"Cập nhật học phí",
			Group.QUAN_LY,
			Arrays.asList(Role.ADMIN),
			"boundary.CapNhatHocPhi"
	);
	
	/** name là tên hiện thị chức năng */
	private final String name;
	
	/** group là tiêu đề của menu */
	private final Group group;
	
	/** roles là quyền được sử dụng */
	private final List<Role> roles;
	
	/** clazzName là đường dẫn tới class view tương ứng */
	private final String clazzName;
	
	
	Function(String name, Group group, List<Role> roles, String clazzName) {
		this.name= name;
		this.group = group;
		this.roles = roles;
		this.clazzName = clazzName;
	}
	
	/**
	 * Hàm này để lấy tên của chức năng
	 * @return tên của chức năng
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Hàm này để lấy nhóm của chức năng đó
	 * @return group của chức năng
	 */
	public Group getGroup() {
		return group;
	}

	/**
	 * Hàm này để lấy roles
	 * @return các quyền được thực hiện
	 */
	public List<Role> getRoles() {
		return roles;
	}

	/**
	 * Hàm này để lấy đường dẫn tới lớp biên tương ứng chức năng
	 * @return
	 */
	public String getClazzName() {
		return clazzName;
	}
	
}
