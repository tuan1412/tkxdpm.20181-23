package common;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

/**
 * Class này chức các phương thức trợ giúp các chức năng liên quan tới user
 * 
 * @author Nhom23
 *
 */
public class UserHelper {

	/**
	 * PATTERN email: hợp lệ khi nhập cá kí tự A->Z, a->z, 0->9, không chứa kí tự
	 * đặc biệt gồm @ và dấu chấm
	 */
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	/**
	 * PATTERN sdt: hợp lệ khi bắt đầu bằng số 0, có chiều dài từ 9-> 10 chữ số
	 */
	private static final String PHONE_NUMBER_PATTER = "0\\d{9,10}";

	public static String MD5encrypt(String password) {
		StringBuffer sb = new StringBuffer();
		try {
			byte[] bytesOfMessage = password.getBytes("UTF-8");
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] thedigest = md.digest(bytesOfMessage);
			for (int i = 0; i < thedigest.length; i++) {
				sb.append(Integer.toString((thedigest[i] & 0xff) + 0x100, 16).substring(1));
			}
		} catch (NoSuchAlgorithmException ex) {
			ex.printStackTrace();
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
		}
		return sb.toString();
	}

	/**
	 * Kiểm tra tính hợp lệ của email
	 * 
	 * @param email
	 * @return true nếu email không hợp lệ
	 */
	public static boolean validateEmail(String email) {
		if (!email.matches(EMAIL_PATTERN))
			return true;
		return false;
	}

	/**
	 * Kiểm tra tính hợp lệ của số điện thoại
	 * 
	 * @param sdt
	 * @return true nếu số điện thoại không hợp lệ
	 */
	public static boolean validatePhoneNumber(String sdt) {
		if (!sdt.trim().matches(PHONE_NUMBER_PATTER))
			return true;
		return false;
	}

	/**
	 * Hàm này để chuyển đổi họ tên thành tên đứng trước họ
	 * 
	 * @param hoten
	 * @return họ tên đã chuyển đổi (tên đứng trước họ đệm)
	 */
	public static String convertHoten(String hoten) {
		hoten = hoten.trim();
		int indexOfTen = hoten.lastIndexOf(" ");
		String ten = hoten.substring(indexOfTen + 1);
		String hodem = hoten.substring(0, indexOfTen);
		return String.join(" ", ten, hodem);
	}

	/**
	 * Tạo random String capcha, bao gồm kí tự và chữ số
	 * 
	 * @return capcha
	 */
	public static String generateCaptchaString() {
		Random random = new Random();
		int length = 6 + (Math.abs(random.nextInt()) % 3);

		StringBuffer captchaStringBuffer = new StringBuffer();
		for (int i = 0; i < length; i++) {
			int baseCharNumber = Math.abs(random.nextInt()) % 62;
			int charNumber = 0;
			if (baseCharNumber < 26) {
				charNumber = 65 + baseCharNumber;
			} else if (baseCharNumber < 52) {
				charNumber = 97 + (baseCharNumber - 26);
			} else {
				charNumber = 48 + (baseCharNumber - 52);
			}
			captchaStringBuffer.append((char) charNumber);
		}

		return captchaStringBuffer.toString();
	}

}
