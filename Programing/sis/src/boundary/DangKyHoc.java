package boundary;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import com.mysql.cj.util.StringUtils;
import common.MainUI;
import controller.DangKiHocController;
import controller.HocPhanController;
import entity.DangKiHocPhan;
import entity.HocPhan;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Vector;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Color;

/**
 * Class này là lớp biên của usecase Đăng kí học phần
 * 
 * @author TungNguyen
 *
 */
public class DangKyHoc extends JPanel {
	private JTextField txtMaHP;
	private JTable tblHPDK;
	JLabel lblBangDK;
	JLabel lblErrorMaHP;
	JLabel lblTongTC;
	int TongTC;

	/** Hàm này định dạng dữ diệu cột trong bảng dưới dạng Boolean */
	DefaultTableModel model = new DefaultTableModel() {
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return columnIndex == 5 ? Boolean.class : String.class;
		}
	};

	/** Hàm này lấy ra thời gian hiện tại */
	DateTimeFormatter dtf = DateTimeFormatter.ISO_LOCAL_DATE;
	LocalDateTime now = LocalDateTime.now();

	/**
	 * Create the panel.
	 */
	public DangKyHoc() {
		initComponent();
	}

	public void initComponent() {
		JLabel lblHcK = new JLabel("Học kỳ:");

		// Bắt sự kiện cho JComboBox
		JComboBox cbbHocKy = new JComboBox();
		cbbHocKy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TongTC = 0;
				lblErrorMaHP.setText("");
				try {
					model.setRowCount(0);
					
					//hàm này để lấy ra danh sách đăng ký học phần theo kỳ
					List<DangKiHocPhan> dkhocphan = DangKiHocController.getInstance()
							.getDangKiHocPhan(cbbHocKy.getSelectedItem().toString(), MainUI.getInstance().getTaiKhoan());
					lblBangDK.setText("Bảng đăng kí học phần kỳ " + cbbHocKy.getSelectedItem().toString()
							+ " của sinh viên " + MainUI.getInstance().getTaiKhoan());
					for (int j = 0; j < dkhocphan.size(); j++) {
						DangKiHocPhan dkhp = dkhocphan.get(j);
						Vector<String> row = new Vector<String>();
						row.add(dkhp.getMahp());
						row.add(dkhp.getTenhp());
						row.add(dkhp.getNgaydk());
						row.add(dkhp.getTrangthaidk());
						row.add(String.valueOf(dkhp.getSotinchi()));
						System.out.println(dkhp.getMahp());
						model.addRow(row);
						tblHPDK.setModel(model);
						TongTC += Integer.parseInt(tblHPDK.getValueAt(j, 4).toString());
						lblTongTC.setText(String.valueOf("Tổng số TC = " + TongTC));
					}

					tblHPDK.setModel(model);
					tblHPDK.setVisible(true);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		});
		cbbHocKy.setModel(
				new DefaultComboBoxModel(new String[] { "20171", "20172", "20173", "20181", "20182", "20183" }));

		JLabel lblHcChngTrnh = new JLabel("Học chương trình:");

		JLabel lblNganhHoc = new JLabel("CT Nhóm ngành CNTT-TT 2-2015");

		JLabel lblMHpng = new JLabel("Mã HP đăng ký:");

		txtMaHP = new JTextField();
		txtMaHP.setColumns(10);

		tblHPDK = new JTable();
		String[] colunmNames = { "Mã HP", "Tên lớp", "Ngày đăng ký", "TT đăng ký", "Số TC", "" };
		Vector<String> colunm = new Vector<String>();
		int numberColumn;
		numberColumn = colunmNames.length;
		for (int i = 0; i < numberColumn; i++) {
			colunm.add(colunmNames[i]);
		}
		model.setColumnIdentifiers(colunm);
		System.out.println();
		tblHPDK.setModel(model);
		tblHPDK.setVisible(true);

		// Bắt sự kiện cho Button Đăng ký
		JButton btnDangKy = new JButton("Đăng ký");
		btnDangKy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				HocPhan hocphan = new HocPhan();
				if (StringUtils.isNullOrEmpty(txtMaHP.getText())) {
					lblErrorMaHP.setText("Chưa nhập mã học phần");
					return;
				}

				for (int i = 0; i < model.getRowCount(); i++) {
					if (tblHPDK.getValueAt(i, 0).toString().equalsIgnoreCase(txtMaHP.getText())) {
						lblErrorMaHP.setText("Mã học phần đã tồn tại");
						return;
					}
				}

				try {
					hocphan = HocPhanController.getInstance().getHocPhan(txtMaHP.getText());
					if (StringUtils.isNullOrEmpty(hocphan.getMaHP())) {
						lblErrorMaHP.setText("Học phần không tồn tại");
						return;
					}
					Vector<String> row = new Vector<String>();
					row.add(hocphan.getMaHP());
					row.add(hocphan.getTenHP());
					row.add(dtf.format(now));
					row.add("insert");
					row.add(String.valueOf(hocphan.getSoTinChi()));
					model.addRow(new Object[] { hocphan.getMaHP(), hocphan.getTenHP(), dtf.format(now), "insert",
							String.valueOf(hocphan.getSoTinChi()), Boolean.FALSE });
					tblHPDK.setModel(model);
					tblHPDK.setVisible(true);
					int numrow = tblHPDK.getRowCount();
					TongTC = 0;
					for (int i = 0; i < numrow; i++) {
						if (numrow == 0) {
							lblTongTC.setText(String.valueOf("Tổng số TC = 0"));
						} else {
							TongTC += Integer.parseInt(tblHPDK.getValueAt(i, 4).toString());
							lblTongTC.setText(String.valueOf("Tổng số TC = " + TongTC));
						}
					}

				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				txtMaHP.setText("");
				System.out.println(dtf.format(now));
			}
		});

		JScrollPane scrollPane = new JScrollPane();

		// Bắt sự kiện cho Button Xóa học phần đã chọn
		JButton btnXoaHP = new JButton("Xóa học phần đã chọn");
		btnXoaHP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (int i = 0; i < tblHPDK.getRowCount(); i++) {
					if (tblHPDK.getValueAt(i, 5) == null)
						continue;
					if (((Boolean) tblHPDK.getValueAt(i, 5))) {
						tblHPDK.setValueAt("Delete", i, 3);
					}
				}
			}
		});

		// Bắt sự kiện cho Button Gửi đăng ký
		JButton btnGuiDK = new JButton("Gửi đăng ký");
		btnGuiDK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for (int count = 0; count < model.getRowCount(); count++) {
					DangKiHocPhan dangkihp = new DangKiHocPhan();
					if (tblHPDK.getValueAt(count, 3).toString().equals("Success")) {
						continue;
					} else if (tblHPDK.getValueAt(count, 3).toString().equals("Delete")) {
						dangkihp.setKihoc(cbbHocKy.getSelectedItem().toString());
						dangkihp.setMahp(model.getValueAt(count, 0).toString());
						dangkihp.setTenhp((model.getValueAt(count, 1).toString()));
						dangkihp.setNgaydk(model.getValueAt(count, 2).toString());
						dangkihp.setSotinchi(Integer.valueOf(model.getValueAt(count, 4).toString()));
						dangkihp.setTaikhoan(MainUI.getInstance().getTaiKhoan());
						if (DangKiHocController.getInstance().deleteDangKiHocPhan(dangkihp)) {
							// JOptionPane.showMessageDialog(null, "Gửi đăng ký học phần thành công");
						}
					} else {
						dangkihp.setKihoc(cbbHocKy.getSelectedItem().toString());
						dangkihp.setMahp(model.getValueAt(count, 0).toString());
						dangkihp.setTenhp((model.getValueAt(count, 1).toString()));
						dangkihp.setNgaydk(model.getValueAt(count, 2).toString());
						dangkihp.setSotinchi(Integer.valueOf(model.getValueAt(count, 4).toString()));
						dangkihp.setTaikhoan(MainUI.getInstance().getTaiKhoan());
						dangkihp.setTrangthaidk("Success");

						if (DangKiHocController.getInstance().insertDangKiHocPhan(dangkihp)) {
							model.setValueAt("Success", count, 3);
							// JOptionPane.showMessageDialog(null, "Gửi đăng ký học phần thành công");
						}
					}
				}
				JOptionPane.showMessageDialog(null, "Gửi đăng ký học phần thành công");
				try {
					model.setRowCount(0);
					List<DangKiHocPhan> dkhocphan = DangKiHocController.getInstance().getDangKiHocPhan(
							cbbHocKy.getSelectedItem().toString(), MainUI.getInstance().getTaiKhoan());
					lblBangDK.setText("Bảng đăng kí học phần kỳ " + cbbHocKy.getSelectedItem().toString()
							+ " của sinh viên " + MainUI.getInstance().getTaiKhoan());
					for (int j = 0; j < dkhocphan.size(); j++) {
						DangKiHocPhan dkhp = dkhocphan.get(j);
						Vector<String> row = new Vector<String>();
						row.add(dkhp.getMahp());
						row.add(dkhp.getTenhp());
						row.add(dkhp.getNgaydk());
						row.add(dkhp.getTrangthaidk());
						row.add(String.valueOf(dkhp.getSotinchi()));
						System.out.println(dkhp.getMahp());
						model.addRow(row);
						tblHPDK.setModel(model);
					}

					int numrow = tblHPDK.getRowCount();
					TongTC = 0;
					for (int i = 0; i < numrow; i++) {
						if (numrow == 0) {
							lblTongTC.setText(String.valueOf("Tổng số TC = 0"));
						} else {
							TongTC += Integer.parseInt(tblHPDK.getValueAt(i, 4).toString());
							lblTongTC.setText(String.valueOf("Tổng số TC = " + TongTC));
						}
					}

					tblHPDK.setModel(model);
					tblHPDK.setVisible(true);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		lblErrorMaHP = new JLabel("");
		lblErrorMaHP.setForeground(Color.RED);

		lblBangDK = new JLabel("");
		lblBangDK.setFont(new Font("Tahoma", Font.BOLD, 12));

		lblTongTC = new JLabel("Tổng số TC = 0");

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
						.createSequentialGroup().addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
								.createSequentialGroup().addComponent(lblHcK).addGap(18)
								.addComponent(cbbHocKy, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
								.addGap(18).addComponent(lblHcChngTrnh).addGap(18).addComponent(lblNganhHoc))
								.addComponent(lblErrorMaHP)
								.addGroup(groupLayout.createSequentialGroup().addComponent(lblMHpng).addGap(18)
										.addComponent(txtMaHP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(18).addComponent(btnDangKy))))
						.addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(scrollPane,
								GroupLayout.DEFAULT_SIZE, 543, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup().addGap(167)
								.addComponent(lblBangDK, GroupLayout.DEFAULT_SIZE, 176, Short.MAX_VALUE).addGap(210)))
				.addContainerGap())
				.addGroup(Alignment.LEADING,
						groupLayout.createSequentialGroup().addGap(224)
								.addComponent(btnGuiDK, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addGap(250))
				.addGroup(groupLayout.createSequentialGroup().addContainerGap(525, Short.MAX_VALUE)
						.addComponent(btnXoaHP).addContainerGap())
				.addGroup(groupLayout.createSequentialGroup().addContainerGap(618, Short.MAX_VALUE)
						.addComponent(lblTongTC).addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addGap(35)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblHcK)
						.addComponent(cbbHocKy, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(lblHcChngTrnh).addComponent(lblNganhHoc))
				.addGap(26)
				.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addComponent(lblMHpng)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtMaHP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(btnDangKy)))
				.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(lblErrorMaHP).addGap(8)
				.addComponent(lblBangDK).addGap(18)
				.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(lblTongTC)
				.addPreferredGap(ComponentPlacement.RELATED, 29, Short.MAX_VALUE).addComponent(btnXoaHP).addGap(16)
				.addComponent(btnGuiDK).addGap(21)));

		scrollPane.setViewportView(tblHPDK);
		setLayout(groupLayout);
	}

}
