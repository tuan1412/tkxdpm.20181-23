package boundary;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.TitledBorder;

import common.MainUI;
import common.UserHelper;

/**
 * Class này là lớp biên của usecase Đổi mật khẩu
 * 
 * @author TungNguyen
 *
 */
public class DoiMatKhau extends JPanel {
	private JPasswordField pfXacNhanMK;
	private JPasswordField pfMatKhauMoi;
	private JPasswordField pfMatKhauCu;
	private String taikhoan;
	private String matkhau;
	JLabel lblError;

	/**
	 * Create the panel.
	 */
	public DoiMatKhau() {
		taikhoan = MainUI.getInstance().getTaiKhoan();
		matkhau = MainUI.getInstance().getMatKhau();
		initComponent();

	}

	/**
	 * Hàm này để khởi tạo các components
	 */
	public void initComponent() {
		JPanel panel = new JPanel();
		panel.setBorder(
				new TitledBorder(null, "THÔNG TIN TÀI KHOẢN", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		JLabel lbliMtKhu = new JLabel("ĐỔI MẬT KHẨU");
		lbliMtKhu.setFont(new Font("Tahoma", Font.PLAIN, 14));

		// Bắt sự kiện ấn nút Thay đổi
		JButton btnThayDoi = new JButton("Thay đổi");
		btnThayDoi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				doiMatKhau();
			}
		});
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(166).addComponent(lbliMtKhu))
						.addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(btnThayDoi))
						.addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(panel,
								GroupLayout.DEFAULT_SIZE, 429, Short.MAX_VALUE)))
				.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(lbliMtKhu, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE).addGap(18)
						.addComponent(panel, GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE).addGap(18)
						.addComponent(btnThayDoi).addContainerGap()));

		JLabel lblTiKhon = new JLabel("Tài khoản:");

		JLabel lblMtKhuC = new JLabel("Mật khẩu cũ:");

		JLabel lblMtKhuMi = new JLabel("Mật khẩu mới:");

		JLabel lblXcNhnMt = new JLabel("Xác nhận mật khẩu:");

		pfXacNhanMK = new JPasswordField();

		pfMatKhauMoi = new JPasswordField();

		pfMatKhauCu = new JPasswordField();

		JLabel lblTaiKhoan = new JLabel(MainUI.getInstance().getTaiKhoan());

		lblError = new JLabel("");
		lblError.setForeground(Color.RED);

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING).addComponent(lblXcNhnMt)
										.addComponent(lblMtKhuMi).addComponent(lblMtKhuC).addComponent(lblTiKhon))
								.addGap(18)
								.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblTaiKhoan, GroupLayout.DEFAULT_SIZE, 251, Short.MAX_VALUE)
										.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
												.addComponent(pfMatKhauCu).addComponent(pfMatKhauMoi).addComponent(
														pfXacNhanMK, GroupLayout.DEFAULT_SIZE, 187, Short.MAX_VALUE)))
								.addContainerGap(136, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup().addComponent(lblError).addContainerGap(500,
								Short.MAX_VALUE)))));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addGap(26)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblTiKhon)
						.addComponent(lblTaiKhoan, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblMtKhuC).addComponent(
						pfMatKhauCu, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblMtKhuMi).addComponent(
						pfMatKhauMoi, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addGap(18)
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblXcNhnMt).addComponent(
						pfXacNhanMK, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
				.addPreferredGap(ComponentPlacement.RELATED, 45, Short.MAX_VALUE).addComponent(lblError)
				.addContainerGap()));
		panel.setLayout(gl_panel);
		setLayout(groupLayout);
	}

	/**
	 * Hàm này thực hiện chức năng đổi mật khẩu
	 */
	private void doiMatKhau() {
		String matKhauCu = new String(pfMatKhauCu.getPassword());
		String matKhauMoi = new String(pfMatKhauMoi.getPassword());
		String xacNhanMK = new String(pfXacNhanMK.getPassword());
		lblError.setText("");
		if (matKhauCu.length() < 8 || matKhauMoi.length() < 8 || xacNhanMK.length() < 8) {
			lblError.setText("Mật khẩu phải ít nhất 8 ký tự");

		} else if (!UserHelper.MD5encrypt(matKhauCu).equals(matkhau)) {
			lblError.setText("Mật khẩu của bạn không đúng");
		} else if (!matKhauMoi.equals(xacNhanMK)) {
			lblError.setText("Xác nhận mật khẩu không đúng");
		} else {
			controller.UserController.getInstance().changePassword(taikhoan, UserHelper.MD5encrypt(matKhauMoi));
			JOptionPane.showMessageDialog(null, "Thay đổi mật khẩu thành công");
		}
	}
}
