package boundary;

import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;

import controller.HocPhiController;

/**
 * Class này là form để sửa hoặc thêm mới học phí
 * 
 * @author TuanNguyen
 *
 */
@SuppressWarnings("serial")
public class HocPhiForm extends JFrame {

	/** panel chứu giao diện */
	private JPanel contentPane;

	/** TextField để nhập hoặc sửa kì học */
	private JTextField tfKiHoc;

	/** TextField để nhập hoặc sửa học phí */
	private JTextField tfHocPhi;

	/** Boundary gọi ra form này */
	private CapNhatHocPhi capNhatHocPhi;

	public HocPhiForm(CapNhatHocPhi capNhatHocPhi, int kihoc, long hocphi) {
		initComponent(capNhatHocPhi, kihoc, hocphi);
	}

	/**
	 * Hàm này của sự kiện ấn nút xác nhận
	 */
	private void xacNhan() {
		String kihoc = tfKiHoc.getText();
		String hocphi = tfHocPhi.getText();
		if (kihoc.isEmpty() || hocphi.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Trường thông tin không được trống");
			return;
		}
		try {
			int kihocInt = Integer.parseInt(kihoc);
			long hocPhiLong = Long.parseLong(hocphi);
			boolean success = HocPhiController.getInstance().insertOrUpdateHocPhi(kihocInt, hocPhiLong);
			if (success) {
				JOptionPane.showMessageDialog(this, "Thành công");
				capNhatHocPhi.generateHocPhi();
				this.dispose();
			} else {
				JOptionPane.showMessageDialog(this, "Thất bại");
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Trường dữ liệu nhập vào không đúng");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(this, "Lỗi kết nối cơ sở dữ liệu");
		}
	}

	/**
	 * Hàm này của sự kiện ấn nút hủy
	 */
	private void huy() {
		this.dispose();
	}

	/**
	 * Hàm này để khởi tạo các component
	 * 
	 * @param capNhatHocPhi
	 *            là boundary gọi form này
	 * @param kihoc
	 *            là kì học cần sửa hoặc rỗng (nếu thêm mới)
	 * @param hocphi
	 *            là học phí cần sửa hoặc rỗng (nếu thêm mới)
	 */
	private void initComponent(CapNhatHocPhi capNhatHocPhi, int kihoc, long hocphi) {
		this.capNhatHocPhi = capNhatHocPhi;


		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		

		setBounds(100, 100, 353, 192);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JLabel lblKiHoc = new JLabel("Kì học:");

		JLabel lblHocPhi = new JLabel("Học phí");

		JButton btnXacNhan = new JButton("Xác nhận");

		btnXacNhan.addActionListener(e -> xacNhan());

		JButton btnHuy = new JButton("Hủy");
		btnHuy.addActionListener(e -> huy());

		tfKiHoc = new JTextField();
		tfKiHoc.setColumns(10);
		if (kihoc > 0) {
			tfKiHoc.setText(String.valueOf(kihoc));
		}

		tfHocPhi = new JTextField();
		tfHocPhi.setColumns(10);
		if (hocphi > 0) {
			tfHocPhi.setText(String.valueOf(hocphi));
		}

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING).addGroup(gl_contentPane
				.createSequentialGroup().addGap(26)
				.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(lblHocPhi, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)
						.addComponent(lblKiHoc, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE))
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(
						gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(tfHocPhi, GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
								.addGroup(gl_contentPane.createSequentialGroup().addComponent(btnXacNhan).addGap(18)
										.addComponent(btnHuy, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addComponent(tfKiHoc, GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))
				.addGap(110)));
		gl_contentPane.setVerticalGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup().addGap(34)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblKiHoc, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
								.addComponent(tfKiHoc, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(18)
						.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE).addComponent(lblHocPhi)
								.addComponent(tfHocPhi, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(18).addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnXacNhan).addComponent(btnHuy))
						.addContainerGap(21, Short.MAX_VALUE)));
		contentPane.setLayout(gl_contentPane);
	}
}
