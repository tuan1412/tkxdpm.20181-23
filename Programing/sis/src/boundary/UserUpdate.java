package boundary;

import javax.swing.JPanel;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.*;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import javax.swing.JButton;

import common.MainUI;
import common.UserHelper;
import controller.SinhVienController;
import entity.SinhVien;

import java.awt.Font;
import java.awt.Color;
import java.awt.event.ActionEvent;

/**
 * Class thể hiện View Cập nhật thông tin sinh viên
 * 
 * @author PhamTuan
 *
 */
public class UserUpdate extends JPanel implements ActionListener {

	/** TextField nhập email, số điện thoại, địa chỉ */
	private JTextField tfEmail, tfSoDT, tfDiaChi, tfCapcha;

	/** Label hiển thị thông tin và lỗi */
	private JLabel lbmasv, lbHoten, lbNgaySinh, lbLop, lbChuongTrinh, lbHeHoc, lbEmail, lbSoDienThoai, lbDiaChi,
			lbTrangThai, lbThongtinTK, lbContentMssv, lbContentHoten, lbContentNgaySinh, lbContentLop,
			lbContentChuongTrinh, lbContentHeHoc, lbContentTrangThai, lbCapcha, lbDisplayCapcha, lberrorCapcha,
			lbDisplay, lbErrorEmail, lbErrorSoDT, lbErrorDiaChi;

	/** Button thực hiện cập nhật thông tin */
	private JButton btnCapNhatTT;

	/**
	 * Create the panel updateinfo.
	 * 
	 * @throws SQLException
	 */
	public UserUpdate() throws SQLException {

		lbmasv = new JLabel("Mã sinh viên:");
		lbHoten = new JLabel("Họ tên SV:");
		lbNgaySinh = new JLabel("Ngày sinh:");
		lbLop = new JLabel("Lớp:");
		lbChuongTrinh = new JLabel("Chương trình:");
		lbHeHoc = new JLabel("Hệ học:");
		lbEmail = new JLabel("Email*:");
		lbSoDienThoai = new JLabel("Số điện thoại*:");
		lbDiaChi = new JLabel("Địa chỉ*:");
		lbTrangThai = new JLabel("Trạng thái:");
		lbThongtinTK = new JLabel("Thông tin tài khoản");
		lbThongtinTK.setFont(new Font("Tahoma", Font.BOLD, 14));

		lbContentMssv = new JLabel("20151111");
		lbContentHoten = new JLabel("Nguyễn Văn A");
		lbContentNgaySinh = new JLabel("05/06/1997");
		lbContentLop = new JLabel("CNTT2.4");
		lbContentChuongTrinh = new JLabel("Kỹ sư CNTT");
		lbContentHeHoc = new JLabel("Kỹ sư");
		lbContentTrangThai = new JLabel("Học");

		tfEmail = new JTextField();
		tfEmail.setColumns(10);

		tfSoDT = new JTextField();
		tfSoDT.setColumns(10);

		tfDiaChi = new JTextField();
		tfDiaChi.setColumns(10);

		btnCapNhatTT = new JButton("Cập nhật thông tin");
		btnCapNhatTT.addActionListener(this);

		lbDisplay = new JLabel(" ");
		lbDisplay.setForeground(Color.RED);

		lbErrorEmail = new JLabel(" ");
		lbErrorEmail.setForeground(Color.RED);

		lbErrorSoDT = new JLabel(" ");
		lbErrorSoDT.setForeground(Color.RED);

		lbErrorDiaChi = new JLabel(" ");
		lbErrorDiaChi.setForeground(Color.RED);
		lbCapcha = new JLabel("Mã Capcha*:");

		tfCapcha = new JTextField();
		tfCapcha.setColumns(10);

		lbDisplayCapcha = new JLabel(" ");
		lbDisplayCapcha.setFont(new Font("HoangYen 1.1", Font.BOLD | Font.ITALIC, 21));

		lberrorCapcha = new JLabel(" ");
		lberrorCapcha.setForeground(Color.RED);

		// Load Data User ->set text for label
		LoadData();

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addGap(34)
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addComponent(lbHeHoc).addContainerGap())
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addComponent(lbThongtinTK)
										.addContainerGap())
								.addGroup(groupLayout.createSequentialGroup().addGroup(groupLayout
										.createParallelGroup(Alignment.LEADING)
										.addComponent(lbHoten, GroupLayout.DEFAULT_SIZE, 89, Short.MAX_VALUE)
										.addGroup(groupLayout.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED).addComponent(lbmasv)
												.addPreferredGap(ComponentPlacement.RELATED, 26, Short.MAX_VALUE))
										.addComponent(lbTrangThai).addComponent(lbLop).addComponent(lbNgaySinh)
										.addComponent(lbChuongTrinh, GroupLayout.PREFERRED_SIZE, 89,
												GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lbContentLop)
												.addComponent(lbContentNgaySinh, GroupLayout.PREFERRED_SIZE, 110,
														GroupLayout.PREFERRED_SIZE)
												.addGroup(groupLayout.createSequentialGroup()
														.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
																.addGroup(groupLayout.createSequentialGroup()
																		.addComponent(lbEmail)
																		.addPreferredGap(ComponentPlacement.UNRELATED))
																.addGroup(groupLayout.createSequentialGroup()
																		.addGroup(groupLayout
																				.createParallelGroup(Alignment.TRAILING)
																				.addComponent(lbSoDienThoai)
																				.addComponent(lbDiaChi)
																				.addComponent(lbCapcha))
																		.addPreferredGap(ComponentPlacement.UNRELATED))
																.addGroup(groupLayout.createSequentialGroup()
																		.addGroup(groupLayout
																				.createParallelGroup(Alignment.LEADING)
																				.addComponent(lbContentMssv)
																				.addComponent(lbContentHoten,
																						GroupLayout.PREFERRED_SIZE, 129,
																						GroupLayout.PREFERRED_SIZE)
																				.addComponent(lbContentChuongTrinh,
																						GroupLayout.PREFERRED_SIZE, 250,
																						GroupLayout.PREFERRED_SIZE)
																				.addComponent(lbContentHeHoc)
																				.addComponent(lbContentTrangThai,
																						GroupLayout.PREFERRED_SIZE, 175,
																						GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(ComponentPlacement.RELATED)))
														.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
																.addComponent(lberrorCapcha)
																.addGroup(groupLayout
																		.createParallelGroup(Alignment.LEADING, false)
																		.addComponent(lbErrorDiaChi)
																		.addComponent(lbErrorEmail)
																		.addComponent(lbErrorSoDT)
																		.addComponent(tfEmail, GroupLayout.DEFAULT_SIZE,
																				248, Short.MAX_VALUE)
																		.addComponent(tfSoDT).addComponent(tfDiaChi))
																.addGroup(groupLayout.createSequentialGroup()
																		.addComponent(tfCapcha,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(39).addComponent(lbDisplayCapcha,
																				GroupLayout.PREFERRED_SIZE, 157,
																				GroupLayout.PREFERRED_SIZE))
																.addGroup(groupLayout.createSequentialGroup().addGap(1)
																		.addGroup(groupLayout
																				.createParallelGroup(Alignment.LEADING)
																				.addComponent(btnCapNhatTT)
																				.addComponent(lbDisplay))))))
										.addGap(61))))));
		groupLayout
				.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(14).addGroup(groupLayout
								.createParallelGroup(
										Alignment.TRAILING)
								.addGroup(
										groupLayout
												.createSequentialGroup().addGap(21).addComponent(lbThongtinTK)
												.addGap(24)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(lbmasv, GroupLayout.DEFAULT_SIZE, 16,
																Short.MAX_VALUE)
														.addComponent(lbContentMssv))
												.addGap(33)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(lbHoten, GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(lbContentHoten))
												.addPreferredGap(ComponentPlacement.RELATED))
								.addGroup(groupLayout.createSequentialGroup()
										.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
												.addComponent(tfEmail, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lbEmail))
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(lbErrorEmail)
										.addGap(6)))
								.addGap(14)
								.addGroup(groupLayout
										.createParallelGroup(Alignment.BASELINE)
										.addComponent(tfSoDT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lbSoDienThoai))
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup().addGap(18)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(lbNgaySinh, GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(lbContentNgaySinh)))
										.addGroup(groupLayout
												.createSequentialGroup().addGap(7).addComponent(lbErrorSoDT)))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(tfDiaChi, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lbDiaChi))
								.addPreferredGap(ComponentPlacement.RELATED)
								.addGroup(
										groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lbErrorDiaChi)
												.addComponent(lbLop, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
														Short.MAX_VALUE)
												.addComponent(lbContentLop))
								.addGap(18)
								.addGroup(
										groupLayout
												.createParallelGroup(Alignment.BASELINE).addComponent(lbCapcha)
												.addComponent(tfCapcha, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lbDisplayCapcha))
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
										.createSequentialGroup().addGap(26)
										.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
												.addComponent(lbChuongTrinh).addComponent(lbContentChuongTrinh)))
										.addGroup(groupLayout
												.createSequentialGroup().addPreferredGap(ComponentPlacement.RELATED)
												.addComponent(lberrorCapcha)))
								.addPreferredGap(ComponentPlacement.RELATED, 6, Short.MAX_VALUE)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
										.createSequentialGroup().addPreferredGap(ComponentPlacement.UNRELATED)
										.addComponent(btnCapNhatTT).addGap(10).addComponent(lbDisplay).addGap(11)
										.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
												.addComponent(lbTrangThai, GroupLayout.DEFAULT_SIZE,
														GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
												.addComponent(lbContentTrangThai, GroupLayout.PREFERRED_SIZE, 14,
														GroupLayout.PREFERRED_SIZE))
										.addGap(34))
										.addGroup(groupLayout.createSequentialGroup().addGap(24)
												.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
														.addComponent(lbHeHoc, GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
														.addComponent(lbContentHeHoc))
												.addGap(46)))));
		setLayout(groupLayout);

	}

	/**
	 * Hiển thị thông tin sinh viên, set giá trị cho các label hiển thị thông tin
	 * 
	 * @throws SQLException
	 */
	public void LoadData() throws SQLException {

		String taikhoan = MainUI.getInstance().getTaiKhoan();
		SinhVien sinhvien = SinhVienController.getInstance().getUser(taikhoan);
		String capcha = UserHelper.generateCaptchaString();
		lbContentMssv.setText(sinhvien.getTaikhoan());
		lbContentHoten.setText(sinhvien.getHoten());
		lbContentNgaySinh.setText(sinhvien.getNgaysinh());
		lbContentChuongTrinh.setText(sinhvien.getChuongtrinhhoc());
		lbContentHeHoc.setText(sinhvien.getHehoc());
		tfEmail.setText(sinhvien.getEmail());
		tfSoDT.setText(sinhvien.getSdt());
		tfDiaChi.setText(sinhvien.getDiachi());
		lbDisplayCapcha.setText(capcha);
	}

	/**
	 * Xử lý event khi click button 'Cập nhật thông tin' Cập nhật thông tin sinh
	 * viên
	 */
	public void actionPerformed(ActionEvent ae) {

		lbDisplay.setText("");
		// Kiểm tra input
		if (ae.getSource() == btnCapNhatTT) {

			boolean ktEmail = UserHelper.validateEmail(tfEmail.getText());
			int errorEmail = 0;
			if (tfEmail.getText().equals("")) {
				lbErrorEmail.setText("Email không được để trống");
				errorEmail = 1;
			} else if (ktEmail) {
				lbErrorEmail.setText("Email không hợp lệ");
				errorEmail = 1;
			} else {
				lbErrorEmail.setText("");
				errorEmail = 0;
			}

			boolean ktSodt = UserHelper.validatePhoneNumber(tfSoDT.getText().trim());
			int errorSodt = 0;
			if (tfSoDT.getText().equals("")) {
				lbErrorSoDT.setText("Số điện thoại không được để trống");
				errorSodt = 1;
			} else if (ktSodt) {
				lbErrorSoDT.setText("Số điện thoại không hợp lệ");
				errorSodt = 1;
			} else {
				lbErrorSoDT.setText("");
				errorSodt = 0;
			}

			int errorDiachi = 0;
			if (tfDiaChi.getText().equals("")) {
				lbErrorDiaChi.setText("Địa chỉ không được để trống");
				errorDiachi = 1;
			} else {
				lbErrorDiaChi.setText("");
				errorDiachi = 0;
			}

			int errorCapcha = 0;
			if (!tfCapcha.getText().trim().equalsIgnoreCase(lbDisplayCapcha.getText().trim())) {
				lberrorCapcha.setText("Mã Capcha không chính xác");
				lbDisplayCapcha.setText(UserHelper.generateCaptchaString());
				errorCapcha = 1;
			} else {
				lberrorCapcha.setText("");
				errorCapcha = 0;
			}

			// Nếu không có lỗi input, cập nhật thông tin sinh viên

			if (errorDiachi < 1 && errorSodt < 1 && errorEmail < 1 && errorCapcha < 1) {
				String taikhoan = MainUI.getInstance().getTaiKhoan();
				String email = tfEmail.getText();
				String sdt = tfSoDT.getText();
				String diachi = tfDiaChi.getText();
				boolean check = SinhVienController.getInstance().UpdateInfoUser(email, sdt, diachi, taikhoan);
				if (check) {
					lbErrorDiaChi.setText("");
					lbErrorEmail.setText("");
					lbErrorSoDT.setText("");
					lberrorCapcha.setText("");
					lbDisplay.setText("Cập nhật thông tin thành công");
					JOptionPane.showMessageDialog(null, "Update Success");
				} else {
					lbDisplay.setText("Cập nhật thất bại");
					JOptionPane.showMessageDialog(null, "Có lỗi xảy ra");
				}
			}

		}
	}

}
