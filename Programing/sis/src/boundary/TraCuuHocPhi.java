package boundary;

import javax.swing.JPanel;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import common.MainUI;
import controller.HocPhiController;
import entity.ChiTietHocPhi;
import entity.HocPhi;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
 * Class này là lớp biên của chức năng tra cứu học phí
 * 
 * @author TuanNguyen
 *
 */
@SuppressWarnings("serial")
public class TraCuuHocPhi extends JPanel {

	/** bảng hiện thị các môn học kì này */
	private JTable tblHocPhi;

	/** label hiện thị kì này */
	private JLabel lblHocKi;

	/** label hiện hiện thị mã sinh viên */
	private JLabel lblMaSinhVien;

	/** label hiện thị hóa phí */
	private JLabel lblHocPhi;

	/** label hiện thị giá tiền 1 tín chỉ */
	private JLabel lblGiaTienTin;

	public TraCuuHocPhi() {
		initComponent();
		String taikhoan = MainUI.getInstance().getTaiKhoan();
		HocPhi hocphi = searchHocPhi(taikhoan);
		generateHocPhi(hocphi);

	}

	/**
	 * Hàm này để lấy thông tin học phí của tài khoản
	 * 
	 * @param taikhoan
	 *            là tài khoản (mssv) của sinh viên
	 * @return đối tượng học phí bao gồm các môn học và giá tiền
	 */
	private HocPhi searchHocPhi(String taikhoan) {
		HocPhi hocPhi = new HocPhi();
		try {
			hocPhi = HocPhiController.getInstance().getHocPhi(taikhoan);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return hocPhi;
	}

	/**
	 * Hàm này để hiện thị các thông tin học phí lên màn hình
	 * 
	 * @param hocphi
	 *            là thông tin học phí
	 */
	private void generateHocPhi(HocPhi hocphi) {
		lblHocKi.setText("Kì học: " + String.valueOf(hocphi.getHocki()));
		lblMaSinhVien.setText("Mã sinh viên: " + hocphi.getTaikhoan());
		lblHocPhi.setText(String.join(" ", String.valueOf(hocphi.getHocphi()), "VND"));
		lblGiaTienTin.setText(String.join(" ", String.valueOf(hocphi.getGiaTienTinChi()), "VND"));
		List<ChiTietHocPhi> chiTietList = hocphi.getChitiet();
		Vector<String> row, colunm;
		DefaultTableModel model = new DefaultTableModel();
		String[] colunmNames = { "Mã HP", "Tên HP", "Thời lượng", "TC học phần", "TC học phí", "Mã lớp" };
		colunm = new Vector<String>();
		int numberColumn;
		numberColumn = colunmNames.length;
		for (int i = 0; i < numberColumn; i++) {
			colunm.add(colunmNames[i]);
		}
		model.setColumnIdentifiers(colunm);
		for (ChiTietHocPhi chiTietHocPhi : chiTietList) {
			row = new Vector<String>();
			row.add(chiTietHocPhi.getMahp());
			row.add(chiTietHocPhi.getHocphan());
			row.add(chiTietHocPhi.getThoiluong());
			row.add(String.valueOf(chiTietHocPhi.getSoTinChi()));
			row.add(String.valueOf(chiTietHocPhi.getTinChiHocPhi()));
			row.add(chiTietHocPhi.getMalop());
			model.addRow(row);
		}
		tblHocPhi.setModel(model);
		tblHocPhi.setVisible(true);
	}

	/**
	 * Hàm này để khởi tạo các component
	 */
	private void initComponent() {
		lblHocKi = new JLabel("");
		lblMaSinhVien = new JLabel("");
		JScrollPane scrollPane = new JScrollPane();

		tblHocPhi = new JTable();
		scrollPane.setViewportView(tblHocPhi);
		tblHocPhi.setModel(new DefaultTableModel(
				new Object[][] { { null, null, null, null, null, null, null },
						{ null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
						{ null, null, null, null, null, null, null } },
				new String[] { "Mã HP", "Tên HP", "Thời lượng", "TC học phần", "TC học phí", "Mã lớp" }));

		JLabel lblTotal = new JLabel("Tổng số tiền phải đóng: ");

		lblHocPhi = new JLabel("New label");

		JLabel lblGiaTienTinChi = new JLabel("Giá tiền tín chỉ:");

		lblGiaTienTin = new JLabel("0");
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addGap(10)
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(lblMaSinhVien, Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
										GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblHocKi, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 180,
										Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup().addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(lblTotal).addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(lblHocPhi, GroupLayout.PREFERRED_SIZE, 191, GroupLayout.PREFERRED_SIZE))
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 639, GroupLayout.PREFERRED_SIZE)))
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addComponent(lblGiaTienTinChi, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(lblGiaTienTin, GroupLayout.PREFERRED_SIZE, 127, GroupLayout.PREFERRED_SIZE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addComponent(lblHocKi, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE).addGap(11)
				.addComponent(lblMaSinhVien, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE).addGap(11)
				.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 209, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(ComponentPlacement.UNRELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblGiaTienTinChi)
						.addComponent(lblGiaTienTin))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblTotal)
						.addComponent(lblHocPhi, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGap(26)));
		setLayout(groupLayout);
	}
}
