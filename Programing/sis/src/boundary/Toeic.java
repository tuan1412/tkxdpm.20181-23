package boundary;

import javax.swing.JPanel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;
import controller.SinhVienController;
import controller.ToeicController;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.Color;
import java.awt.Font;
import javax.swing.SwingConstants;

/**
 * Class thể hiện View tra cứu kết quả Toeic
 * 
 * @author PhamTuan
 *
 */
public class Toeic extends JPanel implements ActionListener {

	/** TextField nhập mã tài khoản, họ tên sinh viên */
	private JTextField tfMasv, tfHoten;

	/** Label hiển thị thông tin và lỗi */
	private JLabel lbHocKy, lbMasv, lbHoten, lbDisplay, lbErrorMasv;

	/** Button thực hiện tra cứu điểm thi Toeic */
	private JButton btnTraCuuToeic;

	/** comboBox hiện các học kỳ */
	private JComboBox cbBoxHocKy;

	/** table hiển thị kết quả tra cứu điểm Toeic */
	private JTable tbToeic;

	/** ScrollPane chứa table */
	private JScrollPane scrollPane;

	/**
	 * 
	 * Create the panel Toeic.
	 */
	public Toeic() {

		lbHocKy = new JLabel("Chọn học kỳ:");
		lbMasv = new JLabel("Mã SV:");
		lbHoten = new JLabel("hoặc họ tên đầy đủ:");
		lbDisplay = new JLabel("Danh sách kết quả tra cứu");
		lbDisplay.setHorizontalAlignment(SwingConstants.CENTER);
		lbDisplay.setFont(new Font("Tahoma", Font.PLAIN, 12));

		btnTraCuuToeic = new JButton("Tra cứu");
		btnTraCuuToeic.addActionListener(this);
		tfMasv = new JTextField();
		tfMasv.setColumns(10);

		tfHoten = new JTextField();
		tfHoten.setColumns(10);

		String[] hocky = { "Tất cả", "20151", "20152", "20153", "20161", "20162", "20163", "20171", "20172", "20173",
				"20181" };
		cbBoxHocKy = new JComboBox(hocky);

		scrollPane = new JScrollPane();

		lbErrorMasv = new JLabel("");
		lbErrorMasv.setForeground(Color.RED);

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING).addGroup(groupLayout
						.createSequentialGroup().addGap(57)
						.addGroup(groupLayout
								.createParallelGroup(Alignment.LEADING).addComponent(lbHocKy).addComponent(lbMasv))
						.addGap(18)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(tfMasv, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGap(149).addComponent(lbHoten).addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(tfHoten, GroupLayout.PREFERRED_SIZE, 156,
												GroupLayout.PREFERRED_SIZE)
										.addGap(28).addComponent(btnTraCuuToeic))
								.addComponent(cbBoxHocKy, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
								.addComponent(lbErrorMasv))
						.addGap(27))
						.addGroup(Alignment.LEADING,
								groupLayout.createSequentialGroup().addContainerGap()
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(lbDisplay, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE,
														744, Short.MAX_VALUE)
												.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 744,
														Short.MAX_VALUE))))
				.addContainerGap()));
		groupLayout
				.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(38)
								.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
										.addComponent(cbBoxHocKy, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lbHocKy))
								.addGap(28)
								.addGroup(
										groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lbMasv)
												.addComponent(tfMasv, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(lbHoten)
												.addComponent(tfHoten, GroupLayout.PREFERRED_SIZE,
														GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
												.addComponent(btnTraCuuToeic))
								.addGap(29).addComponent(lbErrorMasv).addGap(35).addComponent(lbDisplay)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
								.addContainerGap(49, Short.MAX_VALUE)));
		setLayout(groupLayout);
	}

	/**
	 * Thực hiện xử lý event khi click button 'tra cứu' điểm thi toeic Hiển thị kết
	 * quả bảng tra cứu điểm thi
	 */
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == btnTraCuuToeic) {

			String col[] = { "Mã SV", "Họ tên", "Ngày sinh", "Học kỳ", "Ngày thi", "Điểm nghe", "Điểm đọc", "Điểm tổng",
					"Ghi chú" };
			DefaultTableModel tableModel = new DefaultTableModel(col, 0);

			tbToeic = new JTable();
			tbToeic.setModel(tableModel);
			scrollPane.setViewportView(tbToeic);

			lbErrorMasv.setText("");
			int error = 0;

			String taikhoan = tfMasv.getText();
			// Kiểm tra input
			String hoten = tfHoten.getText().trim();
			// check if both of mssv and hoten are blank
			if (taikhoan.equals("") && hoten.equals("")) {
				lbErrorMasv.setText("---Nhập vào mã sinh viên hoặc họ tên---");
				error = 1;
			}
			if (!taikhoan.equals("")) {
				try {
					int mssv = Integer.parseInt(taikhoan);
				} catch (NumberFormatException ex) {
					lbErrorMasv.setText("---Mã số sinh viên không hợp lệ---");
					error = 1;
				}

			}

			// Nếu không có lỗi input, thực hiện hiển thị kết quả tra cứu
			if (error < 1) {
				String hocky = (String) cbBoxHocKy.getSelectedItem();

				List<entity.Toeic> list = ToeicController.getInstance().getListToeic(hocky, taikhoan, hoten);

				for (int i = 0; i < list.size(); i++) {
					int masinhvien = list.get(i).getTaikhoan();
					String tk = Integer.toString(masinhvien);
					String ht = SinhVienController.getInstance().getNameSV(tk);
					String ngaysinh = SinhVienController.getInstance().getNgaySinhSV(tk);
					String hk = list.get(i).getHocki();
					String ngaythi = list.get(i).getNgaythi();
					int diemnghe = list.get(i).getDiemnghe();
					int diemdoc = list.get(i).getDiemdoc();
					int diemtong = list.get(i).getDiemtong();
					String ghichu = list.get(i).getGhichu();

					Object[] data = { masinhvien, ht, ngaysinh, hk, ngaythi, diemnghe, diemdoc, diemtong, ghichu };

					tableModel.addRow(data);
				}

			}
		}

	}
}
