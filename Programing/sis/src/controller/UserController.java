package controller;

import java.sql.SQLException;

import entity.User;

/**
 * Class này để điều khiển các chức năng liên quan tới user
 * 
 * @author TuanNguyen
 *
 */
public class UserController {

	/** user để quản lý thông tin về user */
	User user = new User();

	/** đối tượng khởi tạo để các đối tượng bên ngoài đều dùng chung */
	private static UserController intance;

	/**
	 * Hàm khởi tạo là private để không đối tượng nào bên ngoài có thể khởi tạo tuỳ
	 * ý đối tượng
	 */
	private UserController() {
	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của UserController nếu đối
	 * tượng đố null
	 * 
	 * @return đối tượng dùng chung duy nhất của UserController
	 */
	public static UserController getInstance() {
		if (intance == null)
			intance = new UserController();
		return intance;
	}

	/**
	 * Hàm này để kiểm tra đăng nhập với tài khoản và mật khẩu
	 * 
	 * @param taikhoan
	 *            là tài khoản nhập vào
	 * @param matkhau
	 *            là mật khẩu nhập vào
	 * @return đối tượng user nếu tồn tại, null nếu đối tượng không tồn tại
	 * @throws SQLException
	 */
	public User doLogin(String taikhoan, String matkhau) {
		try {
            return user.searchUser(taikhoan, matkhau);
        } catch (SQLException e) {
            return null;
        }
	}

	/**
	 * Hàm này để thay đổi mật khẩu với tài khoản và mật khẩu
	 * 
	 * @param taikhoan
	 *            là tài khoản của user
	 * @param matkhaumoi
	 *            là mật khẩu mới
	 * @throws SQLException
	 */
	public boolean changePassword(String taikhoan, String matkhaumoi) {
		User user = new User();
		try {
			user.updatePassword(taikhoan, matkhaumoi);
			return true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}
}
