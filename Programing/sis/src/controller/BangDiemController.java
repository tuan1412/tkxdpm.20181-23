package controller;

import java.sql.SQLException;

import entity.BangDiem;

/**
 * Class này để điều khiển các chức năng liên quan tới bảng điểm
 * 
 * @author TungNguyen
 *
 */
public class BangDiemController {

	/** bảng điểm để quản lý thông tin bảng điểm */
	BangDiem bangdiem = new BangDiem();

	/** đối tượng BangDiemController dùng chung */
	private static BangDiemController instance;

	/**
	 * Hàm khởi tạo để private để các đối tượng bên ngoài không thể tùy ý khởi tạo
	 */
	private BangDiemController() {

	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của BangDiemController nếu
	 * đối tượng đố null
	 * 
	 * @return đối tượng BangDiemController dùng chung
	 */
	public static BangDiemController getInstance() {
		if (instance == null)
			instance = new BangDiemController();
		return instance;
	}

	/**
	 * Hàm này để lấy thông tin bảng điểm của sinh viên
	 * 
	 * @param taikhoan
	 *            là tài khoản
	 * @return thông tin bảng điểm
	 * @throws SQLException
	 */
	public BangDiem getBangDiem(String taikhoan) throws SQLException {
		return bangdiem.searchBangDiem(taikhoan);
	}
}
