package controller;

import java.sql.SQLException;

import entity.HocPhan;

/**
 * Class này để điều khiển các chức năng liên quan tới học phần
 * 
 * @author TungNguyen
 *
 */
public class HocPhanController {
	/** hocphan để quản lý thông tin học phần */
	HocPhan hocphan = new HocPhan();

	/** đối tượng HocPhanController dùng chung */
	private static HocPhanController instance;

	/**
	 * Hàm khởi tạo để private để các đối tượng bên ngoài không thể tùy ý khởi tạo
	 */
	private HocPhanController() {
	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của HocPhanController nếu
	 * đối tượng đó null
	 * 
	 * @return đối tượng HocPhanController dùng chung
	 */
	public static HocPhanController getInstance() {
		if (instance == null)
			instance = new HocPhanController();
		return instance;
	}

	/**
	 * Hàm này để lấy thông tin của học phần
	 * 
	 * @param maHP
	 *            là mã học phần của học phần
	 * @return thông tin của học phần
	 * @throws SQLException
	 */
	public HocPhan getHocPhan(String maHP) throws SQLException {
		HocPhan hocphan = new HocPhan();
		return hocphan.searchHocPhan(maHP);
	}

}
