package controller;

import java.sql.SQLException;
import java.util.List;
import entity.DangKiHocPhan;

/**
 * Class này để điều khiển các chức năng liên quan tới đăng kí học
 * 
 * @author TungNguyen
 *
 */
public class DangKiHocController {

	/** đăng kí học phần để quản lý thông tin về đăng kí học phần */
	DangKiHocPhan dangkihocphan = new DangKiHocPhan();

	/** đối tượng DangKiHocController dùng chung */
	private static DangKiHocController instance;

	/**
	 * Hàm khởi tạo để private để các đối tượng bên ngoài không thể tùy ý khởi tạo
	 */
	private DangKiHocController() {

	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của DangKiHocController
	 * nếu đối tượng đố null
	 * 
	 * @return đối tượng DangKiHocController dùng chung
	 */
	public static DangKiHocController getInstance() {
		if (instance == null)
			instance = new DangKiHocController();
		return instance;
	}

	/**
	 * Hàm này lấy danh sách đăng kí học phần theo kì học và tài khoản
	 * 
	 * @param kihoc
	 *            là kì học
	 * @param taikhoan
	 *            là tài khoản
	 * @return danh sách đăng kí học phần
	 * @throws SQLException
	 */
	public List<DangKiHocPhan> getDangKiHocPhan(String kihoc, String taikhoan) throws SQLException {
		DangKiHocPhan dangkihocphan = new DangKiHocPhan();
		return dangkihocphan.selectDangKiHP(kihoc, taikhoan);
	}

	/**
	 * Hàm này để thêm học phần đăng kí của đăng kí học
	 * 
	 * @return true nếu cập nhật thành công, false nếu thất bại
	 */
	public boolean insertDangKiHocPhan(DangKiHocPhan dangkihocphan) {
		try {
			dangkihocphan.insertDangKiHP();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	/**
	 * Hàm này để xóa học phần đã đăng kí của đăng kí học
	 * 
	 * @return true nếu cập nhật thành công, false nếu thất bại
	 */
	public boolean deleteDangKiHocPhan(DangKiHocPhan dangkihocphan) {
		try {
			dangkihocphan.deleteDKHP();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}
}
