package controller;

import java.sql.SQLException;
import java.util.List;

import entity.HocPhi;
import entity.ThongTinKiHoc;

/**
 * Class này để điều khiển các chức năng liên quan tới học phí
 * 
 * @author TuanNguyen
 *
 */
public class HocPhiController {

	/** hocphi để quản lý thông tin học phí của sinh viên */
	HocPhi hocphi = new HocPhi();

	/** thongtinkihoc để quản lý học phí theo kì */
	ThongTinKiHoc thongTinKiHoc = new ThongTinKiHoc();

	/** đối tượng HocPhiController dùng chung */
	private static HocPhiController intance;

	/**
	 * Hàm khởi tạo để private để các đối tượng bên ngoài không thể tùy ý khởi tạo
	 */
	private HocPhiController() {
	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của HocPhiController nếu
	 * đối tượng đố null
	 * 
	 * @return đối tượng HocPhiController dùng chung
	 */
	public static HocPhiController getInstance() {
		if (intance == null)
			intance = new HocPhiController();
		return intance;
	}

	/**
	 * Hàm này để lấy thông tin học phí của sinh viên kì mới nhất
	 * 
	 * @param taikhoan
	 *            là tài khoản (mssv) của sinh viên
	 * @return thông tin học phí
	 * @throws SQLException
	 */
	public HocPhi getHocPhi(String taikhoan) throws SQLException {
		return hocphi.searchHocPhi(taikhoan);
	}

	/**
	 * Hàm này để lấy thông tin các kì học
	 * 
	 * @return thông tin của các kì học
	 * @throws SQLException
	 */
	public List<ThongTinKiHoc> getHocPhiKiHoc() throws SQLException {
		return thongTinKiHoc.getKiHoc();
	}

	/**
	 * Hàm này để cập nhật hoặc thêm mới học phí
	 * 
	 * @param kihoc
	 *            là kì học cần cập nhật hoặc thêm mới
	 * @param hocPhi
	 *            là học phí của kì học đó
	 * @return true nếu cập nhật hoặc thêm mới thành công, false nếu thất bại
	 * @throws SQLException
	 */
	public boolean insertOrUpdateHocPhi(int kihoc, long hocPhi) throws SQLException {
		if (thongTinKiHoc.getOne(kihoc) != null) {
			return thongTinKiHoc.updateHocPhi(kihoc, hocPhi);
		}
		return thongTinKiHoc.insertHocPhi(kihoc, hocPhi);
	}
}
