###Home work: Programing

###Assignment of each member
Nguyễn Anh Tuấn

- Lập trình uc Đăng nhập, Tra cứu học phí, Cập nhật điểm sinh viên 100%

- Method desgin: 100%

- Comment code: 100%

- Áp dụng view vào các usecase: 100%

Phạm Văn Tuấn

- Lập trình uc Xem thời khóa biểu, Tra cứu điểm Toeic, Cập nhật thông tin cá nhân người dùng: 100%

Nguyễn Hữu Tùng

- Lập trình uc Đổi mật khẩu, Tra cứu bảng điểm cá nhân, Đăng kí học phần: 100%

Làm việc nhóm
- Refactor code áp dụng Design Patterns và Design Principles.

###Review
Nguyễn Anh Tuấn review Nguyễn Hữu Tùng

Nguyễn Hữu Tùng review Phạm Văn Tuấn

Phạm Văn Tuấn review Nguyễn Anh Tuấn