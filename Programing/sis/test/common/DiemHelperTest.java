package common;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

class DiemHelperTest {

	@Test
	void testRound() {
		assertThrows(IllegalArgumentException.class, () -> DiemHelper.round(3.534f, -2));

		BigDecimal actualTc1 = DiemHelper.round(3.534f, 0);
		assertEquals("4", actualTc1.toString());

		BigDecimal actualTc2 = DiemHelper.round(3.534f, 2);
		assertEquals("3.53", actualTc2.toString());

		BigDecimal actualTc3 = DiemHelper.round(3.534f, 3);
		assertEquals("3.534", actualTc3.toString());

		BigDecimal actualTc4 = DiemHelper.round(3.534f, 4);
		assertEquals("3.5340", actualTc4.toString());
	}

	@Test
	void testRoundNearestFive() {
		assertEquals(4.0, DiemHelper.roundNearestFive(4f));
		assertEquals(4.0, DiemHelper.roundNearestFive(4.1f));
		assertEquals(4.5, DiemHelper.roundNearestFive(4.25f));
		assertEquals(4.5, DiemHelper.roundNearestFive(4.35f));
		assertEquals(5.0, DiemHelper.roundNearestFive(4.75f));
		assertEquals(5.0, DiemHelper.roundNearestFive(4.8f));
	}

	@Test
	void testParseToDiemChu() {
		assertThrows(IllegalArgumentException.class, () -> DiemHelper.parseToDiemChu(-2f));
		assertEquals("F", DiemHelper.parseToDiemChu(0f));
		assertEquals("F", DiemHelper.parseToDiemChu(3.95f));
		assertEquals("D", DiemHelper.parseToDiemChu(4f));
		assertEquals("D", DiemHelper.parseToDiemChu(4.25f));
		assertEquals("D+", DiemHelper.parseToDiemChu(4.95f));
		assertEquals("D+", DiemHelper.parseToDiemChu(5.2f));
		assertEquals("C", DiemHelper.parseToDiemChu(5.45f));
		assertEquals("C", DiemHelper.parseToDiemChu(6f));
		assertEquals("C+", DiemHelper.parseToDiemChu(6.45f));
		assertEquals("C+", DiemHelper.parseToDiemChu(6.6f));
		assertEquals("B", DiemHelper.parseToDiemChu(6.95f));
		assertEquals("B", DiemHelper.parseToDiemChu(7.93f));
		assertEquals("B+", DiemHelper.parseToDiemChu(7.95f));
		assertEquals("B+", DiemHelper.parseToDiemChu(8.42f));
		assertEquals("A", DiemHelper.parseToDiemChu(8.45f));
		assertEquals("A", DiemHelper.parseToDiemChu(9.3f));
		assertEquals("A", DiemHelper.parseToDiemChu(9.3f));
		assertEquals("A+", DiemHelper.parseToDiemChu(9.45f));
		assertEquals("A+", DiemHelper.parseToDiemChu(9.75f));
		assertEquals("A+", DiemHelper.parseToDiemChu(10f));
		assertThrows(IllegalArgumentException.class, () -> DiemHelper.parseToDiemChu(11f));
	}

	@Test
	void testParseToDiemSo() {
		assertThrows(IllegalArgumentException.class, () -> DiemHelper.parseToDiemSo("aaa"));
		assertEquals(4f, DiemHelper.parseToDiemSo("A+"));
		assertEquals(4f, DiemHelper.parseToDiemSo("A"));
		assertEquals(3.5f, DiemHelper.parseToDiemSo("B+"));
		assertEquals(3f, DiemHelper.parseToDiemSo("B"));
		assertEquals(2.5f, DiemHelper.parseToDiemSo("C+"));
		assertEquals(2f, DiemHelper.parseToDiemSo("C"));
		assertEquals(1.5f, DiemHelper.parseToDiemSo("D+"));
		assertEquals(1f, DiemHelper.parseToDiemSo("D"));
		assertEquals(0f, DiemHelper.parseToDiemSo("F"));
	}

}
