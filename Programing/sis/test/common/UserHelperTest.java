/**
 * 
 */
package common;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class UserHelperTest {

	@Test
	public void testValidateEmail() {
		// return true if email is invalid
		assertEquals(true, UserHelper.validateEmail("#$abc123@gmail.com"));
		assertEquals(true, UserHelper.validateEmail("Ab123cd.com.vn"));
		assertEquals(true, UserHelper.validateEmail("123abc,vn@gmail.com"));
		assertEquals(true, UserHelper.validateEmail("Bd123_vn@gmailvn"));
		assertEquals(true, UserHelper.validateEmail("Vdbb12.vn@gmail.c"));

		assertEquals(false, UserHelper.validateEmail("12abc_yahoo.com@hust.edu.vn"));
	}

	@Test
	public void testValidatePhoneNumber() {
		// return true if phone number is invalid
		assertEquals(true, UserHelper.validatePhoneNumber("Abc123456"));
		assertEquals(true, UserHelper.validatePhoneNumber("01234567"));
		assertEquals(true, UserHelper.validatePhoneNumber("012345678999"));
		assertEquals(true, UserHelper.validatePhoneNumber("1234567895"));
		assertEquals(true, UserHelper.validatePhoneNumber("0166 843 1234"));

		assertEquals(false, UserHelper.validatePhoneNumber("0371234567"));

	}

}
