package controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import entity.ChiTietLopHoc;
import entity.LopHoc;

class LopHocControllerTest {

	private static LopHoc lopHoc;
	private static List<ChiTietLopHoc> chiTietList;

	private boolean compareList(List<ChiTietLopHoc> list1, List<ChiTietLopHoc> list2) {
		if (list1.size() != list2.size())
			return false;
		int length = list1.size();
		for (int i = 0; i < length; i++) {
			if (!list1.get(i).getTenSinhVien().trim().equals(list2.get(i).getTenSinhVien().trim()))
				return false;
		}
		return true;
	}

	@BeforeAll
	static void before() {
		lopHoc = new LopHoc();
		lopHoc.setMalop("100000");
		lopHoc.setTrongSo(0.7f);
		ChiTietLopHoc ct1 = new ChiTietLopHoc("20151101", "Nguyễn Văn An", 8f, 8f);
		ChiTietLopHoc ct2 = new ChiTietLopHoc("20150672", "Mai Tiến Dũng", 6f, 9f);
		ChiTietLopHoc ct3 = new ChiTietLopHoc("20154101", "Nguyễn Anh Tuấn", 8f, 7f);
		chiTietList = Arrays.asList(ct1, ct2, ct3);
	}

	@Test
	void testSearchLopHoc() {
		assertEquals(null, LopHocController.getInstance().searchLopHoc(""));

		List<ChiTietLopHoc> actual = LopHocController.getInstance().searchLopHoc("100000").getChiTietList();
		assertTrue(this.compareList(chiTietList, actual));

	}

	@Test
	void testUpdateDiemLopHoc() {
		assertFalse(LopHocController.getInstance().updateDiemLopHoc(null, lopHoc));
		assertFalse(LopHocController.getInstance().updateDiemLopHoc(chiTietList, null));
		assertTrue(LopHocController.getInstance().updateDiemLopHoc(chiTietList, lopHoc));
	}

}
