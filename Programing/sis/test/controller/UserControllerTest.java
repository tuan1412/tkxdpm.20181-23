package controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

import common.UserHelper;
import entity.User;

class UserControllerTest {
    boolean checkEqualUser(User user1, User user2) {
       if (user1 == null || user2 == null) return false;
       return user1.getTaikhoan().equals(user2.getTaikhoan()) 
            && user1.getMatkhau().equals(user2.getMatkhau())
            && user1.getRole().equals(user2.getRole());
    }
    @Test
    void testDoLogin() {
        User user = new User();
        user.setTaikhoan("20154101");
        user.setMatkhau(UserHelper.MD5encrypt("12345678"));
        user.setRole(0);
        String hashPass = UserHelper.MD5encrypt("12345678");
        
        assertEquals(null, UserController.getInstance().doLogin(null, null));
        assertEquals(null, UserController.getInstance().doLogin("20154101", null));
        assertEquals(null, UserController.getInstance().doLogin(null, "12345678"));
        assertEquals(null, UserController.getInstance().doLogin("20154101", "123456789"));
        assertTrue(checkEqualUser(user, UserController.getInstance().doLogin("20154101", hashPass)));
        assertEquals(null, UserController.getInstance().doLogin("20154101", ""));
        assertEquals(null, UserController.getInstance().doLogin("", hashPass));
    }

	@Test
	void testChangePassword() {
		assertEquals(true, UserController.getInstance().changePassword("20154485", "20021997"));
	}

}
