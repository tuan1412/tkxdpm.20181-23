package boundary;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import common.DiemHelper;
import controller.LopHocController;
import entity.ChiTietLopHoc;
import entity.LopHoc;

/**
 * Class này là lớp biên của usecase Cập nhật điểm sinh viên
 * 
 * @author TuanNguyen
 *
 */
@SuppressWarnings("serial")
public class CapNhatDiem extends JPanel {

	/** TextField để nhập mã lớp */
	private JTextField tfMaLop;

	/** Table để hiện thị danh dách lớp */
	private JTable tblLopHoc;

	/** Label hiện thị tên học phần */
	private JLabel lblTenHocPhan;

	/** Label hiện thị kì học */
	private JLabel lblKiHoc;

	/** Lớp học hiện thị */
	private LopHoc lophoc;

	public CapNhatDiem() {
		initComponent();

	};

	/**
	 * Hàm này để hiện thị lại lớp học lên table
	 */
	private void generateLopHoc() {
		String maLop = tfMaLop.getText();
		if (maLop.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Mã lớp không được để trống");
			return;
		}
		this.lophoc = LopHocController.getInstance().searchLopHoc(maLop);
		if (this.lophoc == null || this.lophoc.getId() == 0) {
			JOptionPane.showMessageDialog(this, "Không tồn tại mã lớp này");
			// Để xóa dữ liệu trên bảng
			this.lophoc = new LopHoc();
			this.lophoc.setId(0);
			this.lophoc.setTenhp("");
			this.lophoc.setKihoc("");
			this.lophoc.setChiTietList(new ArrayList<>());
		}

		lblTenHocPhan.setText("Học phần: " + this.lophoc.getTenhp());
		lblKiHoc.setText("Kì học: " + this.lophoc.getKihoc());

		List<ChiTietLopHoc> chiTietList = this.lophoc.getChiTietList();

		int count = 1;
		String[] columnNames = { "STT", "MSSV", "Họ tên", "Điểm QT", "Điểm thi", "Điểm chữ" };
		List<Object[]> data = new ArrayList<>();

		for (ChiTietLopHoc chitiet : chiTietList) {
			data.add(new Object[] { count, chitiet.getMaSinhVien(), chitiet.getTenSinhVien(), chitiet.getDiemQt(),
					chitiet.getDiemThi(), chitiet.getDiemChu() });
			count++;

		}
		tblLopHoc.setModel(new MyTableModel(columnNames, data));
		tblLopHoc.setVisible(true);

	};

	/**
	 * Hàm này để lưu bảng điểm đã thay đổi
	 */
	private void saveBangDiem() {
		List<ChiTietLopHoc> chiTietList = new ArrayList<>();
		TableModel model = tblLopHoc.getModel();
		int rowCount = model.getRowCount();
		if (rowCount == 0) {
			JOptionPane.showMessageDialog(this, "Danh sách lớp rỗng");
			return;
		}
		for (int i = 0; i < rowCount; i++) {

			String maSinhVien = (String) model.getValueAt(i, 1);
			String tenSinhVien = (String) model.getValueAt(i, 2);
			float diemQt = Float.parseFloat(model.getValueAt(i, 3).toString());
			float diemThi = Float.parseFloat(model.getValueAt(i, 4).toString());

			ChiTietLopHoc chiTietLop = new ChiTietLopHoc(maSinhVien, tenSinhVien, diemQt, diemThi);
			chiTietList.add(chiTietLop);
		}
		boolean saveRs = LopHocController.getInstance().updateDiemLopHoc(chiTietList, this.lophoc);
		if (saveRs) {
			JOptionPane.showMessageDialog(this, "Lưu thành công");
			generateLopHoc();

		} else {
			JOptionPane.showMessageDialog(this, "Lưu không thành công");
		}
	}

	/**
	 * Hàm này để khởi tạo các component
	 */
	private void initComponent() {
		JLabel lblMaLop = new JLabel("Nhập mã lớp");

		tfMaLop = new JTextField();
		tfMaLop.setColumns(10);

		JButton btnTimKiem = new JButton("Tìm kiếm");

		tfMaLop.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					generateLopHoc();
				}
			}
		});

		btnTimKiem.addActionListener(e -> {
			generateLopHoc();
		});

		JScrollPane scrollPane = new JScrollPane();

		tblLopHoc = new JTable();
		tblLopHoc.setModel(new DefaultTableModel(new Object[][] {},
				new String[] { "STT", "MSSV", "Họ tên", "Điểm QT", "Điểm Thi", "Điểm chữ" }));

		scrollPane.setViewportView(tblLopHoc);

		lblTenHocPhan = new JLabel("Tên học phần");

		lblKiHoc = new JLabel("Kì học");

		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(e -> {
			saveBangDiem();
		});

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addContainerGap()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addGroup(groupLayout.createSequentialGroup()
								.addComponent(lblMaLop, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.UNRELATED)
								.addComponent(tfMaLop, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnTimKiem))
						.addComponent(lblTenHocPhan, GroupLayout.PREFERRED_SIZE, 363, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblKiHoc, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnSave, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 609, GroupLayout.PREFERRED_SIZE))
				.addContainerGap(148, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addContainerGap()
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lblMaLop)
								.addComponent(tfMaLop, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(btnTimKiem))
						.addPreferredGap(ComponentPlacement.RELATED).addComponent(lblTenHocPhan)
						.addPreferredGap(ComponentPlacement.UNRELATED).addComponent(lblKiHoc).addGap(18)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE)
						.addGap(18).addComponent(btnSave).addGap(26)));
		setLayout(groupLayout);
	}

	/**
	 * Class này để xác định model cho bảng hiện thị lớp học Class sẽ xác định cột
	 * nào được sửa và validate trường nhập vào
	 * 
	 * @author TuanNguyen
	 *
	 */
	class MyTableModel extends AbstractTableModel {

		/** tên các cột của bảng */
		private String[] columnNames;

		/** data của bảng */
		private List<Object[]> data;

		public MyTableModel(String[] columnNames, List<Object[]> data) {
			super();
			this.columnNames = columnNames;
			this.data = data;

		}

		/**
		 * Hàm này để xác định class tại một cột của bảng
		 * 
		 * @param columnIndex
		 *            là cột cần xác đinh
		 * @return class tương ứng
		 */
		@Override
		public Class<?> getColumnClass(int columnIndex) {
			return getValueAt(0, columnIndex).getClass();
		}

		/**
		 * Hàm này để xác định tiêu đề của cột
		 * 
		 * @param column
		 *            là cột cần xác đinh
		 * @return tiêu đề cột tương ứng
		 */
		@Override
		public String getColumnName(int column) {
			return columnNames[column];
		}

		/**
		 * Hàm này để xác định số cột
		 * 
		 * @return số cột của bảng
		 */
		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		/**
		 * Hàm này để xác định số hàng
		 * 
		 * @return số hàng của bảng
		 */
		@Override
		public int getRowCount() {
			return data.size();
		}

		/**
		 * Hàm này để lấy giá trị của bảng tại dòng và cột tương ứng
		 * 
		 * @param row
		 *            là hàng của giá trị cần lấy
		 * @param col
		 *            là cột của giá trị cần lấy
		 * @return giá trị cần lấy
		 */
		@Override
		public Object getValueAt(int row, int col) {
			return data.get(row)[col];
		}

		/**
		 * Hàm này để xác định cột nào, dòng nào được sửa
		 * 
		 * @param rowIndex
		 *            dòng cần sửa
		 * @param columnIndex
		 *            cột cần sửa
		 * @return true nếu vị trí đó được sửa, false nếu không được sửa
		 */
		@Override
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return columnIndex == 3 || columnIndex == 4;
		}

		/**
		 * Hàm này để gán giá trị vào bảng, validate trường nhập vào
		 * 
		 * @param value
		 *            là giá trị cần gán
		 * @param row
		 *            là vị trí dòng cần gán
		 * @param col
		 *            là vị trí cột cần gán
		 */
		@Override
		public void setValueAt(Object value, int row, int col) {
			try {
				float newVal = Float.parseFloat(value.toString());
				if (newVal < 0 || newVal > 10) {
					JOptionPane.showMessageDialog(null, "Dữ liệu vào không đúng");
				} else {
					data.get(row)[col] = DiemHelper.roundNearestFive(newVal);
				}
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(null, "Dữ liệu vào không đúng");
			}
		}
	}
}
