package boundary;

import java.sql.SQLException;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import controller.HocPhiController;
import entity.ThongTinKiHoc;

/**
 * Class này là lớp biên của chức năng cập nhật học phí
 * 
 * @author TuanNguyen
 *
 */
@SuppressWarnings("serial")
public class CapNhatHocPhi extends JPanel {

	/** Bảng hiện thị học phí */
	private JTable tblKiHoc;

	public CapNhatHocPhi() {
		initComponent();
		generateHocPhi();

	}

	/**
	 * Hàm này để hiện thị học phí các năm qua bảng
	 */
	public void generateHocPhi() {
		try {
			List<ThongTinKiHoc> kihocs = HocPhiController.getInstance().getHocPhiKiHoc();
			String[] colunmNames = { "Kì học", "Giá tiền tín chỉ" };

			DefaultTableModel model = new DefaultTableModel(null, colunmNames);
			for (ThongTinKiHoc kihoc : kihocs) {
				model.addRow(new Object[] { kihoc.getKihoc(), kihoc.getGiaTienTinChi() });
			}

			tblKiHoc.setModel(model);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Hàm này để khởi tạo các component
	 */
	private void initComponent() {
		JScrollPane scrollPane = new JScrollPane();

		tblKiHoc = new JTable();
		scrollPane.setViewportView(tblKiHoc);

		tblKiHoc.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "Kì hoc", "Giá tiền tín chỉ" }));

		JButton btnSuaHocPhi = new JButton("Sửa học phí");
		btnSuaHocPhi.addActionListener(e -> {
			suaHocPhi();
		});

		JButton btnThemHocPhi = new JButton("Thêm học phí");
		btnThemHocPhi.addActionListener(e -> {
			themHocPhi();
		});

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addGap(10).addComponent(scrollPane,
										GroupLayout.PREFERRED_SIZE, 343, GroupLayout.PREFERRED_SIZE))
								.addGroup(groupLayout.createSequentialGroup().addContainerGap()
										.addComponent(btnThemHocPhi).addGap(16).addComponent(btnSuaHocPhi)))
						.addContainerGap(97, Short.MAX_VALUE)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(41)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 181, GroupLayout.PREFERRED_SIZE)
						.addGap(18).addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnSuaHocPhi).addComponent(btnThemHocPhi))
						.addContainerGap(37, Short.MAX_VALUE)));
		setLayout(groupLayout);

	}
	/**
	 * Hàm này để sửa thông tin học phí
	 */
	private void suaHocPhi() {
		if (tblKiHoc.getSelectedRow() != -1) {
			int kihoc = Integer.parseInt(tblKiHoc.getValueAt(tblKiHoc.getSelectedRow(), 0).toString());
			long hocphi = Long.parseLong(tblKiHoc.getValueAt(tblKiHoc.getSelectedRow(), 1).toString());
			HocPhiForm hocPhiForm = new HocPhiForm(this, kihoc, hocphi);
			hocPhiForm.setVisible(true);
		} else {
			JOptionPane.showMessageDialog(null, "Bạn phải chọn một hàng");
		}
	}
	/**
	 * Hàm này để thêm thông tin học phí
	 */
	private void themHocPhi() {
		HocPhiForm hocPhiForm = new HocPhiForm(this, 0, 0l);
		hocPhiForm.setVisible(true);
	}
}
