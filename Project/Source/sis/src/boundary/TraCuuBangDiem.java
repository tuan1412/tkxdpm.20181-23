package boundary;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.table.DefaultTableModel;
import common.DiemHelper;
import common.MainUI;
import controller.BangDiemController;
import controller.HocPhanController;
import controller.SinhVienController;
import entity.BangDiem;
import entity.HocPhan;
import entity.SinhVien;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JSeparator;
import java.util.List;
import java.util.Vector;
import java.awt.Font;
import java.sql.SQLException;

/**
 * Class này là lớp biên của chức năng tra cứu bảng điểm
 * 
 * @author TungNguyen
 *
 */
public class TraCuuBangDiem extends JPanel {

	/** JTable bảng điểm */
	private JTable tblBangDiem;

	/** Label hiển thị thông tin và lỗi */
	public JLabel lbMaSV, lbHoTen, lbNgaySinh, lbLop, lbChuongTrinh, lbHeHoc, lbTrangThai, lblContentMaSV,
			lblContentHoTen, lblContentNgaySinh, lblContentLop, lblContentChuongTrinh, lblContentHeHoc,
			lblContentTrangThai;

	DefaultTableModel model = new DefaultTableModel();
	JScrollPane scrollPane;
	private JLabel lblKtQuHc;
	private JTable tblKetQuaHocTap;

	/**
	 * Create the panel.
	 * 
	 * @throws SQLException
	 */
	public TraCuuBangDiem() throws SQLException {
		initComponents();
		String taikhoan = MainUI.getInstance().getTaiKhoan();
		BangDiem bangdiem = searchBangDiem(taikhoan);
		generateBangDiem(bangdiem);
		LoadData();
	}

	/**
	 * Hàm này để lấy thông tin bảng điểm của tài khoản
	 * 
	 * @param taikhoan
	 *            là tài khoản
	 * @return đối tượng bảng điểm
	 */
	private BangDiem searchBangDiem(String taikhoan) {
		BangDiem bangDiem = new BangDiem();
		try {
			bangDiem = BangDiemController.getInstance().getBangDiem(taikhoan);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bangDiem;
	}

	/**
	 * Hàm này để hiện thị các thông tin bảng điểm lên màn hình
	 * 
	 * @param bangdiem
	 *            là thông tin bảng điểm
	 */
	public void generateBangDiem(BangDiem bangdiem) {
		List<BangDiem> chiTietList = bangdiem.getChiTietList();
		Vector<String> row, colunm;
		DefaultTableModel model = new DefaultTableModel();
		String[] colunmNames = { "Học Kỳ", "Mã HP", "Tên HP", "TC", "Lớp học", "Điểm QT", "Điểm thi", "Điểm chữ" };
		colunm = new Vector<String>();
		int numberColumn;
		numberColumn = colunmNames.length;
		for (int i = 0; i < numberColumn; i++) {
			colunm.add(colunmNames[i]);
		}
		model.setColumnIdentifiers(colunm);
		for (BangDiem chiTietBangDiem : chiTietList) {
			row = new Vector<String>();
			row.add(String.valueOf(chiTietBangDiem.getKihoc()));
			row.add(chiTietBangDiem.getMahp());
			row.add(chiTietBangDiem.getTenhp());
			row.add(String.valueOf(chiTietBangDiem.getSotinchi()));
			row.add(chiTietBangDiem.getMalop());
			row.add(String.valueOf(chiTietBangDiem.getDiemqt()));
			row.add(String.valueOf(chiTietBangDiem.getDiemthi()));

			try {
				HocPhan hocPhan = HocPhanController.getInstance().getHocPhan(chiTietBangDiem.getMahp());
				float trongSo = hocPhan.getTrongSo();
				float diemtb = chiTietBangDiem.getDiemqt() * (1 - trongSo) + chiTietBangDiem.getDiemthi() * trongSo;
				row.add(DiemHelper.parseToDiemChu(diemtb));
			} catch (SQLException e) {
				// TODO
			}

			model.addRow(row);
		}
		tblBangDiem.setModel(model);
		tblBangDiem.setVisible(true);
	}

	//
	public void KetQuanHocTap() {
		Vector<String> row, column;
		DefaultTableModel model = new DefaultTableModel();
		String[] colunmNames = { "Học Kỳ", "GPA", "CPA", "TC qua", "TC tích lũy", "TC nợ ĐK", "TC ĐK", "Trình độ" };
		column = new Vector<String>();
		int numberColumn;
		numberColumn = colunmNames.length;
		for (int i = 0; i < numberColumn; i++) {
			column.add(colunmNames[i]);
		}
		model.setColumnIdentifiers(column);
	}

	//

	/**
	 * Hàm này để khởi tạo các component
	 */
	public void initComponents() {
		JScrollPane scrollPane = new JScrollPane();

		JLabel lblBngimSinh = new JLabel("Bảng điểm sinh viên");
		lblBngimSinh.setFont(new Font("Tahoma", Font.PLAIN, 14));

		lbMaSV = new JLabel("Mã sinh viên:");

		lbHoTen = new JLabel("Họ tên SV:");

		lbNgaySinh = new JLabel("Ngày sinh:");

		lbLop = new JLabel("Lớp:");

		lbChuongTrinh = new JLabel("Chương trình:");

		lbHeHoc = new JLabel("Hệ học:");

		lbTrangThai = new JLabel("Trạng thái:");

		JSeparator separator = new JSeparator();

		JLabel lblNewLabel_1 = new JLabel("BẢNG ĐIỂM CÁ NHÂN");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));

		lblContentMaSV = new JLabel(".............");

		lblContentHoTen = new JLabel("..........");

		lblContentNgaySinh = new JLabel("..................");

		lblContentLop = new JLabel("..............");

		lblContentChuongTrinh = new JLabel(".............");

		lblContentHeHoc = new JLabel("..............");

		lblContentTrangThai = new JLabel("...........");

		lblKtQuHc = new JLabel("Kết quả học tập của sinh viên");

		JScrollPane scrollPane_1 = new JScrollPane();

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(233).addComponent(lblBngimSinh))
						.addGroup(groupLayout.createSequentialGroup().addGap(22)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup().addComponent(lbHoTen).addGap(18)
												.addComponent(lblContentHoTen))
										.addGroup(groupLayout.createSequentialGroup().addComponent(lbNgaySinh)
												.addGap(18).addComponent(lblContentNgaySinh))
										.addGroup(groupLayout.createSequentialGroup().addComponent(lbLop).addGap(18)
												.addComponent(lblContentLop))
										.addGroup(groupLayout.createSequentialGroup().addComponent(lbChuongTrinh)
												.addGap(18).addComponent(lblContentChuongTrinh))
										.addGroup(groupLayout.createSequentialGroup().addComponent(lbHeHoc).addGap(18)
												.addComponent(lblContentHeHoc))
										.addGroup(groupLayout.createSequentialGroup().addComponent(lbTrangThai)
												.addGap(18).addComponent(lblContentTrangThai))
										.addComponent(lblNewLabel_1)
										.addComponent(scrollPane, GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
										.addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addGroup(groupLayout.createSequentialGroup().addComponent(lbMaSV).addGap(18)
												.addComponent(lblContentMaSV))
										.addComponent(scrollPane_1, GroupLayout.DEFAULT_SIZE, 695, Short.MAX_VALUE)))
						.addGroup(groupLayout.createSequentialGroup().addGap(242).addComponent(lblKtQuHc)))
				.addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup().addGap(21).addComponent(lblNewLabel_1)
				.addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(separator, GroupLayout.PREFERRED_SIZE, 2, GroupLayout.PREFERRED_SIZE)
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(groupLayout
						.createParallelGroup(Alignment.BASELINE).addComponent(lbMaSV).addComponent(lblContentMaSV))
				.addGap(6)
				.addGroup(groupLayout
						.createParallelGroup(Alignment.BASELINE).addComponent(lbHoTen).addComponent(lblContentHoTen))
				.addGap(6)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lbNgaySinh)
						.addComponent(lblContentNgaySinh))
				.addGap(6)
				.addGroup(groupLayout
						.createParallelGroup(Alignment.BASELINE).addComponent(lbLop).addComponent(lblContentLop))
				.addGap(6)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lbChuongTrinh)
						.addComponent(lblContentChuongTrinh))
				.addGap(6)
				.addGroup(groupLayout
						.createParallelGroup(Alignment.BASELINE).addComponent(lbHeHoc).addComponent(lblContentHeHoc))
				.addGap(6)
				.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lbTrangThai)
						.addComponent(lblContentTrangThai))
				.addGap(7).addComponent(lblBngimSinh).addPreferredGap(ComponentPlacement.RELATED)
				.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 162, GroupLayout.PREFERRED_SIZE).addGap(18)
				.addComponent(lblKtQuHc).addPreferredGap(ComponentPlacement.UNRELATED)
				.addComponent(scrollPane_1, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
				.addContainerGap(66, Short.MAX_VALUE)));

		tblKetQuaHocTap = new JTable();
		tblKetQuaHocTap.setModel(new DefaultTableModel(new Object[][] {
				{ null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null }, },
				new String[] { "H\u1ECDc k\u1EF3", "GPA", "CPA", "TC qua", "TC t\u00EDch l\u0169y",
						"TC n\u1EE3 \u0110K", "TC \u0110K", "Tr\u00ECnh \u0111\u1ED9" }));
		scrollPane_1.setViewportView(tblKetQuaHocTap);

		tblBangDiem = new JTable();
		tblBangDiem.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null },
				{ null, null, null, null, null, null, null, null }, },
				new String[] { "H\u1ECDc K\u1EF3", "M\u00E3 HP", "T\u00EAn HP", "TC", "L\u1EDBp h\u1ECDc",
						"\u0110i\u1EC3m QT", "\u0110i\u1EC3m thi", "\u0110i\u1EC3m ch\u1EEF" }));
		scrollPane.setViewportView(tblBangDiem);
		setLayout(groupLayout);
	}

	/**
	 * Hiển thị thông tin sinh viên, set giá trị cho các label hiển thị thông tin
	 * 
	 * @throws SQLException
	 */
	public void LoadData() throws SQLException {
		String taikhoan = MainUI.getInstance().getTaiKhoan();
		SinhVien sinhvien = SinhVienController.getInstance().getUser(taikhoan);

		lblContentMaSV.setText(sinhvien.getTaikhoan());
		lblContentHoTen.setText(sinhvien.getHoten());
		lblContentNgaySinh.setText(sinhvien.getNgaysinh());
		lblContentLop.setText(sinhvien.getLop());
		lblContentChuongTrinh.setText(sinhvien.getChuongtrinhhoc());
		lblContentHeHoc.setText(sinhvien.getHehoc());
		lblContentTrangThai.setText(sinhvien.getTrangthai());
	}
}
