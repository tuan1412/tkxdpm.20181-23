package boundary;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.table.DefaultTableModel;

import controller.ThoiKhoaBieuController;

import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.Color;

/**
 * Class thể hiện view Thời khóa biểu
 * 
 * @author PhamTuan
 *
 */
public class ThoiKhoaBieu extends JPanel implements ActionListener {

	/** TextField nhập mã tài khoản sinh viên */
	private JTextField tfmaSV;

	/** Label hiển thị thông tin và lỗi */
	private JLabel lbTKB, lbmaSV, lbKetQua, lbErrorMaSV;

	/** Button tra cứu thời khóa biểu */
	private JButton btnTraCuu;

	/** Table hiển thị kết quả thời khóa biểu */
	private JTable tbTKB;

	/** ScrollPane chứa table */
	private JScrollPane scrollPane;

	/**
	 * Create the panel thoikhoabieu.
	 */
	public ThoiKhoaBieu() {

		lbTKB = new JLabel("Thời khóa biểu");
		lbTKB.setFont(new Font("Tahoma", Font.BOLD, 14));
		lbmaSV = new JLabel("Nhập mã số SV:");

		tfmaSV = new JTextField();
		tfmaSV.setColumns(10);

		lbErrorMaSV = new JLabel("");
		lbErrorMaSV.setForeground(Color.RED);
		lbErrorMaSV.setFont(new Font("Tahoma", Font.PLAIN, 11));

		btnTraCuu = new JButton("Tra cứu");
		btnTraCuu.addActionListener(this);
		scrollPane = new JScrollPane();

		lbKetQua = new JLabel("");

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING).addGroup(groupLayout
				.createSequentialGroup()
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(43)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING).addComponent(lbTKB)
										.addGroup(groupLayout.createSequentialGroup().addComponent(lbmaSV)
												.addPreferredGap(ComponentPlacement.RELATED)
												.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
														.addGroup(groupLayout.createSequentialGroup()
																.addComponent(tfmaSV, GroupLayout.PREFERRED_SIZE, 126,
																		GroupLayout.PREFERRED_SIZE)
																.addGap(44).addComponent(btnTraCuu))
														.addComponent(lbErrorMaSV, GroupLayout.PREFERRED_SIZE, 183,
																GroupLayout.PREFERRED_SIZE)))))
						.addGroup(groupLayout.createSequentialGroup().addContainerGap().addComponent(scrollPane,
								GroupLayout.PREFERRED_SIZE, 782, GroupLayout.PREFERRED_SIZE)))
				.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING,
						groupLayout.createSequentialGroup().addContainerGap(245, Short.MAX_VALUE)
								.addComponent(lbKetQua, GroupLayout.PREFERRED_SIZE, 364, GroupLayout.PREFERRED_SIZE)
								.addGap(183)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(47).addComponent(lbTKB).addGap(40)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE).addComponent(lbmaSV)
								.addComponent(tfmaSV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(btnTraCuu))
						.addGap(18).addComponent(lbErrorMaSV).addGap(37).addComponent(lbKetQua)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(scrollPane, GroupLayout.PREFERRED_SIZE, 166, GroupLayout.PREFERRED_SIZE)
						.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
		setLayout(groupLayout);

	}

	/**
	 * Xử lý sự kiện khi click button 'tra cứu' thời khóa biểu Hiển thị bảng kết quả
	 * thời khóa biểu
	 */
	public void actionPerformed(ActionEvent ae) {
		if (ae.getSource() == btnTraCuu) {

			String col[] = { "Thời gian", "Tuần học", "Phòng học", "Mã lớp", "Mã học phần", "Tên lớp" };

			DefaultTableModel tableModel = new DefaultTableModel(col, 0);
			tbTKB = new JTable();
			tbTKB.setModel(tableModel);
			scrollPane.setViewportView(tbTKB);

			String taikhoan = tfmaSV.getText().trim();
			int error = 0;
			lbKetQua.setText("");
			// Kiểm tra lỗi input
			if (taikhoan.equals("")) {
				lbErrorMaSV.setText("--Nhập vào mã số sinh viên---");
				error = 1;
			}

			if (!taikhoan.equals("")) {
				try {
					int mssv = Integer.parseInt(taikhoan);
				} catch (NumberFormatException ex) {
					lbErrorMaSV.setText("---Mã số sinh viên không hợp lệ---");
					error = 1;
				}
			}

			// Nếu không có lỗi input, thực hiện hiển thị thời khóa biểu
			if (error == 0) {

				lbErrorMaSV.setText("");
				lbKetQua.setText("Kết quả tra cứu thời khóa biểu MSSV: " + taikhoan);
				List<entity.ThoiKhoaBieu> list = ThoiKhoaBieuController.getInstance().getListTKB(taikhoan);

				for (int i = 0; i < list.size(); i++) {

					String thoiGian = list.get(i).getThoiGian();
					String tuanHoc = list.get(i).getTuanHoc();
					String phongHoc = list.get(i).getPhongHoc();
					String maHP = list.get(i).getMaHP();
					String tenLop = list.get(i).getTenLop();
					String malop = list.get(i).getMaLop();

					Object[] data = { thoiGian, tuanHoc, phongHoc, malop, maHP, tenLop };

					tableModel.addRow(data);
				}

			}

		}
	}
}
