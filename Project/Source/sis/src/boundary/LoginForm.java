package boundary;

import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import common.MainUI;
import common.UserHelper;
import controller.UserController;
import entity.User;
import javax.swing.ImageIcon;
import java.awt.Color;

/**
 * Class này là lớp biên của usecase Đăng nhập
 * 
 * @author TuanNguyen
 *
 */
@SuppressWarnings("serial")
public class LoginForm extends JPanel {

	/** TextField để người dùng nhập tài khoản */
	private JTextField tfTaiKhoan;

	/** PasswordField để người dùng nhập password */
	private JPasswordField pfMatKhau;

	public LoginForm() {
		initComponents();
	}

	/**
	 * Hàm này để khởi tạo các components
	 */
	private void initComponents() {

		JLabel lblDangNhap = new JLabel("Đăng nhập");
		lblDangNhap.setFont(new Font("Tahoma", Font.BOLD, 18));

		JLabel lblTaiKhoan = new JLabel("Tài khoản");
		JLabel lblMatKhau = new JLabel("Mật khẩu");

		tfTaiKhoan = new JTextField();
		tfTaiKhoan.setColumns(10);

		JButton btnDangNhap = new JButton("Đăng nhập");

		pfMatKhau = new JPasswordField();
		
		JLabel lblStudentInformationSystem = new JLabel("Student Information System");
		lblStudentInformationSystem.setForeground(Color.RED);
		lblStudentInformationSystem.setFont(new Font("Times New Roman", Font.PLAIN, 34));
		
		JLabel lblNewLabel = new JLabel("Hanoi University of Science and Technology");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setFont(new Font("Times New Roman", Font.PLAIN, 34));
		GroupLayout gl_contentPane = new GroupLayout(this);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(300)
							.addComponent(lblDangNhap, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(209)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(lblTaiKhoan, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblMatKhau, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE))
							.addGap(18)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(tfTaiKhoan, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
								.addComponent(btnDangNhap, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
								.addComponent(pfMatKhau, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(159)
							.addComponent(lblStudentInformationSystem))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(51)
							.addComponent(lblNewLabel)))
					.addContainerGap(91, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(33)
					.addComponent(lblStudentInformationSystem)
					.addGap(18)
					.addComponent(lblNewLabel)
					.addGap(71)
					.addComponent(lblDangNhap, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
					.addGap(34)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblTaiKhoan, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
						.addComponent(tfTaiKhoan, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(12)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(pfMatKhau, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblMatKhau, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
					.addGap(18)
					.addComponent(btnDangNhap)
					.addGap(246))
		);
		setLayout(gl_contentPane);

		// Bắt sự kiện ấn nút Đăng nhập
		btnDangNhap.addActionListener(e -> {
			doLogin();
		});

		// Bắt sự kiện ấn phím enter khi nhập mật khẩu
		pfMatKhau.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					doLogin();
				}
			}
		});
	}

	/**
	 * Hàm này thực hiện chức năng đăng nhập
	 */
	private void doLogin() {
		String taikhoan = tfTaiKhoan.getText().trim();
		String pword = new String(new String(pfMatKhau.getPassword()).trim());
		String password = UserHelper.MD5encrypt(pword);

		boolean flag = true;
		if (taikhoan.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Tài khoản không được để trống.");
			flag = false;
		}
		if (flag && password.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Password không được để trống.");
			flag = false;
		}
		if (flag) {
			User user = null;
			user = UserController.getInstance().doLogin(taikhoan, password);
			if (user != null) {
				MainUI.getInstance().setView(user);
			} else {
				JOptionPane.showMessageDialog(this, "Tài khoản hoặc mật khẩu không đúng");
			}
		}
	}
}
