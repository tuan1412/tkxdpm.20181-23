package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import common.ConnectionHelper;

/**
 * Class này quản lý các thông tin của lớp học trong cơ sở dữ liệu
 * 
 * @author TuanNguyen
 *
 */
public class LopHoc {

	/** id của lớp học */
	private int id;

	/** mã lớp của lớp học */
	private String malop;

	/** mã học phần của lớp học */
	private String mahp;

	/** tên học phần của lớp học */
	private String tenhp;

	/** kì học của lớp */
	private String kihoc;

	/** trọng số môn học */
	private float trongSo;

	/** chi tiết danh sách sinh viên trong lớp */
	private List<ChiTietLopHoc> chiTietList;

	/** câu lệnh sql để tìm kiếm thông tin lớp học */
	private final String SEARCH_LOP = "select idlophoc, malop, kihoc, mahp, tenhp, trongSo, hoten, mssv, diemqt, diemthi, diemchu "
			+ "from lophoc_view " + "where malop = ? ";

	/** câu lệnh sql để update điểm lớp học */
	private final String UPDATE_BANGDIEM = "update lophoc_view set diemqt = ?, diemthi = ?, diemchu = ? "
			+ "where mssv = ? and malop = ?";

	/**
	 * Hàm này để lấy thông tin lớp học
	 * 
	 * @param malop
	 *            là mã lớp cần tra cứu
	 * @return thông tin của lớp học
	 * @throws SQLException
	 */
	public LopHoc searchLopHoc(String malop) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SEARCH_LOP);
		ps.setString(1, malop);

		ResultSet rs = ps.executeQuery();
		boolean flag = false;
		LopHoc lopHoc = new LopHoc();
		List<ChiTietLopHoc> chiTietList = new ArrayList<>();

		while (rs.next()) {
			if (!flag) {
				lopHoc.setId(rs.getInt("idlophoc"));
				lopHoc.setKihoc(rs.getString("kihoc"));
				lopHoc.setMahp(rs.getString("mahp"));
				lopHoc.setMalop(rs.getString("malop"));
				lopHoc.setTenhp(rs.getString("tenhp"));
				lopHoc.setTrongSo(rs.getFloat("trongSo"));
				flag = true;
			}
			ChiTietLopHoc chiTiet = new ChiTietLopHoc();
			chiTiet.setTenSinhVien(rs.getString("hoten"));
			chiTiet.setMaSinhVien(rs.getString("mssv"));
			chiTiet.setDiemQt(rs.getFloat("diemqt"));
			chiTiet.setDiemThi(rs.getFloat("diemthi"));
			chiTiet.setDiemChu(rs.getString("diemchu"));
			chiTietList.add(chiTiet);
		}
		if (flag) {
			lopHoc.setChiTietList(chiTietList);
			ConnectionHelper.closeDB();
			return lopHoc;
		}
		ConnectionHelper.closeDB();
		return null;

	}

	/**
	 * Hàm này để cập nhật điểm của lớp học
	 * 
	 * @param chiTietList
	 *            là danh sách điểm cần cập nhật
	 * @param malop
	 *            là mã của lớp học
	 * @return true nếu cập nhật thành công, false nếu thất bại
	 * @throws SQLException
	 */
	public boolean updateDiemLopHoc(List<ChiTietLopHoc> chiTietList, String malop) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		boolean flag = true;
		try {
			PreparedStatement ps = conn.prepareStatement(UPDATE_BANGDIEM);
			conn.setAutoCommit(false);
			for (ChiTietLopHoc chitiet : chiTietList) {
				ps.setFloat(1, chitiet.getDiemQt());
				ps.setFloat(2, chitiet.getDiemThi());
				ps.setString(3, chitiet.getDiemChu());
				ps.setString(4, chitiet.getMaSinhVien());
				ps.setString(5, malop);
				ps.addBatch();
			}
			;
			int[] rs = ps.executeBatch();
			if (rs.length > 0) {
				conn.commit();
			}
		} catch (SQLException e) {
			conn.rollback();
			e.printStackTrace();
			ConnectionHelper.closeDB();
			flag = false;
		}
		return flag;

	}

	/**
	 * Hàm này để lấy id của lớp học
	 * 
	 * @return id là id lớp học
	 */
	public int getId() {
		return id;
	}

	/**
	 * Hàm này để gán id của lớp học
	 * 
	 * @param id
	 *            là id cần gán
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Hàm này để lấy mã lớp học
	 * 
	 * @return mã lớp học
	 */
	public String getMalop() {
		return malop;
	}

	/**
	 * Hàm này để gán mã lớp
	 * 
	 * @param malop
	 *            là mã lớp cần gán
	 */
	public void setMalop(String malop) {
		this.malop = malop;
	}

	/**
	 * Hàm này để lấy mã học phần
	 * 
	 * @return mã học phần lớp học
	 */
	public String getMahp() {
		return mahp;
	}

	/**
	 * Hàm này để gán mã học phần
	 * 
	 * @param mahp
	 *            là mã học phần lớp học
	 */
	public void setMahp(String mahp) {
		this.mahp = mahp;
	}

	/**
	 * Hàm này để gán tên học phần của lớp học
	 * 
	 * @return tên học phần
	 */
	public String getTenhp() {
		return tenhp;
	}

	/**
	 * Hàm này để gán tên học phần
	 * 
	 * @param tenhp
	 *            là tên học phần
	 */
	public void setTenhp(String tenhp) {
		this.tenhp = tenhp;
	}

	/**
	 * Hàm này để lấy kì học
	 * 
	 * @return kì học
	 */
	public String getKihoc() {
		return kihoc;
	}

	/**
	 * Hàm này để gán kì học
	 * 
	 * @param kihoc
	 */
	public void setKihoc(String kihoc) {
		this.kihoc = kihoc;
	}

	/**
	 * Hàm này để lấy chi tiết danh sách lớp học
	 * 
	 * @return danh sách chi tết lớp học
	 */
	public List<ChiTietLopHoc> getChiTietList() {
		return chiTietList;
	}

	/**
	 * Hàm này để gán danh sách chi tiết lớp học
	 * 
	 * @param chiTietList
	 *            là danh sách chi tiết lớp học
	 */
	public void setChiTietList(List<ChiTietLopHoc> chiTietList) {
		this.chiTietList = chiTietList;
	}

	/**
	 * Hàm này để lấy trọng số môn học
	 * 
	 * @return trọng số môn học
	 */
	public float getTrongSo() {
		return trongSo;
	}

	/**
	 * Hàm này để gán trọng số môn học
	 * 
	 * @param trongSo
	 *            là trọng số môn học
	 */
	public void setTrongSo(float trongSo) {
		this.trongSo = trongSo;
	}

}
