package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import common.ConnectionHelper;

/**
 * Class thực hiện các truy vấn liên quan đến thời khóa biểu
 * 
 * @author PhamTuan
 *
 */
public class ThoiKhoaBieu {
	/**
	 * Tham số đầu vào thời gian, tuần học, phòng học, mã học phần, tên lớp học, ghi
	 * chú
	 */
	private String thoiGian, tuanHoc, phongHoc, maHP, tenLop, ghiChu, maLop;

	/**
	 * Truy vấn thời khóa biểu,lấy thông tin thời gian, tuần học, phòng học, mã học
	 * phần, tên lớp học,ghi chú
	 */

	private final String SEARCH_TKB = "select thoigian,tuanhoc,phonghoc,malop,mahp,tenhp,kihoc"
			+ " from lophoc_view where lophoc_view.mssv = ?"
			+ "	and lophoc_view.kihoc in (select max(lophoc_view.kihoc) from lophoc_view)";

	/**
	 * Lấy thông tin thời khóa biểu, gồm thời gian, tuần học, phòng học, mã học phần
	 * và tên lớp học
	 * 
	 * @param taikhoan:
	 *            mã tài khoản sinh viên
	 * @return danh sách thời khóa biểu theo mã tài khoản sinh viên
	 */
	public List<ThoiKhoaBieu> searchTKB(String taikhoan) {

		List<ThoiKhoaBieu> list = new ArrayList<ThoiKhoaBieu>();

		try {
			Connection conn = ConnectionHelper.connectDb();
			PreparedStatement ptmt = conn.prepareStatement(SEARCH_TKB);
			ptmt.setString(1, taikhoan);
			ResultSet rs = ptmt.executeQuery();

			while (rs.next()) {
				ThoiKhoaBieu tkb = new ThoiKhoaBieu();
				String thoiGian = rs.getString("thoigian");
				String tuanHoc = rs.getString("tuanhoc");
				String phongHoc = rs.getString("phonghoc");
				String maHP = rs.getString("mahp");
				String tenlop = rs.getString("tenhp");
				String maLop = rs.getString("malop");

				tkb.setThoiGian(thoiGian);
				tkb.setTuanHoc(tuanHoc);
				tkb.setPhongHoc(phongHoc);
				tkb.setMaHP(maHP);
				tkb.setTenLop(tenlop);
				tkb.setMaLop(maLop);

				list.add(tkb);
			}
		} catch (SQLException e) {
			System.out.println("Error search TKB: " + e);
		}

		return list;
	}

	/**
	 * Lấy thông tin thời gian của môn học
	 * 
	 * @return thời gian môn học
	 */
	public String getThoiGian() {
		return thoiGian;
	}

	/**
	 * Gán thời gian cho môn học
	 * 
	 * @param thoiGian:
	 *            thời gian
	 */
	public void setThoiGian(String thoiGian) {
		this.thoiGian = thoiGian;
	}

	/**
	 * Lấy thông tin tuần học
	 * 
	 * @return tuần học
	 */
	public String getTuanHoc() {
		return tuanHoc;
	}

	/**
	 * Gán thông tin tuần học
	 * 
	 * @param tuanHoc:
	 *            tuần học
	 */
	public void setTuanHoc(String tuanHoc) {
		this.tuanHoc = tuanHoc;
	}

	/**
	 * Lấy thông tin phòng học
	 * 
	 * @return phòng học
	 */
	public String getPhongHoc() {
		return phongHoc;
	}

	/**
	 * Gán thông tin phòng học
	 * 
	 * @param phongHoc:
	 *            phòng học
	 */
	public void setPhongHoc(String phongHoc) {
		this.phongHoc = phongHoc;
	}

	/**
	 * Lấy thông tin mã học phần
	 * 
	 * @return mã học phần
	 */
	public String getMaHP() {
		return maHP;
	}

	/**
	 * Gán mã học phần môn học
	 * 
	 * @param maHP:
	 *            mã học phần
	 */
	public void setMaHP(String maHP) {
		this.maHP = maHP;
	}

	/**
	 * Lấy thông tin lớp tương ứng với mã học phần
	 * 
	 * @return tên lớp học
	 */
	public String getTenLop() {
		return tenLop;
	}

	/**
	 * Gán thông tin lớp tương ứng với mã học phần
	 * 
	 * @param tenLop:
	 *            tên lớp
	 */
	public void setTenLop(String tenLop) {
		this.tenLop = tenLop;
	}

	/**
	 * Lấy thông tin ghi chú của thời khóa biểu
	 * 
	 * @return ghi chú
	 */
	public String getGhiChu() {
		return ghiChu;
	}

	/**
	 * Gán thông tin ghi chú thời khóa biểu
	 * 
	 * @param ghiChu
	 */
	public void setGhiChu(String ghiChu) {
		this.ghiChu = ghiChu;
	}

	/**
	 * Lấy thông tin mã lớp cho thời khóa biểu
	 * 
	 * @return
	 */
	public String getMaLop() {
		return maLop;
	}

	/**
	 * Gán thông tin mã lớp cho thời khóa biểu
	 * 
	 * @param maLop
	 */
	public void setMaLop(String maLop) {
		this.maLop = maLop;
	}

}
