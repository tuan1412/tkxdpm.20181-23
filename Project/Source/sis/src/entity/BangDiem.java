package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import common.ConnectionHelper;

/**
 * Class này quản lý các thông tin của bảng điểm trong cơ sở dữ liệu
 * 
 * @author TungNguyen
 *
 */
public class BangDiem {

	/** tài khoản */
	private String taikhoan;

	/** mã học phần */
	private String mahp;

	/** tên học phần */
	private String tenhp;

	/** số tín chỉ */
	private int sotinchi;

	/** mã lớp */
	private String malop;

	/** điểm quá trình */
	private float diemqt;

	/** điểm thi */
	private float diemthi;

	/** điểm chữ */
	private String diemchu;

	/** kì học */
	private int kihoc;

	/** chi tiết bảng điểm */
	private List<BangDiem> chiTietList;

	/** câu lệnh sql để tìm kiếm thông tin bảng điểm */
	private final String SEARCH_BANGDIEM = "select lophoc.kihoc, lophoc.mahp, hocphan.tenhp, hocphan.soTinChi, lophoc.malop,"
			+ " sinhvien_lophoc.diemqt, sinhvien_lophoc.diemthi "
			+ "from sinhvien inner join sinhvien_lophoc on sinhvien.taikhoan = sinhvien_lophoc.taikhoan "
			+ "inner join lophoc on sinhvien_lophoc.idlophoc = lophoc.idlophoc inner join hocphan on lophoc.mahp = hocphan.mahp "
			+ "where sinhvien_lophoc.taikhoan = ?";

	/**
	 * Hàm này để lấy thông tin bảng điểm
	 * 
	 * @param taikhoan
	 *            là tài khoản cần tra cứu
	 * @return thông tin của bảng điểm
	 * @throws SQLException
	 */
	public BangDiem searchBangDiem(String taikhoan) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SEARCH_BANGDIEM);
		ps.setString(1, taikhoan);
		BangDiem bangDiem = new BangDiem();
		List<BangDiem> chitietList = new ArrayList<>();
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			BangDiem chiTiet = new BangDiem();
			chiTiet.setKihoc(rs.getInt("kihoc"));
			chiTiet.setMahp(rs.getString("mahp"));
			chiTiet.setTenhp(rs.getString("tenhp"));
			chiTiet.setSotinchi(rs.getInt("soTinChi"));
			chiTiet.setMalop(rs.getString("malop"));
			chiTiet.setDiemqt(rs.getFloat("diemqt"));
			chiTiet.setDiemthi(rs.getFloat("diemthi"));
			chitietList.add(chiTiet);

		}
		bangDiem.setTaikhoan(taikhoan);
		bangDiem.setChiTietList(chitietList);
		ConnectionHelper.closeDB();

		return bangDiem;
	}

	/**
	 * Hàm này để lấy tài khoản
	 * 
	 * @return tài khoản
	 */
	public String getTaikhoan() {
		return taikhoan;
	}

	/**
	 * Hàm này để gán tài khoản của user
	 * 
	 * @param taikhoan
	 *            là tài khoản của user
	 */
	public void setTaikhoan(String taikhoan) {
		this.taikhoan = taikhoan;
	}

	/**
	 * Hàm này để lấy mã học phần
	 * 
	 * @return mã học phần
	 */
	public String getMahp() {
		return mahp;
	}

	/**
	 * Hàm này để gán mã học phần của học phần
	 * 
	 * @param mahp
	 *            là mã học phần của học phần
	 */
	public void setMahp(String mahp) {
		this.mahp = mahp;
	}

	/**
	 * Hàm này để lấy tên học phần
	 * 
	 * @return tên học phần
	 */
	public String getTenhp() {
		return tenhp;
	}

	/**
	 * Hàm này để gán tên học phần của học phần
	 * 
	 * @param tenhp
	 *            là tên học phần của học phần
	 */
	public void setTenhp(String tenhp) {
		this.tenhp = tenhp;
	}

	/**
	 * Hàm này để lấy số tín chỉ
	 * 
	 * @return số tín chỉ
	 */
	public int getSotinchi() {
		return sotinchi;
	}

	/**
	 * Hàm này để gán số tín chỉ của học phần
	 * 
	 * @param sotinchi
	 *            là số tín chỉ của học phần
	 */
	public void setSotinchi(int sotinchi) {
		this.sotinchi = sotinchi;
	}

	/**
	 * Hàm này để lấy mã lớp
	 * 
	 * @return mã lớp
	 */
	public String getMalop() {
		return malop;
	}

	/**
	 * Hàm này để gán mã lớp học
	 * 
	 * @param malop
	 *            là mã lớp học
	 */
	public void setMalop(String malop) {
		this.malop = malop;
	}

	/**
	 * Hàm này để lấy điểm quá trình
	 * 
	 * @return điểm quá trình
	 */
	public float getDiemqt() {
		return diemqt;
	}

	/**
	 * Hàm này để gán điểm quá trình
	 * 
	 * @param diemqt
	 *            là điểm quá trình
	 */
	public void setDiemqt(float diemqt) {
		this.diemqt = diemqt;
	}

	/**
	 * Hàm này để lấy điểm thi
	 * 
	 * @return điểm thi
	 */
	public float getDiemthi() {
		return diemthi;
	}

	/**
	 * Hàm này để gán điểm thi
	 * 
	 * @param diemthi
	 *            là điểm thi
	 */
	public void setDiemthi(float diemthi) {
		this.diemthi = diemthi;
	}

	/**
	 * Hàm này để lấy điểm chữ
	 * 
	 * @return điểm chữ
	 */
	public String getDiemchu() {
		return diemchu;
	}

	/**
	 * Hàm này để gán điểm chữ
	 * 
	 * @param diemchu
	 *            là điểm chữ
	 */
	public void setDiemchu(String diemchu) {
		this.diemchu = diemchu;
	}

	/**
	 * Hàm này để lấy kì học
	 * 
	 * @return mã kì học
	 */
	public int getKihoc() {
		return kihoc;
	}

	/**
	 * Hàm này để gán kì học
	 * 
	 * @param kihoc
	 *            là kihoc
	 */
	public void setKihoc(int kihoc) {
		this.kihoc = kihoc;
	}

	/**
	 * Hàm này để gán danh sách chi tiết bảng điểm
	 * 
	 * @param chiTietList
	 *            là danh sách chi tiết bảng điểm
	 */
	public void setChiTietList(List<BangDiem> chiTietList) {
		this.chiTietList = chiTietList;
	}

	/**
	 * Hàm này để lấy chi tiết danh sách bảng điểm
	 * 
	 * @return chi tiết danh sách bảng điểm
	 */
	public List<BangDiem> getChiTietList() {
		return chiTietList;
	}

}
