package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import common.ConnectionHelper;

/**
 * Class này quản lý các thông tin của đăng ký học phần trong cơ sở dữ liệu
 * 
 * @author TungNguyen
 *
 */
public class DangKiHocPhan {

	/** mã học phần của học phần */
	private String mahp;

	/** tên học phần của học phần */
	private String tenhp;

	/** ngày đăng ký học phần */
	private String ngaydk;

	/** trạng thái đăng ký của học phần */
	private String trangthaidk;

	/** kì học đăng kí học phần */
	private String kihoc;

	/** số tín chỉ của học phần */
	private int sotinchi;

	/** tài khoản của user đăng nhập */
	private String taikhoan;

	/** Câu lệnh thêm các thông tin đăng kí học phần vào bảng dangkihocphan */
	private final String INSERT_DANGKIHP = "INSERT INTO bangdangkihocphan (mahp, tenhp, ngaydk, trangthaidk, kihoc, soTinChi, taikhoan) VALUES (?, ?, ?, ?, ?, ?, ?)";

	/** Câu lệnh truy vấn lấy bảng đăng kí học phần với kì học và tài khoản */
	private final String SELECT_DANGKIHP = "SELECT * FROM bangdangkihocphan WHERE bangdangkihocphan.kihoc = ? AND bangdangkihocphan.taikhoan = ?";

	/**
	 * Câu lệnh truy vấn xóa học phần đăng kí đã chọn với mã học phần, kì học và tài
	 * khoản
	 */
	private final String DELETE_DANGKIHP = "DELETE FROM bangdangkihocphan WHERE mahp = ? AND kihoc = ? AND taikhoan = ?";

	/**
	 * Hàm này để thêm mới học phần đăng ký vào db
	 * 
	 * @throws SQLException
	 */
	public void insertDangKiHP() throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(INSERT_DANGKIHP);
		ps.setString(1, getMahp());
		ps.setString(2, getTenhp());
		ps.setString(3, getNgaydk());
		ps.setString(4, getTrangthaidk());
		ps.setString(5, getKihoc());
		ps.setInt(6, getSotinchi());
		ps.setString(7, getTaikhoan());
		ps.execute();
		ConnectionHelper.connectDb();
	}

	/**
	 * Hàm này lấy thông tin đăng ký học phần theo kỳ học và tài khoản
	 * 
	 * @param kihoc
	 *            là kì học đăng ký học phần
	 * @param taikhoan
	 *            là tài khoản của user
	 * @return dkHocPhans thông tin đăng ký học phần
	 * @throws SQLException
	 */
	public List<DangKiHocPhan> selectDangKiHP(String kihoc, String taikhoan) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SELECT_DANGKIHP);
		ps.setString(1, kihoc);
		ps.setString(2, taikhoan);
		ResultSet rs = ps.executeQuery();
		List<DangKiHocPhan> dkHocPhans = new ArrayList<>();
		while (rs.next()) {
			DangKiHocPhan dkhp = new DangKiHocPhan();
			dkhp.setNgaydk(rs.getString("ngaydk"));
			dkhp.setMahp(rs.getString("mahp"));
			dkhp.setTenhp(rs.getString("tenhp"));
			dkhp.setSotinchi(rs.getInt("soTinChi"));
			dkhp.setTrangthaidk(rs.getString("trangthaidk"));
			dkHocPhans.add(dkhp);
		}
		for (int i = 0; i < dkHocPhans.size(); i++) {
			System.out.println(dkHocPhans.get(i).getMahp());
		}
		ConnectionHelper.connectDb();
		return dkHocPhans;
	}

	/**
	 * Hàm này để xóa học phần đã đăng ký trong db
	 * 
	 * @throws SQLException
	 */
	public void deleteDKHP() throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(DELETE_DANGKIHP);
		ps.setString(1, getMahp());
		ps.setString(2, getKihoc());
		ps.setString(3, getTaikhoan());
		ps.execute();
		ConnectionHelper.connectDb();
	}

	/**
	 * Hàm này để lấy mã học phần của học phần
	 * 
	 * @return mahp là mã học phần của học phần
	 */
	public String getMahp() {
		return mahp;
	}

	/**
	 * Hàm này để gán mã học phần của học phần
	 * 
	 * @param mahp
	 *            là mã học phần cần gán
	 */
	public void setMahp(String mahp) {
		this.mahp = mahp;
	}

	/**
	 * Hàm này để lấy tên học phần của học phần
	 * 
	 * @return tenhp là mã học phần của học phần
	 */
	public String getTenhp() {
		return tenhp;
	}

	/**
	 * Hàm này để gán tên học phần của học phần
	 * 
	 * @param tenhp
	 *            là tên học phần cần gán
	 */
	public void setTenhp(String tenhp) {
		this.tenhp = tenhp;
	}

	/**
	 * Hàm này để lấy ngày đăng ký của học phần
	 * 
	 * @return ngaydk là ngày đăng ký của học phần
	 */
	public String getNgaydk() {
		return ngaydk;
	}

	/**
	 * Hàm này để gán ngày đăng ký của học phần
	 * 
	 * @param ngaydk
	 *            là ngày đăng ký cần gán
	 */
	public void setNgaydk(String ngaydk) {
		this.ngaydk = ngaydk;
	}

	/**
	 * Hàm này để lấy trạng thái đăng ký của học phần
	 * 
	 * @return trangthaidk là trạng thái đăng ký của học phần
	 */
	public String getTrangthaidk() {
		return trangthaidk;
	}

	/**
	 * Hàm này để gán trạng thái đăng ký của học phần
	 * 
	 * @param trangthaidk
	 *            là trạng thái đăng ký cần gán
	 */
	public void setTrangthaidk(String trangthaidk) {
		this.trangthaidk = trangthaidk;
	}

	/**
	 * Hàm này để lấy kỳ học đăng ký của học phần
	 * 
	 * @return kihoc là kỳ học đăng ký của học phần
	 */
	public String getKihoc() {
		return kihoc;
	}

	/**
	 * Hàm này để gán kì học đăng kí của học phần
	 * 
	 * @param kihoc
	 *            là kì học cần gán
	 */
	public void setKihoc(String kihoc) {
		this.kihoc = kihoc;
	}

	/**
	 * Hàm này để lấy số tín chỉ của học phần
	 * 
	 * @return sotinchi là số tín chỉ của học phần
	 */
	public int getSotinchi() {
		return sotinchi;
	}

	/**
	 * Hàm này để gán số tín chỉ của học phần
	 * 
	 * @param sotinchi
	 *            là số tín chỉ cần gán
	 */
	public void setSotinchi(int sotinchi) {
		this.sotinchi = sotinchi;
	}

	/**
	 * Hàm này để lấy tài khoản của user
	 * 
	 * @return taikhoan là tài khoản của user
	 */
	public String getTaikhoan() {
		return taikhoan;
	}

	/**
	 * Hàm này để gán tài khoản của user
	 * 
	 * @param taikhoan
	 *            là tài khoản cần gán
	 */
	public void setTaikhoan(String taikhoan) {
		this.taikhoan = taikhoan;
	}

}
