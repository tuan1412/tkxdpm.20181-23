package entity;

/**
 * Class này để quản lý thông tin chi tiết các môn học của thông tin học phí
 * 
 * @author TuanNguyen
 *
 */
public class ChiTietHocPhi {

	/** mã học phần */
	private String mahp;

	/** tên học phần */
	private String hocphan;

	/** thời lượng học phần */
	private String thoiluong;

	/** số tín chỉ */
	private int soTinChi;

	/** tín chỉ học phí */
	private float tinChiHocPhi;

	/** mã lớp */
	private String malop;

	/**
	 * Hàm này để lấy mã học phần
	 * 
	 * @return mã học phần
	 */
	public String getMahp() {
		return mahp;
	}

	/**
	 * Hàm này để gán mã học phần
	 * 
	 * @param mahp
	 *            là mã học phần
	 */
	public void setMahp(String mahp) {
		this.mahp = mahp;
	}

	/**
	 * Hàm này để lấy tên học phần
	 * 
	 * @return tên học phần
	 */
	public String getHocphan() {
		return hocphan;
	}

	/**
	 * Hàm này để gán tên học phần
	 * 
	 * @param hocphan
	 */
	public void setHocphan(String hocphan) {
		this.hocphan = hocphan;
	}

	/**
	 * Hàm này để lấy thời lượng
	 * 
	 * @return thời lượng học phần
	 */
	public String getThoiluong() {
		return thoiluong;
	}

	/**
	 * Hàm này để gán thời lượng
	 * 
	 * @param thoiluong
	 *            là thời lượng
	 */
	public void setThoiluong(String thoiluong) {
		this.thoiluong = thoiluong;
	}

	/**
	 * Hàm này để lấy số tín chỉ
	 * 
	 * @return số tín chỉ
	 */
	public int getSoTinChi() {
		return soTinChi;
	}

	/**
	 * Hàm này để gán số tín chỉ
	 * 
	 * @param soTinChi
	 *            là số tín chỉ
	 */
	public void setSoTinChi(int soTinChi) {
		this.soTinChi = soTinChi;
	}

	/**
	 * Hàm này để lấy số tín chỉ học phí
	 * 
	 * @return tín chỉ học phí
	 */
	public float getTinChiHocPhi() {
		return tinChiHocPhi;
	}

	/**
	 * Hàm này để gán tín chỉ học phí
	 * 
	 * @param tinChiHocPhi
	 *            là tín chỉ học phí
	 */
	public void setTinChiHocPhi(float tinChiHocPhi) {
		this.tinChiHocPhi = tinChiHocPhi;
	}

	/**
	 * Hàm này để lấy mã lớp
	 * 
	 * @return mã lớp
	 */
	public String getMalop() {
		return malop;
	}

	/**
	 * Hàm này để gán mã lớp
	 * 
	 * @param malop
	 *            là mã lớp
	 */
	public void setMalop(String malop) {
		this.malop = malop;
	}

}
