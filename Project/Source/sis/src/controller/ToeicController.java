package controller;

import java.util.List;

import entity.Toeic;

/**
 * Thực hiện các điều khiển liên quan đến đối tượng Toeic
 * 
 * @author PhamTuan
 *
 */
public class ToeicController {

	/** toeic để quản lý thông tin về bảng điểm toeic */
	Toeic toeic = new Toeic();

	/** đối tượng ToeicController dùng chung */
	private static ToeicController instance;

	/**
	 * Hàm khởi tạo để private để các đối tượng bên ngoài không thể tùy ý khởi tạo
	 */
	private ToeicController() {

	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của ToeicController nếu
	 * đối tượng đố null
	 * 
	 * @return đối tượng ToeicController dùng chung
	 */
	public static ToeicController getInstance() {
		if (instance == null)
			instance = new ToeicController();
		return instance;
	}

	/**
	 * Lấy danh sách điểm thi Toeic theo mã tài khoản, họ tên và học kì thi Toeic
	 * 
	 * @param hocky:
	 *            học kỳ thi toeic
	 * @param masv:
	 *            mã tài khoản sinh viên
	 * @param hoten:
	 *            họ tên sinh viên
	 * @return danh sách điểm thi Toeic
	 */
	public List<Toeic> getListToeic(String hocky, String masv, String hoten) {
		return toeic.searchToiec(hocky, masv, hoten);
	}

}
