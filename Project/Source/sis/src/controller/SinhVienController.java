package controller;

import java.sql.SQLException;
import java.util.*;

import entity.SinhVien;

/**
 * Thực hiện các điều khiển liên quan đến đối tượng sinh viên
 * 
 * @author PhamTuan
 *
 */
public class SinhVienController {

	/** sinh viên để quản lý các thông tin sinh viên */
	SinhVien sinhvien = new SinhVien();

	/** đối tượng SinhVienController dùng chung */
	private static SinhVienController instance;

	/**
	 * Hàm khởi tạo để private để các đối tượng bên ngoài không thể tùy ý khởi tạo
	 */
	private SinhVienController() {

	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của SinhVienController nếu
	 * đối tượng đố null
	 * 
	 * @return đối tượng SinhVienController dùng chung
	 */

	public static SinhVienController getInstance() {
		if (instance == null)
			instance = new SinhVienController();
		return instance;
	}

	public SinhVien getUser(String taikhoan) throws SQLException {
		return sinhvien.searchInfoUser(taikhoan);
	}

	/**
	 * Thực hiện cập nhập thông tin sinh viên, gồm email, số điện thoại, địa chỉ
	 * 
	 * @param email
	 * @param sdt:
	 *            số điện thoại
	 * @param diachi
	 *            : địa chỉ
	 * @param taikhoan
	 *            : mã tài khoản
	 * @return true nếu cập nhật thành công, false nếu thất bại
	 */
	public boolean UpdateInfoUser(String email, String sdt, String diachi, String taikhoan) {
		return sinhvien.UpdateInfoUser(email, sdt, diachi, taikhoan);
	}

	/**
	 * Thực hiện lấy danh sách thông tin sinh viên theo họ tên
	 * 
	 * @param hoten:
	 *            họ tên sinh viên
	 * @return danh sách sinh viên
	 */

	public List<SinhVien> getListUserByName(String hoten) {
		return sinhvien.getInfoUserbyName(hoten);
	}

	/**
	 * Thực hiện lấy tên sinh viên
	 * 
	 * @param taikhoan:
	 *            mã tài khoản
	 * @return tên sinh viên tương ứng với mã tài khoản
	 */
	public String getNameSV(String taikhoan) {
		return sinhvien.getNameSV(taikhoan);
	}

	/**
	 * Thực hiện lấy ngày sinh sinh viên
	 * 
	 * @param taikhoan:
	 *            mã tài khoản
	 * @return ngày sinh sinh viên
	 */
	public String getNgaySinhSV(String taikhoan) {
		return sinhvien.getNgaySinhSV(taikhoan);
	}

}
