package controller;

import java.util.List;

import entity.ThoiKhoaBieu;

/**
 * Thực hiển các điểu khiển liên quan đến ThoiKhoaBieu
 * 
 * @author PhamTuan
 *
 */
public class ThoiKhoaBieuController {

	/** tkb để quản lý thông tin thời khóa biểu */
	ThoiKhoaBieu tkb = new ThoiKhoaBieu();

	/** đối tượng ThoiKhoaBieuController dùng chung */
	private static ThoiKhoaBieuController instance;

	/**
	 * Hàm khởi tạo để private để các đối tượng bên ngoài không thể tùy ý khởi tạo
	 */
	private ThoiKhoaBieuController() {

	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của ThoiKhoaBieuController
	 * nếu đối tượng đố null
	 * 
	 * @return đối tượng ThoiKhoaBieuController dùng chung
	 */
	public static ThoiKhoaBieuController getInstance() {
		if (instance == null)
			instance = new ThoiKhoaBieuController();
		return instance;
	}

	/**
	 * Lấy thông tin danh sách thời khóa biểu
	 * 
	 * @param taikhoan:
	 *            mã tài khoản sinh viên
	 * @return danh sách thời khóa biểu
	 */
	public List<ThoiKhoaBieu> getListTKB(String taikhoan) {

		return tkb.searchTKB(taikhoan);

	}

}
