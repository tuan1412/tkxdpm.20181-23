package controller;

import java.sql.SQLException;
import java.util.List;

import common.UserHelper;
import entity.ChiTietLopHoc;
import entity.LopHoc;

/**
 * Class này để điều khiển các chức năng liên quan tới lớp học
 * 
 * @author TuanNguyen
 *
 */
public class LopHocController {

	/** lớp học để quản lý thông tin về lớp học */
	LopHoc lopHoc = new LopHoc();

	/** đối tượng LopHocController dùng chung */
	private static LopHocController instance;

	/**
	 * Hàm khởi tạo để private để các đối tượng bên ngoài không thể tùy ý khởi tạo
	 */
	private LopHocController() {
	}

	/**
	 * Hàm này khởi tạo cho đối tượng dùng chung duy nhất của HocPhiController nếu
	 * đối tượng đố null
	 * 
	 * @return đối tượng HocPhiController dùng chung
	 */
	public static LopHocController getInstance() {
		if (instance == null)
			instance = new LopHocController();
		return instance;
	}

	/**
	 * Hàm này để lấy thông tin của lớp học và sắp xếp lớp theo tên
	 * 
	 * @param malop
	 *            là mã lớp cần lấy thông tin
	 * @return thông tin về lớp học
	 * @throws SQLException
	 */
	public LopHoc searchLopHoc(String malop) {
		try {
			LopHoc lophoc = lopHoc.searchLopHoc(malop);
			if (lophoc == null)
				return null;
			lophoc.getChiTietList().sort((ChiTietLopHoc sv1, ChiTietLopHoc sv2) -> {
				String hoten1 = UserHelper.convertHoten(sv1.getTenSinhVien());
				String hoten2 = UserHelper.convertHoten(sv2.getTenSinhVien());
				return hoten1.compareTo(hoten2);
			});
			return lophoc;
		} catch (SQLException e) {
			return null;
		}
	}

	/**
	 * Hàm này để cập nhật điểm của lớp học
	 * 
	 * @param chiTietList
	 *            là danh sách điểm cần update
	 * @param lophoc
	 *            là lớp học cần update
	 * @return true nếu cập nhật thành công, false nếu thất bại
	 */
	public boolean updateDiemLopHoc(List<ChiTietLopHoc> chiTietList, LopHoc lophoc) {
		try {
			chiTietList.forEach(chitiet -> chitiet.setDiemChu(lophoc.getTrongSo()));
			return lopHoc.updateDiemLopHoc(chiTietList, lophoc.getMalop());
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} catch (NullPointerException e) {
			return false;
		}
	}

}
