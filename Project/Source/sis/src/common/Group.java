package common;

/**
 * Class này liệt kê các group chức năng có trong menu
 * 
 * @author Nhom23
 *
 */
public enum Group {

	/** Gồm các chức năng liên quan tới quản lý tài khoản */
	TAI_KHOAN("Tài khoản"),

	/** Gồm các chức năng liên quan tới tra cứu */
	TRA_CUU("Tra cứu"),

	/** Gồm các chức năng liên quan tới đăng kí học tập */
	DANG_KI("Đăng kí học tập"),

	/** Gồm các chức năng liên quan tới quản lý của admin */
	QUAN_LY("Quản lý"),

	/** Gồm các chức năng liên quan tới kế hoạch học tập của sinh viên */
	KE_HOACH_HOC_TAP("Kế hoạch học tập");

	/** name là tên của nhóm */
	private String name;

	Group(String name) {
		this.name = name;
	}

	/**
	 * Hàm này để lấy tên của nhóm
	 * 
	 * @return name là tên của nhóm
	 */
	public String getName() {
		return name;
	}
}
