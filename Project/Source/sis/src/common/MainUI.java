package common;

import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import entity.User;

/**
 * Class này để hiện thị màn hình dùng chung bao gồm menu và chức năng tương ứng
 * Các chức năng tương ứng với một class thừa kế JPanel và đặt layout khác
 * absoulte
 * 
 * @author Nhom23
 *
 */
public class MainUI {

	/** Đối tượng MainUI dùng chung */
	private static MainUI instance;

	/** Tài khoản của user */
	private String taikhoan;

	/** Mật khẩu của user */
	private String matkhau;

	/** panel hiện thị các chức năng tương ứng */
	private JPanel panel;

	/** frame là khung của chương trình */
	private JFrame frame;

	/**
	 * Constuctor để private để các đối tượng ngoài không khởi tạo MainUI
	 */
	private MainUI() {
		init();
	}

	/**
	 * Đối tượng MainUI dùng chung
	 * 
	 * @return
	 */
	public static MainUI getInstance() {
		if (instance == null) {
			instance = new MainUI();
		}
		return instance;
	}

	/**
	 * Để lấy thông tin tài khoản người dùng sau khi đăng nhập ở bất cứ view nào
	 * 
	 * @return taikhoan người dùng đã đăng nhập
	 */
	public String getTaiKhoan() {
		return taikhoan;
	}

	public String getMatKhau() {
		return matkhau;
	}

	/**
	 * Khởi tạo frame
	 */
	private void init() {
		frame = new JFrame();
		panel = new JPanel();
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
		frame.setSize(900, 530);
		frame.setContentPane(panel);

	}

	public void setView(User user) {
		ViewRoleInfo viewInfo = null;
		if (user == null) {
			viewInfo = new ViewKhach();
		} else {
			taikhoan = user.getTaikhoan();
			matkhau = user.getMatkhau();
			frame.setTitle(taikhoan);
			viewInfo = user.getRole().equals(Role.SINHVIEN) ? new ViewSinhVien() : new ViewAdmin();
		}
		JPanel jpanel = viewInfo.getMainView();
		panel.removeAll();
		panel.repaint();
		panel.revalidate();
		panel.add(jpanel);
		panel.repaint();
		panel.revalidate();

		Map<Group, List<Function>> menuFunctions = viewInfo.getMenuFunctions();
		JMenuBar menuBar = generateMenu(menuFunctions);
		frame.setJMenuBar(menuBar);
	}

	private JMenuBar generateMenu(Map<Group, List<Function>> menuFunctions) {
		JMenuBar menuBar = new JMenuBar();

		menuFunctions.forEach((group, functions) -> {
			JMenu menu = new JMenu(group.getName());
			functions.forEach(function -> {
				JMenuItem menuItem = new JMenuItem(function.getName());
				menu.add(menuItem);
				menuItem.addActionListener(event -> {
					JPanel jpanel = null;
					try {
						jpanel = (JPanel) Class.forName(function.getClazzName()).newInstance();
					} catch (ClassNotFoundException ex) {
						JOptionPane.showMessageDialog(null, function.getClazzName() + " not found.");
					} catch (InstantiationException ex) {
						Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
					} catch (IllegalAccessException ex) {
						Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
					}
					if (function.equals(Function.LOG_OUT)) {
						setView(null);
					} else {
						jpanel.setVisible(true);
						panel.removeAll();
						panel.repaint();
						panel.revalidate();
						panel.add(jpanel);
						panel.repaint();
						panel.revalidate();
					}

				});
			});
			menuBar.add(menu);
		});
		return menuBar;
	}
}
