package common;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import boundary.LoginForm;

/**
 * View của khách
 * 
 * @author Nhóm23
 *
 */
public class ViewKhach extends ViewRoleInfo {

	/** List hiện thị các nhóm có trong menu theo thứ tự của khách */
	private List<Group> orderGroup = Arrays.asList(Group.TAI_KHOAN, Group.KE_HOACH_HOC_TAP);

	@Override
	JPanel getMainView() {
		LoginForm loginForm = new LoginForm();
		return loginForm;
	}

	@Override
	public Map<Group, List<Function>> getMenuFunctions() {
		Map<Group, List<Function>> mapFunctions = new LinkedHashMap<>();
		orderGroup.forEach(group -> {
			mapFunctions.put(group, super.getFunctionByRoleAndGroup(Role.KHACH, group));
		});
		return mapFunctions;
	}

	public List<Group> getOrderGroup() {
		return orderGroup;
	}

}
