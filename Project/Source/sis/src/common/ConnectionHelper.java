package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Class này để tạo và đóng kết nối tới cơ sở dữ liệu
 * 
 * @author Nhom23
 *
 */
public class ConnectionHelper {

	private static String dbName = "chulunku_sis";
	private static String url = "mysql-sis-khaitq.cxq81fnd56et.ap-southeast-1.rds.amazonaws.com:3306";
	private static String username = "khaitq";
	private static String password = "khai871997";
	private static String dbPath = "jdbc:mysql://" + url + "/" + dbName + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&characterEncoding=UTF-8";
	private static Connection conn = null;

	/**
	 * Hàm này để kết nối với cơ sở dữ liệu
	 * 
	 * @return đối tượng connection để kết nối cơ sở dữ liệu
	 * @throws SQLException
	 */
	public static Connection connectDb() throws SQLException {
		conn = DriverManager.getConnection(dbPath, username, password);
		return conn;
	}

	/**
	 * Hàm này để đóng kết nối với cơ sở dữ liệu
	 * 
	 * @throws SQLException
	 */
	public static void closeDB() throws SQLException {
		if (conn != null)
			conn.close();
	}
}
