/**
 * 
 */
package controller;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import entity.SinhVien;

class SinhVienControllerTest {

	@Test
	public void testGetUser() throws SQLException {
		// return true if sinhvien.getTaiKhoan == taikhoan input
		assertEquals("20154101", SinhVienController.getInstance().getUser("20154101").getTaikhoan());
	}

	@Test
	public void testUpdateInfoUser() {
		// return false if mataikhoan isn't exist
		assertEquals(false,SinhVienController.getInstance().UpdateInfoUser("123hello@gmail.com", "01668455258", "HaNoi", "2018"));
		;
	}

	@Test
	public void testGetListUserByName() {
		// size 's list == 0 if name isn't exist
		assertEquals(0, SinhVienController.getInstance().getListUserByName("aaa").size());
	}

	@Test
	public void testGetNameSV() {
		// return '' if matai khoan isn't exist
		assertEquals("", SinhVienController.getInstance().getNameSV("2010"));
	}

	@Test
	public void testGetNgaySinhSV() {
		// return '' if matai khoan isn't exist
		assertEquals("", SinhVienController.getInstance().getNgaySinhSV("2010"));
	}

}
