package entity;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class BangDiemTest {

	@Test
	void testGetTaikhoan() {
		BangDiem bd = new BangDiem();
		bd.setTaikhoan("20154485");
		assertTrue(bd.getTaikhoan().equals("20154485"));
		assertFalse(bd.getTaikhoan().equals(""));
	}

	@Test
	void testGetMahp() {
		BangDiem bd = new BangDiem();
		bd.setMahp("IT4490");
		assertTrue(bd.getMahp().equals("IT4490"));
		assertFalse(bd.getMahp().equals(""));
	}

	@Test
	void testGetTenhp() {
		BangDiem bd = new BangDiem();
		bd.setTenhp("TKXDPM");
		assertTrue(bd.getTenhp().equals("TKXDPM"));
		assertFalse(bd.getTenhp().equals("abc"));
	}

	@Test
	void testGetSotinchi() {
		BangDiem bd = new BangDiem();
		bd.setSotinchi(3);
		assertTrue(bd.getSotinchi() == 3);
		assertFalse(bd.getSotinchi() == 4);
	}

	@Test
	void testGetMalop() {
		BangDiem bd = new BangDiem();
		bd.setMalop("123456");
		assertTrue(bd.getMalop().equals("123456"));
		assertFalse(bd.getMalop().equals("123"));
	}

	@Test
	void testGetDiemqt() {
		BangDiem bd = new BangDiem();
		bd.setDiemqt(8f);
		assertTrue(bd.getDiemqt() == 8f);
		assertFalse(bd.getDiemqt() == 5f);
	}

	@Test
	void testGetDiemthi() {
		BangDiem bd = new BangDiem();
		bd.setDiemthi(8f);
		assertTrue(bd.getDiemthi() == 8f);
		assertFalse(bd.getDiemthi() == 5f);
	}

	@Test
	void testGetKihoc() {
		BangDiem bd = new BangDiem();
		bd.setKihoc(20181);
		assertTrue(bd.getKihoc() == 20181);
		assertFalse(bd.getKihoc() == 20172);
	}

}
