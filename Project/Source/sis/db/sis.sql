CREATE DATABASE  IF NOT EXISTS `sis` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sis`;
-- MySQL dump 10.13  Distrib 5.7.11, for Win64 (x86_64)
--
-- Host: localhost    Database: sis
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `ngaysinh` varchar(45) DEFAULT NULL,
  `hoten` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `sdt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkq1_idx` (`taikhoan`),
  CONSTRAINT `fkq1` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','10.10.1995','Nguyễn Văn A','nguyenvana@gmail.com','01224264167');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bangdangkihocphan`
--

DROP TABLE IF EXISTS `bangdangkihocphan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bangdangkihocphan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mahp` varchar(45) NOT NULL,
  `ngaydk` varchar(45) NOT NULL,
  `trangthaidk` varchar(45) NOT NULL,
  `kihoc` varchar(45) NOT NULL,
  `taikhoan` varchar(45) NOT NULL,
  `tenhp` varchar(45) NOT NULL,
  `soTinChi` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk41_idx` (`mahp`),
  KEY `fk42_idx` (`taikhoan`),
  CONSTRAINT `fk41` FOREIGN KEY (`mahp`) REFERENCES `hocphan` (`mahp`),
  CONSTRAINT `fk42` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bangdangkihocphan`
--

LOCK TABLES `bangdangkihocphan` WRITE;
/*!40000 ALTER TABLE `bangdangkihocphan` DISABLE KEYS */;
INSERT INTO `bangdangkihocphan` VALUES (1,'IT4843','2018.11.04','Success','20183','20154101','Tích hợp dữ liệu và XML	',3),(2,'IT4490','2018.11.04','Success','20171','20154101','Thiết kế và xây dựng phần mềm	',3),(3,'IT4843','2018.11.04','Success','20171','20154101','Tích hợp dữ liệu và XML	',3),(4,'IT4490','2018.11.04','Success','20183','20154101','Thiết kế và xây dựng phần mềm	',3),(5,'IT4440','2018.11.04','Success','20183','20154101','Tương tác Người –Máy	',3),(6,'IT4520','2018.11.04','Success','20183','20154101','Kinh tế công nghệ phần mềm	',2),(7,'IT4310','2018.11.04','Success','20183','20154101','Cơ sở dữ liệu nâng cao	',3),(8,'IT4843','2018.11.04','Success','20171','20154101','Tích hợp dữ liệu và XML	',3),(9,'IT4440','2018.11.04','Success','20181','20154101','Tương tác Người –Máy	',3);
/*!40000 ALTER TABLE `bangdangkihocphan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bangdiemtoeic`
--

DROP TABLE IF EXISTS `bangdiemtoeic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bangdiemtoeic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `hocki` varchar(45) NOT NULL,
  `ngaythi` varchar(45) NOT NULL,
  `diemnghe` int(11) NOT NULL,
  `diemdoc` int(11) NOT NULL,
  `diemtong` int(11) NOT NULL,
  `ghichu` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fktoeic_idx` (`taikhoan`),
  CONSTRAINT `fktoeic` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bangdiemtoeic`
--

LOCK TABLES `bangdiemtoeic` WRITE;
/*!40000 ALTER TABLE `bangdiemtoeic` DISABLE KEYS */;
INSERT INTO `bangdiemtoeic` VALUES (1,'20154101','20171','1.1.2017',400,400,800,'Toeic lần 40'),(2,'20154101','20181','1.2.2018',300,350,650,'Toeic lần 42'),(3,'20154101','20181','1.3.2018',400,200,600,'Toeic lần 42'),(4,'20154101','20172','1.2.2018',200,200,400,'Toeic lần 41'),(23,'20154136','20172','9.2.2017',300,300,600,'Toeic nội bộ lần 25'),(24,'20154136','20161','1.2.2016',390,300,690,'Toeic nội bộ lần 20'),(25,'20154136','20153','12.7.2015',200,300,500,'Toeic nội bộ lần 15'),(26,'20154100','20172','1.2.2017',300,300,600,'Toeic lần 35'),(27,'20154102','20172','1.2.2017',300,320,620,'Toeic lần 35'),(28,'20140076','20172','1.2.2017',300,300,600,'Toeic lần 35'),(29,'20140241','20172','1.2.2017',300,200,500,'Toeic lần 35'),(30,'20152000','20173','7.7.2017',450,450,900,'Toeic quốc tế lần 20');
/*!40000 ALTER TABLE `bangdiemtoeic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hocphan`
--

DROP TABLE IF EXISTS `hocphan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hocphan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mahp` varchar(45) NOT NULL,
  `tenhp` varchar(90) NOT NULL,
  `soTinChi` int(11) NOT NULL,
  `tinChiHocPhi` float NOT NULL,
  `thoiLuong` varchar(45) NOT NULL,
  `trongSo` float NOT NULL,
  PRIMARY KEY (`id`,`mahp`),
  UNIQUE KEY `mahp_UNIQUE` (`mahp`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hocphan`
--

LOCK TABLES `hocphan` WRITE;
/*!40000 ALTER TABLE `hocphan` DISABLE KEYS */;
INSERT INTO `hocphan` VALUES (1,'IT4490','Thiết kế và xây dựng phần mềm	',3,4,'3(3-1-0-6)	',0.6),(2,'IT4843','Tích hợp dữ liệu và XML',3,4,'3(3-1-0-6)	',0.7),(3,'IT4440','Tương tác Người –Máy',3,4,'3(3-1-0-6)	',0.7),(4,'IT4520','Kinh tế công nghệ phần mềm',2,3,'2(2-1-0-4)	',0.7),(5,'IT4310','Cơ sở dữ liệu nâng cao',3,4,'3(3-1-0-6)	',0.7),(6,'IT4551','Phát triển phần mềm chuyên nghiệp',3,4,'3(2-2-0-6)	',0.7),(7,'IT4541','Quản lý dự án phần mềm',2,3,'2(2-1-0-4)	',0.6),(9,'﻿IT3914','Đồ án lập trình',2,4,'2(0-0-0-4)',0.7),(10,'IT3804','Thực tập doanh nghiệp',2,4,'2(0-0-6-4)',0.7),(11,'IT4364','Phân tích thiết kế hệ thống',3,4,'3(2-2-0-6)',0.7),(12,'IT5334','Hệ thống thông tin địa lý',3,4,'3(2-2-0-6)',0.7),(13,'IT4044','Trí tuệ nhân tạo',3,3,'3(3-0-0-6)',0.7),(14,'IT4434','Đồ án các công nghệ xây dựng hệ thống thông tin',3,6,'3(0-0-6-6)',0.7),(15,'IT4694','Mạng số và truyền dữ liệu',3,4.5,'3(2-1-1-6)',0.7),(16,'IT4244','Quản trị dự án CNTT',2,3,'2(2-1-0-4)',0.7),(17,'IT5384','Lưu trữ và phân tích dữ liệu lớn',3,4,'3(2-2-0-6)',0.7),(18,'IT4324','An toàn trong giao dịch điện tử',2,3,'2(2-1-0-6)',0.7),(19,'IT5904','Đồ án tốt nghiệp',12,30,'12(0-0-24-48)',0.5),(20,'IT5130','Đồ án tốt nghiệp Kỹ sư',12,30,'12(0-0-24-48)',0.5),(21,'IT5030E','Graduation research 3',3,7.5,'3(0-0-6-6)',0.5),(22,'IT5120E','Thesis',9,22.5,'9(0-0-18-18)',0.5),(23,'IT4015','Nhập môn an toàn thông tin',3,4,'3(3-1-0-6)',0.6),(24,'IT3062','Toán chuyên đề',2,3,'2(2-1-0-4)',0.7),(25,'IT1140','Tin học đại cương',4,5.5,'4(3-1-1-8)',0.5),(26,'IT3045','Kỹ thuật lập trình an toàn',2,3,'2(2-1-0-4)',0.7),(27,'IT4025','Mật mã ứng dụng',3,4,'3(3-1-0-6)',0.7),(28,'IT4190','An toàn hệ thống',3,4,'3(3-1-0-6)',0.7),(29,'IT4263','An ninh mạng',3,5,'3(2-0-2-6)',0.6),(30,'IT4220','Quản trị an toàn thông tin và rủi ro',2,2,'2(2-0-0-4)',0.6),(31,'IT4830','Phòng chống tấn công mạng',3,3.5,'3(2-0-1-6)',0.6),(32,'IT4403','Phát triển hệ thống Web an toàn',2,3,'2(2-1-0-4)',0.7),(33,'IT4911','Đồ án môn học (Thiết kế hệ thống ATTT)',3,6,'3(0-0-6-12)',0.7),(34,'IT4810','Đánh giá kiểm định an toàn hệ thống thông tin',3,4.5,'3(2-1-1-6)',0.7),(35,'IT4320','An toàn trong giao dịch điện tử',3,4,'3(3-1-0-6)',0.7),(36,'IT4450','Cơ sở pháp lý số',2,3.5,'2(2-0-1-4)',0.7),(37,'IT4570','Phát hiện lỗi và lỗ hổng bảo mật phần mềm',2,3.5,'2(2-0-1-4)',0.7),(38,'IT4580','Kỹ thuật phần mềm an toàn',2,2,'2(2-0-0-4)',0.7),(39,'IT4630','Phân tích mã độc',2,3,'2(2-1-0-4)',0.7),(40,'IT4640','Thu thập và phân tích thông tin an ninh mạng',2,3.5,'2(2-0-1-4)',0.7),(41,'IT4730','An toàn cơ sở dữ liệu',2,2,'2(2-0-0-4)',0.7),(42,'IT4840','Chuyên đề An toàn an ninh hệ thống',2,2,'2(2-0-0-4)',0.7),(43,'IT4910','Điện toán đám mây',2,3,'2(2-1-0-4)',0.7),(44,'IT5260','Đồ án tốt nghiệp kỹ sư (ATTT)',12,30,'12(0-0-24-24)',0.5),(45,'IT4074','Lý thuyết ngôn ngữ và phương pháp dịch',3,4,'3(3-1-0-6)',0.5),(46,'IT4312E','Data Modeling',2,2,'2(2-0-0-4)',0.7),(47,'IT4492E','Structured Programming',2,2,'2(2-0-0-4)',0.7),(48,'IT4592E','Information Theory',2,2,'2(2-0-0-4)',0.7),(49,'IT3402','Hệ thống thông tin trên Web',2,2,'2(2-0-0-4)',0.7),(50,'IT4552','Thực hành Lập trình Web',2,6,'2(0-0-4-4)',0.7),(51,'IT4062','Thực hành Lập trình mạng',2,6,'2(0-0-4-4)',0.7),(52,'IT4272','Hệ thống máy tinh',2,2,'2(2-0-0-4)',0.7),(53,'IT4492','Lập trình cấu trúc',2,2,'2(2-0-0-4)',0.7),(54,'IT4312','Mô hình hóa dữ liệu',2,2,'2(2-0-0-4)',0.7),(55,'IT4940','Project 3',3,6,'3(0-0-6-12)',0.7),(56,'IT4012','Bảo mật thông tin',2,2,'2(2-0-0-4)',0.7),(57,'IT4132E','System Program',2,2,'2(2-0-0-4)',0.7),(58,'IT4262E','Network Security',2,2,'2(2-0-0-4)',0.7),(59,'IT4152E','Network Software Architecture',2,2,'2(2-0-0-4)',0.7),(60,'IT4652E','Internetworking',2,2,'2(2-0-0-4)',0.7),(61,'IT4362E','Knowledge Engineering',2,2,'2(2-0-0-4)',0.7),(62,'IT4212E','Realtime System',2,2,'2(2-0-0-4)',0.7),(63,'IT4542E','Management of Software Development',2,2,'2(2-0-0-4)',0.7),(64,'IT5022E','Graduation Research 2',3,7.5,'3(0-0-6-6)',0.5),(65,'IT5xxx','Đồ án tốt nghiệp (KSTN CNTT K55)',12,30,'12(0-0-24-48)',0.5),(72,'IT1010','Tin học đại cương',4,6,'3(2-2-0-6)',0.6),(73,'IT3910','Project I',3,7.5,'3(2-2-0-6)',0.5),(74,'IT3920','Project II',3,7.5,'3(2-2-0-6)',0.5),(75,'IT4409','Công nghệ web',3,7.5,'3(2-2-0-6)',0.5),(76,'IT4010','An toàn và bảo mật thông tin',3,7.5,'3(2-2-0-6)',0.5),(77,'IT3010','Cấu trúc dữ liệu và giải thuật',3,7.5,'3(2-2-0-6)',0.5),(78,'IT3020','Toán rời rạc',3,7.5,'3(2-2-0-6)',0.5),(79,'IT3030','Kiến trúc máy tính',3,7.5,'3(2-2-0-6)',0.5),(80,'IT3040','Kỹ thuật lập trình',3,7.5,'3(2-2-0-6)',0.5),(81,'IT3070','Hệ điều hành',3,7.5,'3(2-2-0-6)',0.5),(82,'IT3080','Mạng máy tính',3,7.5,'3(2-2-0-6)',0.5),(83,'IT3090','Cơ sở dữ liệu',2,7.5,'3(2-2-0-6)',0.5),(84,'IT3100','Lập trình hướng đối tượng',2,7.5,'3(2-2-0-6)',0.5),(85,'IT3110','Linux và phần mềm mã nguồn mở',2,7.5,'3(2-2-0-6)',0.5),(86,'IT3120','Phân tích và thiết kế hệ thống thông tin',2,7.5,'3(2-2-0-6)',0.5),(87,'IT4040','Trí tuệ nhân tạo',2,7.5,'3(2-2-0-6)',0.5),(88,'IT4480','Làm việc nhóm và kỹ năng giao tiếp',2,7.5,'3(2-2-0-6)',0.5),(89,'IT4500','Đảm bảo chất lượng phần mềm',2,7.5,'3(2-2-0-6)',0.5);
/*!40000 ALTER TABLE `hocphan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lophoc`
--

DROP TABLE IF EXISTS `lophoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lophoc` (
  `idlophoc` int(11) NOT NULL AUTO_INCREMENT,
  `malop` varchar(45) NOT NULL,
  `thoigian` varchar(45) NOT NULL,
  `phonghoc` varchar(45) NOT NULL,
  `tuanhoc` varchar(45) NOT NULL,
  `kihoc` int(11) NOT NULL,
  `mahp` varchar(45) NOT NULL,
  PRIMARY KEY (`idlophoc`),
  KEY `fkl1_idx` (`mahp`),
  KEY `fkl2_idx` (`kihoc`),
  KEY `fll2_idx` (`kihoc`),
  CONSTRAINT `fkl1` FOREIGN KEY (`mahp`) REFERENCES `hocphan` (`mahp`),
  CONSTRAINT `fll2` FOREIGN KEY (`kihoc`) REFERENCES `thongtinkihoc` (`kihoc`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lophoc`
--

LOCK TABLES `lophoc` WRITE;
/*!40000 ALTER TABLE `lophoc` DISABLE KEYS */;
INSERT INTO `lophoc` VALUES (1,'104444','Thứ 2,12h30 - 15h50	','TC-207	','2-9,11-18	',20181,'IT4490'),(2,'104442','Thứ 3,12h30 - 15h50	','D8-304	','2-9,11-18	',20181,'IT4843'),(3,'104443','Thứ 4,14h15 - 17h35	','TC-207	','2-9,11-18	',20181,'IT4440'),(4,'104448','Thứ 5,12h30 - 15h0	','T-409	','2-9,11-18	',20181,'IT4520'),(5,'104438','Thứ 6,14h15 - 17h35	','D8-304	','2-9,11-18	',20181,'IT4310'),(6,'100000','Thứ 6,14h15 - 17h35','D8-304','2-9,11-18',20172,'IT4310'),(7,'104439','Thứ 2, 6h45-9h15','TC-201','2-9,11-18	',20181,'IT4551'),(8,'104440','Thứ 2, 9h20-11h50','TC-201','2-9,11-18	',20181,'IT4541'),(9,'104441','Thứ 3, 6h45-9h15','TC-202','2-9,11-18	',20181,'IT1010'),(10,'104445','Thứ 3, 9h20-11h50','TC-202','2-9,11-18	',20181,'IT3910'),(11,'104446','Thứ 4, 6h45-9h15','D9-101','2-9,11-18	',20181,'IT3920'),(12,'104447','Thứ 4, 9h20-11h50','D9-101','2-9,11-18	',20181,'IT4409'),(13,'104449','Thứ 5, 6h45-9h15','D5-102','2-9,11-18	',20181,'IT4010'),(14,'104450','Thứ 5, 9h20-11h50','D5-102','2-9,11-18	',20181,'IT3010'),(15,'104451','Thứ 6, 6h45-9h15','D3-401','2-9,11-18	',20181,'IT3020'),(16,'104452','Thứ 6, 9h20-11h50','D3-401','2-9,11-18	',20181,'IT3030');
/*!40000 ALTER TABLE `lophoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `lophoc_view`
--

DROP TABLE IF EXISTS `lophoc_view`;
/*!50001 DROP VIEW IF EXISTS `lophoc_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `lophoc_view` AS SELECT 
 1 AS `mssv`,
 1 AS `hoten`,
 1 AS `mahp`,
 1 AS `tenhp`,
 1 AS `thoiLuong`,
 1 AS `trongSo`,
 1 AS `soTinChi`,
 1 AS `tinChiHocPhi`,
 1 AS `idlophoc`,
 1 AS `malop`,
 1 AS `kihoc`,
 1 AS `thoigian`,
 1 AS `tuanhoc`,
 1 AS `phonghoc`,
 1 AS `diemqt`,
 1 AS `diemthi`,
 1 AS `diemchu`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sinhvien`
--

DROP TABLE IF EXISTS `sinhvien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sinhvien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `hoten` varchar(45) NOT NULL,
  `ngaysinh` varchar(45) DEFAULT NULL,
  `lop` varchar(45) DEFAULT NULL,
  `chuongtrinhhoc` varchar(45) DEFAULT NULL,
  `hehoc` varchar(45) DEFAULT NULL,
  `trangthai` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `sdt` varchar(45) DEFAULT NULL,
  `diachi` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkq3_idx` (`taikhoan`),
  CONSTRAINT `fkq3` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sinhvien`
--

LOCK TABLES `sinhvien` WRITE;
/*!40000 ALTER TABLE `sinhvien` DISABLE KEYS */;
INSERT INTO `sinhvien` VALUES (1,'20154101','Nguyễn Anh Tuấn','14.12.1997','CNTT2-4','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học','anhtuantoan1215@gmail.com','0123456789','as'),(2,'20150672','Mai Tiến Dũng ','10.06.1997','CNTT2-4 ','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học','maitiendung@gmail.com','01672048460','bắc ninh'),(3,'20151101','Nguyễn Văn An','12.10.1997','CNTT2-4 ','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học','an@gmail.com','01672048465','Ha Noi'),(8,'20154100','Nguyễn Tuấn','14.10.1997','CNTT2-3','CT Nhóm ngành CNTT-TT 1-2015','Đại học','Học','ngtuan@gmail.com','01668455259','Ha Noi'),(9,'20154102','Lê Văn Tuấn','12.10.1997','CNTT2-3','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học','lvtuan@gmail.com','01668455257','Ha Noi'),(10,'20154103','Nguyễn Thành Nam','11.10.1997','CNTT2-2 ','CT Nhóm ngành CNTT-TT 1-2015','Đại học','Học','thanhnam@gmail.com','01668455256','Ha Noi'),(11,'20154104','Lê Văn Tài','10.10.1997','CNTT2-2 ','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học','lvtai@gmail.com','01668455255','Ha Noi'),(12,'20154105','Nguyễn Văn Anh','09.10.1997','CN.CNTT','CT CN.CNTT 2015','Cử nhân','Học','nguyenvananh@gmail.com','01668455254','Ha Noi'),(13,'20154106','Lê Thị Lan','08.10.1997','CN.CNTT','CT CN.CNTT 2015','Cử nhân','Học','lelan@gmail.com','01668455253','Ha Noi'),(14,'20154485','Nguyễn Hữu Tùng','20.02.1997','CNTT2-4 K60','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học',NULL,NULL,NULL),(15,'20150010','Nguyễn Thành An','12.03.1996','CN.CNTT','CT CN.CNTT 2015','Cử nhân','Học',NULL,NULL,NULL),(16,'20154136','Phạm Văn Tuấn','05.09.1997','CNTT2-2 ','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học',NULL,NULL,NULL),(17,'20150021','Lê Hằng','06.09.1997','CN.CNTT','CT CN.CNTT 2015','Cử nhân','Học',NULL,NULL,NULL),(18,'20140076','Chu Văn An','07.09.1996','CN.CNTT','CT CN.CNTT 2014','Cử nhân','Học',NULL,NULL,NULL),(19,'20140241','Lê Văn Anh','08.09.1996','CNTT2-2 ','CT Nhóm ngành CNTT-TT 2-2014','Đại học','Học',NULL,NULL,NULL),(20,'20140400','Nguyễn Đức Cường','09.09.1996','CNTT2-2 ','CT Nhóm ngành CNTT-TT 2-2014','Đại học','Học',NULL,NULL,NULL),(21,'20146084','Nguyễn Thị Duyên','15.09.1996','CNTT2-2 ','CT Nhóm ngành CNTT-TT 2-2014','Đại học','Học',NULL,NULL,NULL),(22,'20152000','Nguyễn Anh Tuấn','12.03.1996','CN.CNTT','CN CNTT 2015','Cử nhân','Học',NULL,NULL,NULL);
/*!40000 ALTER TABLE `sinhvien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sinhvien_lophoc`
--

DROP TABLE IF EXISTS `sinhvien_lophoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sinhvien_lophoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `idlophoc` int(11) NOT NULL,
  `diemqt` float DEFAULT NULL,
  `diemthi` float DEFAULT NULL,
  `diemchu` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fksl1_idx` (`taikhoan`),
  KEY `fksl2_idx` (`idlophoc`),
  CONSTRAINT `fksl1` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`),
  CONSTRAINT `fksl2` FOREIGN KEY (`idlophoc`) REFERENCES `lophoc` (`idlophoc`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sinhvien_lophoc`
--

LOCK TABLES `sinhvien_lophoc` WRITE;
/*!40000 ALTER TABLE `sinhvien_lophoc` DISABLE KEYS */;
INSERT INTO `sinhvien_lophoc` VALUES (1,'20154101',1,8,9,'A'),(2,'20154101',2,NULL,NULL,NULL),(3,'20154101',3,NULL,NULL,NULL),(4,'20154101',4,NULL,NULL,NULL),(5,'20154101',5,NULL,NULL,NULL),(6,'20150672',1,8,7,'B'),(7,'20151101',1,NULL,NULL,NULL),(8,'20154101',6,8,7,'B'),(9,'20150672',6,6,9,'B+'),(10,'20151101',6,8,8,'B+'),(36,'20154485',2,NULL,NULL,NULL),(37,'20154485',3,NULL,NULL,NULL),(38,'20154485',4,NULL,NULL,NULL),(39,'20154485',5,NULL,NULL,NULL),(40,'20154485',6,NULL,NULL,NULL),(41,'20150010',3,NULL,NULL,NULL),(42,'20150010',4,NULL,NULL,NULL),(43,'20150010',5,NULL,NULL,NULL),(44,'20150010',6,NULL,NULL,NULL),(45,'20150010',7,NULL,NULL,NULL),(46,'20154136',4,NULL,NULL,NULL),(47,'20154136',5,NULL,NULL,NULL),(48,'20154136',6,NULL,NULL,NULL),(49,'20154136',7,NULL,NULL,NULL),(50,'20154136',8,NULL,NULL,NULL),(51,'20150021',5,NULL,NULL,NULL),(52,'20150021',6,NULL,NULL,NULL),(53,'20150021',7,NULL,NULL,NULL),(54,'20150021',8,NULL,NULL,NULL),(55,'20150021',9,NULL,NULL,NULL),(56,'20140076',1,9,9,'A'),(57,'20140076',3,NULL,NULL,NULL),(58,'20140076',5,NULL,NULL,NULL),(59,'20140076',7,NULL,NULL,NULL),(60,'20140076',9,NULL,NULL,NULL),(61,'20150078',2,NULL,NULL,NULL),(62,'20150078',4,NULL,NULL,NULL),(63,'20150078',6,NULL,NULL,NULL),(64,'20150078',8,NULL,NULL,NULL),(65,'20150078',10,NULL,NULL,NULL),(66,'20140241',1,NULL,NULL,NULL),(67,'20140241',9,NULL,NULL,NULL),(68,'20140241',7,NULL,NULL,NULL),(69,'20140241',5,NULL,NULL,NULL),(70,'20140241',3,NULL,NULL,NULL),(71,'20140400',1,NULL,NULL,NULL),(72,'20140400',2,NULL,NULL,NULL),(73,'20140400',5,NULL,NULL,NULL),(74,'20140400',4,NULL,NULL,NULL),(75,'20140400',3,NULL,NULL,NULL),(91,'20146084',3,NULL,NULL,NULL),(92,'20146084',2,NULL,NULL,NULL),(93,'20146084',4,NULL,NULL,NULL),(94,'20146084',1,8,8,'B+'),(95,'20146084',5,NULL,NULL,NULL),(96,'20140514',5,NULL,NULL,NULL),(97,'20140514',7,NULL,NULL,NULL),(98,'20140514',3,NULL,NULL,NULL),(99,'20140514',2,NULL,NULL,NULL),(100,'20140514',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sinhvien_lophoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thongtinkihoc`
--

DROP TABLE IF EXISTS `thongtinkihoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thongtinkihoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kihoc` int(11) NOT NULL,
  `hocphi` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kihoc_UNIQUE` (`kihoc`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thongtinkihoc`
--

LOCK TABLES `thongtinkihoc` WRITE;
/*!40000 ALTER TABLE `thongtinkihoc` DISABLE KEYS */;
INSERT INTO `thongtinkihoc` VALUES (1,20171,205000),(2,20172,215000),(3,20181,230000),(5,20182,235000);
/*!40000 ALTER TABLE `thongtinkihoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `matkhau` varchar(45) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`,`taikhoan`),
  UNIQUE KEY `taikhoan_UNIQUE` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','25d55ad283aa400af464c76d713c07ad',1),(2,'20154101','25d55ad283aa400af464c76d713c07ad',2),(3,'20150672','25d55ad283aa400af464c76d713c07ad',2),(4,'20151101','25d55ad283aa400af464c76d713c07ad',2),(6,'20154100','25d55ad283aa400af464c76d713c07ad',2),(7,'20154102','25d55ad283aa400af464c76d713c07ad',2),(8,'20154103','25d55ad283aa400af464c76d713c07ad',2),(9,'20154104','25d55ad283aa400af464c76d713c07ad',2),(10,'20154105','25d55ad283aa400af464c76d713c07ad',2),(11,'20154106','25d55ad283aa400af464c76d713c07ad',2),(12,'20150010','25d55ad283aa400af464c76d713c07ad',2),(13,'20154485','25d55ad283aa400af464c76d713c07ad',2),(14,'20154136','25d55ad283aa400af464c76d713c07ad',2),(15,'20150021','25d55ad283aa400af464c76d713c07ad',2),(16,'20140076','25d55ad283aa400af464c76d713c07ad',2),(17,'20150078','25d55ad283aa400af464c76d713c07ad',2),(18,'20140241','25d55ad283aa400af464c76d713c07ad',2),(19,'20140400','25d55ad283aa400af464c76d713c07ad',2),(20,'20146084','25d55ad283aa400af464c76d713c07ad',2),(21,'20140514','25d55ad283aa400af464c76d713c07ad',2),(23,'20152000','25d55ad283aa400af464c76d713c07ad',2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `lophoc_view`
--

/*!50001 DROP VIEW IF EXISTS `lophoc_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `lophoc_view` AS select `sinhvien`.`taikhoan` AS `mssv`,`sinhvien`.`hoten` AS `hoten`,`hocphan`.`mahp` AS `mahp`,`hocphan`.`tenhp` AS `tenhp`,`hocphan`.`thoiLuong` AS `thoiLuong`,`hocphan`.`trongSo` AS `trongSo`,`hocphan`.`soTinChi` AS `soTinChi`,`hocphan`.`tinChiHocPhi` AS `tinChiHocPhi`,`lophoc`.`idlophoc` AS `idlophoc`,`lophoc`.`malop` AS `malop`,`lophoc`.`kihoc` AS `kihoc`,`lophoc`.`thoigian` AS `thoigian`,`lophoc`.`tuanhoc` AS `tuanhoc`,`lophoc`.`phonghoc` AS `phonghoc`,`sinhvien_lophoc`.`diemqt` AS `diemqt`,`sinhvien_lophoc`.`diemthi` AS `diemthi`,`sinhvien_lophoc`.`diemchu` AS `diemchu` from (((`hocphan` join `lophoc` on((`hocphan`.`mahp` = `lophoc`.`mahp`))) join `sinhvien_lophoc` on((`lophoc`.`idlophoc` = `sinhvien_lophoc`.`idlophoc`))) join `sinhvien` on((`sinhvien_lophoc`.`taikhoan` = `sinhvien`.`taikhoan`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-15  9:55:01
