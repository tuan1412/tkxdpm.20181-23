CREATE DATABASE  IF NOT EXISTS `sis` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `sis`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: sis
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `ngaysinh` varchar(45) DEFAULT NULL,
  `hoten` varchar(45) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `sdt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkq1_idx` (`taikhoan`),
  CONSTRAINT `fkq1` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin','10-10-1992','Nguyễn Văn A','nguyenvana@gmail.com','01224264167');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bangdangkihocphan`
--

DROP TABLE IF EXISTS `bangdangkihocphan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bangdangkihocphan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mahp` varchar(45) NOT NULL,
  `ngaydk` varchar(45) NOT NULL,
  `trangthaidk` varchar(45) NOT NULL,
  `kihoc` varchar(45) NOT NULL,
  `taikhoan` varchar(45) NOT NULL,
  `tenhp` varchar(45) NOT NULL,
  `soTinChi` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk41_idx` (`mahp`),
  KEY `fk42_idx` (`taikhoan`),
  CONSTRAINT `fk41` FOREIGN KEY (`mahp`) REFERENCES `hocphan` (`mahp`),
  CONSTRAINT `fk42` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bangdangkihocphan`
--

LOCK TABLES `bangdangkihocphan` WRITE;
/*!40000 ALTER TABLE `bangdangkihocphan` DISABLE KEYS */;
INSERT INTO `bangdangkihocphan` VALUES (1,'IT4843','2018-11-04','Success','20183','20154101','Tích hợp dữ liệu và XML	',3),(2,'IT4490','2018-11-04','Success','20171','20154101','Thiết kế và xây dựng phần mềm	',3),(3,'IT4843','2018-11-04','Success','20171','20154101','Tích hợp dữ liệu và XML	',3),(4,'IT4490','2018-11-04','Success','20183','20154101','Thiết kế và xây dựng phần mềm	',3),(5,'IT4440','2018-11-04','Success','20183','20154101','Tương tác Người –Máy	',3),(6,'IT4520','2018-11-04','Success','20183','20154101','Kinh tế công nghệ phần mềm	',2),(7,'IT4310','2018-11-04','Success','20183','20154101','Cơ sở dữ liệu nâng cao	',3),(8,'IT4843','2018-11-04','Success','20171','20154101','Tích hợp dữ liệu và XML	',3),(9,'IT4440','2018-11-04','Success','20181','20154101','Tương tác Người –Máy	',3);
/*!40000 ALTER TABLE `bangdangkihocphan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bangdiemtoeic`
--

DROP TABLE IF EXISTS `bangdiemtoeic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bangdiemtoeic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `hocki` varchar(45) NOT NULL,
  `ngaythi` varchar(45) NOT NULL,
  `diemnghe` int(11) NOT NULL,
  `diemdoc` int(11) NOT NULL,
  `diemtong` int(11) NOT NULL,
  `ghichu` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fktoeic_idx` (`taikhoan`),
  CONSTRAINT `fktoeic` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bangdiemtoeic`
--

LOCK TABLES `bangdiemtoeic` WRITE;
/*!40000 ALTER TABLE `bangdiemtoeic` DISABLE KEYS */;
/*!40000 ALTER TABLE `bangdiemtoeic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hocphan`
--

DROP TABLE IF EXISTS `hocphan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hocphan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mahp` varchar(45) NOT NULL,
  `tenhp` varchar(90) NOT NULL,
  `soTinChi` int(11) NOT NULL,
  `tinChiHocPhi` float NOT NULL,
  `thoiLuong` varchar(45) NOT NULL,
  `trongSo` float NOT NULL,
  PRIMARY KEY (`id`,`mahp`),
  UNIQUE KEY `mahp_UNIQUE` (`mahp`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hocphan`
--

LOCK TABLES `hocphan` WRITE;
/*!40000 ALTER TABLE `hocphan` DISABLE KEYS */;
INSERT INTO `hocphan` VALUES (1,'IT4490','Thiết kế và xây dựng phần mềm	',3,4,'3(3-1-0-6)	',0.6),(2,'IT4843','Tích hợp dữ liệu và XML	',3,4,'3(3-1-0-6)	',0.7),(3,'IT4440','Tương tác Người –Máy	',3,4,'3(3-1-0-6)	',0.7),(4,'IT4520','Kinh tế công nghệ phần mềm	',2,3,'2(2-1-0-4)	',0.7),(5,'IT4310','Cơ sở dữ liệu nâng cao	',3,4,'3(3-1-0-6)	',0.7),(6,'IT4551','Phát triển phần mềm chuyên nghiệp	',3,4,'3(2-2-0-6)	',0.7),(7,'IT4541','Quản lý dự án phần mềm	',2,3,'2(2-1-0-4)	',0.6);
/*!40000 ALTER TABLE `hocphan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lophoc`
--

DROP TABLE IF EXISTS `lophoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lophoc` (
  `idlophoc` int(11) NOT NULL AUTO_INCREMENT,
  `malop` varchar(45) NOT NULL,
  `thoigian` varchar(45) NOT NULL,
  `phonghoc` varchar(45) NOT NULL,
  `tuanhoc` varchar(45) NOT NULL,
  `kihoc` int(11) NOT NULL,
  `mahp` varchar(45) NOT NULL,
  PRIMARY KEY (`idlophoc`),
  KEY `fkl1_idx` (`mahp`),
  KEY `fkl2_idx` (`kihoc`),
  KEY `fll2_idx` (`kihoc`),
  CONSTRAINT `fkl1` FOREIGN KEY (`mahp`) REFERENCES `hocphan` (`mahp`),
  CONSTRAINT `fll2` FOREIGN KEY (`kihoc`) REFERENCES `thongtinkihoc` (`kihoc`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lophoc`
--

LOCK TABLES `lophoc` WRITE;
/*!40000 ALTER TABLE `lophoc` DISABLE KEYS */;
INSERT INTO `lophoc` VALUES (1,'104444','Thứ 2,12h30 - 15h50	','TC-207	','2-9,11-18	',20181,'IT4490'),(2,'104442','Thứ 3,12h30 - 15h50	','D8-304	','2-9,11-18	',20181,'IT4843'),(3,'104443','Thứ 4,14h15 - 17h35	','TC-207	','2-9,11-18	',20181,'IT4440'),(4,'104448','Thứ 5,12h30 - 15h0	','T-409	','2-9,11-18	',20181,'IT4520'),(5,'104438','Thứ 6,14h15 - 17h35	','D8-304	','2-9,11-18	',20181,'IT4310');
/*!40000 ALTER TABLE `lophoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sinhvien`
--

DROP TABLE IF EXISTS `sinhvien`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sinhvien` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `hoten` varchar(45) NOT NULL,
  `ngaysinh` varchar(45) DEFAULT NULL,
  `lop` varchar(45) DEFAULT NULL,
  `chuongtrinhhoc` varchar(45) DEFAULT NULL,
  `hehoc` varchar(45) DEFAULT NULL,
  `trangthai` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `sdt` varchar(45) DEFAULT NULL,
  `diachi` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fkq3_idx` (`taikhoan`),
  CONSTRAINT `fkq3` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sinhvien`
--

LOCK TABLES `sinhvien` WRITE;
/*!40000 ALTER TABLE `sinhvien` DISABLE KEYS */;
INSERT INTO `sinhvien` VALUES (1,'20154101','Nguyễn Anh Tuấn','14-12-1997','CNTT2-4','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học','anhtuantoan1215@gmail.com','0372048460','ninh xá, thuận thành, bắc ninh'),(2,'20150672','Mai Tiến Dũng ','10-06-1997','CNTT2-4 ','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học','maitiendung@gmail.com','01672048460','bắc ninh'),(3,'20151101','Nguyễn Văn An','12-10-1997','CNTT2-4 ','CT Nhóm ngành CNTT-TT 2-2015','Đại học','Học','an@gmail.com',NULL,NULL);
/*!40000 ALTER TABLE `sinhvien` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sinhvien_lophoc`
--

DROP TABLE IF EXISTS `sinhvien_lophoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sinhvien_lophoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `idlophoc` int(11) NOT NULL,
  `diemqt` float DEFAULT NULL,
  `diemthi` float DEFAULT NULL,
  `diemchu` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fksl1_idx` (`taikhoan`),
  KEY `fksl2_idx` (`idlophoc`),
  CONSTRAINT `fksl1` FOREIGN KEY (`taikhoan`) REFERENCES `user` (`taikhoan`),
  CONSTRAINT `fksl2` FOREIGN KEY (`idlophoc`) REFERENCES `lophoc` (`idlophoc`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sinhvien_lophoc`
--

LOCK TABLES `sinhvien_lophoc` WRITE;
/*!40000 ALTER TABLE `sinhvien_lophoc` DISABLE KEYS */;
INSERT INTO `sinhvien_lophoc` VALUES (1,'20154101',1,8,9,'A'),(2,'20154101',2,NULL,NULL,NULL),(3,'20154101',3,NULL,NULL,NULL),(4,'20154101',4,NULL,NULL,NULL),(5,'20154101',5,NULL,NULL,NULL),(6,'20150672',1,8,7,'B'),(7,'20151101',1,NULL,NULL,NULL);
/*!40000 ALTER TABLE `sinhvien_lophoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `thongtinkihoc`
--

DROP TABLE IF EXISTS `thongtinkihoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thongtinkihoc` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kihoc` int(11) NOT NULL,
  `hocphi` bigint(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kihoc_UNIQUE` (`kihoc`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `thongtinkihoc`
--

LOCK TABLES `thongtinkihoc` WRITE;
/*!40000 ALTER TABLE `thongtinkihoc` DISABLE KEYS */;
INSERT INTO `thongtinkihoc` VALUES (1,20171,205000),(2,20172,215000),(3,20181,230000),(5,20182,235000);
/*!40000 ALTER TABLE `thongtinkihoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taikhoan` varchar(45) NOT NULL,
  `matkhau` varchar(45) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`,`taikhoan`),
  UNIQUE KEY `taikhoan_UNIQUE` (`taikhoan`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','25d55ad283aa400af464c76d713c07ad',1),(2,'20154101','25d55ad283aa400af464c76d713c07ad',2),(3,'20150672','25d55ad283aa400af464c76d713c07ad',2),(4,'20151101','25d55ad283aa400af464c76d713c07ad',2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-11  9:16:00
