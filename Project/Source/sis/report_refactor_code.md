### Báo cáo áp dụng Design Principles và Design Patterns
#### 1. Áp dụng Design Principles
-	Áp dụng nguyên lý Open/Close nhóm em tách 3 màn hình giao diện của Khách, Admin, Sinh viên thành 3 class kế thừa abstract class ViewRoleInfo. MainUI sẽ dựa vào Role để quyết định sử dụng class nào. Cách làm này có ích lợi khi nào thêm role mới, view mới thì chỉ cần thêm class kế thừa ViewRoleInfo.

- Áp dụng The Single Responsibility Principle, các class đều chứa một hoặc ít trách nhiệm. Các class Boundary chỉ hiện thị view. Các class Controller chỉ xử lý logic. Các class Entity chỉ kết nối cơ sở dữ liệu. Các class dùng chung được xử dụng để xử lý logic trong Controller thì viết ở Common.

#### 2. Áp dụng Design Pattern
-	Trong bài nhóm em áp dụng Singleton Pattern vào các lớp Controller và MainUI. 
Lớp Controller là lớp chỉ chứa các phương thức xử lý logic nên các lớp Boundary gọi đến nó thì chỉ cần một đối tượng.
Tương tự như thế MainUI là khung chung của ứng dụng được sử dụng xuyên suốt từ lúc bắt đầu ứng dụng đến cuối ứng dụng.
