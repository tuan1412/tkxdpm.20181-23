﻿###Home work: Project

###Assignment of each member
Nguyễn Anh Tuấn

- Lập trình uc Đăng nhập, Tra cứu học phí, Cập nhật điểm sinh viên: 100%

Phạm Văn Tuấn

- Lập trình uc Xem thời khóa biểu, Tra cứu điểm Toeic, Cập nhật thông tin cá nhân người dùng: 100%

Nguyễn Hữu Tùng

- Lập trình uc Đổi mật khẩu, Tra cứu bảng điểm cá nhân, Đăng kí học phần: 100%

Làm việc nhóm
- Refactor code áp dụng Design Patterns và Design Principles.

- Thiết kế cơ sở dữ liệu

- Xây dựng SRS, SDD, slide, video demo.


###Review
Nguyễn Anh Tuấn review Nguyễn Hữu Tùng

Nguyễn Hữu Tùng review Phạm Văn Tuấn

Phạm Văn Tuấn review Nguyễn Anh Tuấn


###Test
Nguyễn Anh Tuấn test Usecase Đăng nhập, Usecase Cập nhật điểm sinh viên

Nguyễn Hữu Tùng test Usce Đăng ký học phần

Phạm Văn Tuấn test Usecase Cập nhật thông tin cá nhân