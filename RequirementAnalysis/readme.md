### Home work: RequirementAnalysis
### Assignment  of  each  member
Nguyễn Anh Tuấn - 20154101

- Mô tả tổng quan, các tác nhân, biểu đồ use case tổng quan, biểu đồ usecase phân rã: 100%

- Quy trình cập nhật thông tin người dùng của quản trị viên: 100%

- Đặc tả usecase: 100%
    + Đăng nhập

    + Xem thời khóa biểu

    + Đăng xuất

    + Xem thông tin cá nhân

    + Đăng kí học phần tự chọn tự do

    + Tra cứu học phí

    + Xem bảng điểm học phần

    + Xem hướng dẫn đăng kí

    + Xem thông tin người dùng
    
    + Cập nhật kết quả học tập người dùng 
    

Nguyễn Hữu Tùng – 20154885

- Quy trình sử dụng phần mềm:100%

- Đặc tả các use-case:100%

    + Xem danh mục học phần

    + Tra cứu lớp sinh viên

    + Đổi mật khẩu

    + Đăng ký học phần

    + Xem kết quả xét nhận đồ án tốt nghiệp

    + Xem bảng điểm cá nhân

    + Góp ý về trang web

    + Tìm kiếm người dùng

    + Thay đổi lịch học người dùng
- Các yêu cầu khác:100%

Phạm Văn Tuấn - 20154136

- Giới thiệu tài liệu SRS: Mục đích, Phạm vi, Từ điển thuật ngữ, Tài liệu tham khảo : 100%

- Quy trình quản lý học phần của quản trị viên : 100%

- Đặc tả usecase : 100%

    + Xem tin tức từ trang chủ
    
    + Tra cứu sinh viên đăng ký học tập
    
    + Xem chương trình đào tạo
    
    + Cập nhật thông tin cá nhân
    
    + Đăng kí tốt nghiệp
    
    + Đăng kí phân ngành
    
    + Tra cứu kết quả phân ngành
    
    + Tra cứu điểm thi Toeic
    
    + CRUD người dùng