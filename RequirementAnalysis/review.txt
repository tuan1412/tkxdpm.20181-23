﻿Phạm Văn Tuấn review Nguyễn Anh Tuấn
- Mô tả tổng quan, các tác nhân, biểu đồ use case tổng quan, biểu đồ usecase phân rã:
+  Đã thực hiện việc mô tả tổng quan hệ thống, vẽ các biểu đồ usecase đầy đủ
- Quy trình cập nhật thông tin người dùng của quản trị viên:
+ Thực hiện đầy đủ yêu cầu
- Đặc tả use case:
+ Đăng nhập: Luồng sự kiện thay thế 4a,5a -> thiếu form mô tả giao diện đăng nhập ( tên đăng nhập, mật khẩu, mã capcha)
+ Xem thời khóa biểu: Luồng sự kiện thay thế ->  thiếu trường hợp: hệ thống hiển thị thông báo lỗi 'bạn chưa nhập mã sinh viên'
+ Đăng xuất: thực hiện đủ yêu cầu
+ Xem thông tin cá nhân: thực hiện đầy đủ yêu cầu
+ Đăng kí học phần tự chọn tự do: thiếu mô tả chi tiết'bảng các học phần đăng ký' khi hệ thống hiển thị học phần (STT 6)
+ Tra cứu học phí: thiếu mô tả chi tiết 'bảng học phí' ( STT 3)
+ Xem bảng điểm học phần: thực hiện đầy đủ yêu cầu
+ Xem hướng dẫn đăng kí: thực hiện đầy đủ yêu cầu
+ Xem thông tin người dùng: thực hiện đầy đủ yêu cầu
+ Cập nhật kết quả học tập người dùng: thực hiện đầy đủ yêu cầu
=======
﻿Nguyễn Anh Tuấn review Nguyễn Hữu Tùng
- Quy trình sử dụng phần mềm: thực hiện đầy đủ yêu cầu
- Đặc tả usecase:
+ Các đặc tả usecase có yêu cầu đăng nhập thay ghi vì người dùng đăng nhập thành công thì ghi khách đăng nhập thành công vì người dùng tức là đã đăng nhập
+ Các usecase khác hoàn thiện đầy đủ yêu cầu
- Các chức năng khác
+ Các phần chú thích của cô cần xóa đi
+ Yêu cầu phi chắc chức năng cần thêm tính tin cậy, như nhập điểm của sinh viên cần tính ra GPA, CPA chuẩn xác, xét điều kiện tốt nghiệp chuẩn xác, kiểm tra việc đăng kí học phần đúng
=======
﻿Nguyễn Hữu Tùng review Phạm Văn Tuấn
-Quy trình sử dụng phần mềm: thực hiện đầy đủ các yêu cầu
-Phần giới thiêu: 
+cơ bản thực hiện đầy đủ các mục của phần giới thiệu
+Sửa lại 1 số lỗi chính tả trong mục 1.2:Phạm vi
-Đặc tả use-case: 
+Tra cứu SV đăng ký học tập
+Xem chương trình đào tạo
+Xem tin tức trang chủ
+Cập nhật thông tin cá nhân
+Tra cứu kết quả phân ngành
+Đăng ký tốt nghiệp
+Đăng ký phân ngành
+Tra cứu điểm thi TOEIC
+CRUD người dùng: cần xóa đi chú thích của cô
Các use-case đã được đặc tả đầy đủ các yêu cầu.