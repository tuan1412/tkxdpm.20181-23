### Home work: DetailDesign
### Assignment  of  each  member
Nguyễn Anh Tuấn - 20154101
- Thiét kế giao diện đăng nhập, tra cứu học phí, cập nhật điểm người dùng: 100%

- Thiết kế chi tiết lớp các usecase tương ứng: 100%

Nguyễn Hữu Tùng - 20154485

-Thiết kế chi tiết các lớp usecase đổi mật khẩu, xem bảng điểm cá nhân, đăng ký học phần: 100%

-Thiết kế giao diện các chức năng đổi mật khẩu, xem bảng điểm cá nhân, đăng ký học phần: 100%
 
Phạm Văn Tuấn - 20154136

- Thiết kế chi tiết lớp và giao diện các usecase: cập nhật thông tin cá nhân, tra cứu điểm toeic, tra cứu thời khóa biểu: 100%

Làm theo nhóm

- Ghép thiết kế tổng quan, thiết kế chi tiết gói

- Biểu đồ thực thể liên kết và bảng quan hệ

- Mô tả các bảng

### Review
Nguyễn Anh Tuấn review Nguyễn Hữu Tùng

Nguyễn Hữu Tùng review Phạm Văn Tuấn

Phạm Văn Tuấn review Nguyễn Anh Tuấn
