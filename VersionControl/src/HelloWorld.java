import java.util.Scanner;

public class HelloWorld {
	int day, month, year;
	String name;
	public void inputPerson() {
		Scanner cs = new Scanner(System.in);
		System.out.println("Name: ");
		name = cs.nextLine();

		do {
			System.out.println("Day of birthday: ");
			day = cs.nextInt();
			cs.nextLine();
			System.out.println("Month of birthday: ");
			month = cs.nextInt();
			cs.nextLine();
			System.out.println("Year of birthday: ");
			year = cs.nextInt();
			cs.nextLine();
			if (!DateUtil.checkDate(day, month, year)) {
				System.out.println("Please re-enter your birthday: ");
			}
		} while (!DateUtil.checkDate(day, month, year));
	}
}
