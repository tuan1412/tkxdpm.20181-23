import java.util.*;

public class DateUtil {
	public static boolean checkDate(int day, int month, int year) {
		int days[] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		if (year <= 1900 || year > 9999) {
			return false;
		}
		if (month < 0 || month > 12) {
			return false;
		}
		// check leap year
		boolean isLeapYear = ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
		if (isLeapYear) {
			days[1]++;
		}
		if (day > 0 && day <= days[month-1]) {
			return true;
		}
		return false;
	}
	public static int  calculateAge(int year) {
		int year_now = Calendar.getInstance().get(Calendar.YEAR);
		return (year_now - year) ;
	}

}
