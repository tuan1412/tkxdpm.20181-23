import java.lang.Math;

public class NumberUtil {
	public static boolean checkSquareNumber(int number) {
		if (number < 0) {
			return false;
		}
		double sqrt = Math.sqrt(number);
		int roundSqrt = (int) sqrt;
		return Math.pow(sqrt, 2) == Math.pow(roundSqrt, 2);
	}

	public static boolean checkIsPrimeNumber(int n) {
		if (n < 2)
			return false;
		for (int i = 2; i <= n - 1; i++) {
			if (n % i == 0)
				return false;
		}
		return true;
	}

	public static boolean checkPefectNumber(int number) {
		int sum = 0;
		int temp = number;
		for (int i = 1; i < temp; i++) {
			if (temp % i == 0)
				sum = sum + i;
		}

		if (sum == temp)
			return true;
		else
			return false;
	}
}
