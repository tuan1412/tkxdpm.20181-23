### Home work: Version control
### Assignment  of  each  member
Nguyễn Anh Tuấn - 20154101
- Viết hàm checkDate trong DateUtil: 100%
  
- Viết hàm checkSqureNumber trong NumberUtil: 100%;
  
Phạm Văn Tuấn - 20154136

- Viết hàm calculateAge trong DataUtil: 100%
  
- Viết hàm check  if  an  integer  is  a  perfect  number trong NumberUtil: 100%
  
- Viết Check dữ liệu đầu vào, kiểm tra là số nguyên và hiển thị kết quả  (là số nguyên tố hay hợp số) 	FirstNumberApp: 100%
  
Nguyễn Hữu Tùng - 20154485

- Viết hàm checkIsPrimeNumber trong NumberUtil: 100%
  
- Viết hàm inputPerson trong HelloWorld: 100%

- Viết display if the number is a square number: 100%

### Review
Nguyễn Anh Tuấn review Nguyễn Hữu Tùng

- Viết hàm checkIsPrimeNumber trong NumberUtil
	
- Viết hàm inputPerson trong HelloWorld

Nguyễn Hữu Tùng review Phạm Văn Tuấn
- Viết hàm calculateAge trong DateUtil

- Viết hàm check if an  integer  is  a  perfect  number trong NumberUtil

- Viết Check dữ liệu đầu vào, kiểm tra là số nguyên và hiển thị kết quả  (là số nguyên tố hay hợp số)

Phạm Văn Tuấn review Nguyễn Anh Tuấn

- Viết hàm checkDate trong DataUtil

- Viết hàm check  if  an  integer  is  a  square  number trong NumberUtil