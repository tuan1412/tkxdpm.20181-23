package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.UserController;
import entity.User;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;

@SuppressWarnings("serial")
public class LoginForm extends JFrame {

	private JPanel contentPane;
	private JTextField tfTaiKhoan;
	private JPasswordField pfMatKhau;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginForm frame = new LoginForm();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public LoginForm() {
		initComponents();

	}

	private void initComponents() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblDangNhap = new JLabel("Đăng nhập");
		lblDangNhap.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblDangNhap.setBounds(155, 37, 112, 33);
		contentPane.add(lblDangNhap);
		
		JLabel lblTaiKhoan = new JLabel("Tài khoản");
		lblTaiKhoan.setBounds(60, 85, 67, 21);
		contentPane.add(lblTaiKhoan);
		
		JLabel lblMatKhau = new JLabel("Mật khẩu");
		lblMatKhau.setBounds(60, 131, 67, 14);
		contentPane.add(lblMatKhau);
		
		tfTaiKhoan = new JTextField();
		tfTaiKhoan.setBounds(137, 85, 190, 20);
		contentPane.add(tfTaiKhoan);
		tfTaiKhoan.setColumns(10);
		
		JButton btnDangNhap = new JButton("Đăng nhập");
		btnDangNhap.setBounds(137, 172, 112, 23);
		contentPane.add(btnDangNhap);
		
		pfMatKhau = new JPasswordField();
		pfMatKhau.setBounds(137, 128, 190, 20);
		contentPane.add(pfMatKhau);
		
		btnDangNhap.addActionListener(e -> {
				doLogin();
		});
		
		pfMatKhau.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					doLogin();	
				}
			}
		});
		
	}

	private void doLogin(){
		String taikhoan = tfTaiKhoan.getText();
		String password = new String(new String(pfMatKhau.getPassword()));
		boolean flag = true;
		if (taikhoan.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Tài khoản không được để trống.");
			flag = false;
		}
		if (flag && password.isEmpty()) {
			JOptionPane.showMessageDialog(this, "Password không được để trống.");
			flag = false;
		}
		if (flag) {
			User user = null;
			try {
				user = UserController.getInstance().doLogin(taikhoan, password);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			if (user == null) {
				JOptionPane.showMessageDialog(this, "Tài khoản hoặc mật khẩu không đúng");
			} else {
				JOptionPane.showMessageDialog(this, "User " + user.getFullname() + " đăng nhập thành công");
			}
		}
	}
}
