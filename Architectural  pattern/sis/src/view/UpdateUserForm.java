package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import common.ConnectionHelper;
import controller.UpdateUserController;
import controller.UserController;
import entity.Toeic;
import entity.UserUpdate;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.util.List;

import javax.swing.JTextField;
import javax.swing.Box;
import java.awt.Color;

public class UpdateUserForm extends JFrame implements ActionListener {

	public JPanel contentPane;
	public JTextField textFieldEmail,textFieldSoDT,textFieldDiaChi;
	public JLabel lbTTTK,lbMaSV,lbHoTen,lbNgaySinh,lbLop,lbChuongTrinh,lbHeHoc,lbTrangThai,lbEmail,lbSoDT,lbDiaChi,
					lbcontentMaSV,lbcontentHoTen,lbcontentNgaySinh,lbcontentLop,lbcontentChuonTrinh,lbcontentHeHoc,lbcontentTrangThai,
					lblerrorEmail,lblerrorSoDT,lblerrorDiaChi,	lbDisplaySuccess;
	public JButton btnCapNhatTT;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UpdateUserForm frame = new UpdateUserForm();
					frame.setVisible(true);
					frame.setTitle("Thông tin cá nhân");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public UpdateUserForm() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 860, 516);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		lbTTTK = new JLabel("Thông tin tài khoản");
		lbTTTK.setFont(new Font("Tahoma", Font.BOLD, 15));
		lbTTTK.setBounds(37, 11, 174, 23);
		contentPane.add(lbTTTK);
		
		lbMaSV = new JLabel("Mã sinh viên:");
		lbMaSV.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbMaSV.setBounds(60, 93, 99, 14);
		contentPane.add(lbMaSV);
		
		lbHoTen = new JLabel("Họ tên SV:");
		lbHoTen.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbHoTen.setBounds(60, 144, 66, 14);
		contentPane.add(lbHoTen);
		
		lbNgaySinh = new JLabel("Ngày sinh:");
		lbNgaySinh.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbNgaySinh.setBounds(60, 195, 66, 14);
		contentPane.add(lbNgaySinh);
		
		lbLop = new JLabel("Lớp:");
		lbLop.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbLop.setBounds(60, 246, 46, 14);
		contentPane.add(lbLop);
		
		lbChuongTrinh = new JLabel("Chương trình:");
		lbChuongTrinh.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbChuongTrinh.setBounds(60, 297, 89, 14);
		contentPane.add(lbChuongTrinh);
		
		lbHeHoc = new JLabel("Hệ học:");
		lbHeHoc.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbHeHoc.setBounds(60, 348, 46, 14);
		contentPane.add(lbHeHoc);
		
		lbTrangThai = new JLabel("Trạng thái:");
		lbTrangThai.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbTrangThai.setBounds(60, 399, 73, 14);
		contentPane.add(lbTrangThai);
		
		lbEmail = new JLabel("Email*:");
		lbEmail.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbEmail.setBounds(411, 93, 46, 14);
		contentPane.add(lbEmail);
		
		lbSoDT = new JLabel("Số điện thoại*:");
		lbSoDT.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbSoDT.setBounds(411, 144, 89, 14);
		contentPane.add(lbSoDT);
		
		lbDiaChi = new JLabel("Địa chỉ*:");
		lbDiaChi.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbDiaChi.setBounds(411, 195, 46, 14);
		contentPane.add(lbDiaChi);
		
		btnCapNhatTT = new JButton("Cập nhật thông tin");
		btnCapNhatTT.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnCapNhatTT.setBounds(510, 262, 159, 23);
		btnCapNhatTT.addActionListener(this);
		contentPane.add(btnCapNhatTT);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setBounds(510, 91, 296, 20);
		contentPane.add(textFieldEmail);
		textFieldEmail.setColumns(10);
		
		textFieldSoDT = new JTextField();
		textFieldSoDT.setBounds(510, 142, 296, 20);
		contentPane.add(textFieldSoDT);
		textFieldSoDT.setColumns(10);
		
		textFieldDiaChi = new JTextField();
		textFieldDiaChi.setBounds(513, 193, 293, 20);
		contentPane.add(textFieldDiaChi);
		textFieldDiaChi.setColumns(10);
		
		lbcontentMaSV = new JLabel("");
		lbcontentMaSV.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbcontentMaSV.setBounds(149, 94, 200, 14);
		contentPane.add(lbcontentMaSV);
		
		lbcontentHoTen = new JLabel("");
		lbcontentHoTen.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbcontentHoTen.setBounds(149, 144, 200, 14);
		contentPane.add(lbcontentHoTen);
		
		lbcontentNgaySinh = new JLabel("");
		lbcontentNgaySinh.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbcontentNgaySinh.setBounds(149, 195, 200, 14);
		contentPane.add(lbcontentNgaySinh);
		
		lbcontentLop = new JLabel("");
		lbcontentLop.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbcontentLop.setBounds(149, 246, 200, 14);
		contentPane.add(lbcontentLop);
		
		lbcontentChuonTrinh = new JLabel("");
		lbcontentChuonTrinh.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbcontentChuonTrinh.setBounds(149, 297, 190, 14);
		contentPane.add(lbcontentChuonTrinh);
		
		lbcontentHeHoc = new JLabel("");
		lbcontentHeHoc.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbcontentHeHoc.setBounds(149, 348, 200, 14);
		contentPane.add(lbcontentHeHoc);
		
		lbcontentTrangThai = new JLabel("");
		lbcontentTrangThai.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lbcontentTrangThai.setBounds(153, 399, 196, 14);
		contentPane.add(lbcontentTrangThai);
		
		lblerrorEmail = new JLabel("");
		lblerrorEmail.setForeground(Color.RED);
		lblerrorEmail.setBounds(510, 117, 296, 14);
		contentPane.add(lblerrorEmail);
		
		lblerrorSoDT = new JLabel("");
		lblerrorSoDT.setForeground(Color.RED);
		lblerrorSoDT.setBounds(510, 168, 296, 14);
		contentPane.add(lblerrorSoDT);
		
		lblerrorDiaChi = new JLabel("");
		lblerrorDiaChi.setForeground(Color.RED);
		lblerrorDiaChi.setBounds(510, 226, 283, 14);
		contentPane.add(lblerrorDiaChi);
		
		lbDisplaySuccess = new JLabel("");
		lbDisplaySuccess.setForeground(Color.RED);
		lbDisplaySuccess.setBounds(37, 45, 302, 14);
		contentPane.add(lbDisplaySuccess);
		setData();
	}

public void setData() {
	String taikhoan = "20154136";
	
	Connection conn = ConnectionHelper.connectDb();
	UpdateUserController getlist = new UpdateUserController();
	
	List<UserUpdate> list = getlist.getListUser(taikhoan, conn);
	// set text for each label
	
	lbcontentMaSV.setText(String.valueOf(list.get(0).getMssv()));
	lbcontentHoTen.setText(list.get(0).getHoten());
	lbcontentNgaySinh.setText(list.get(0).getNgaysinh());
	lbcontentLop.setText(list.get(0).getLop());
	lbcontentChuonTrinh.setText(list.get(0).getChuongtrinh());
	lbcontentHeHoc.setText(list.get(0).getHehoc());
	lbcontentTrangThai.setText(list.get(0).getTrangthai());
	
	textFieldEmail.setText(list.get(0).getEmail());
	
	if(list.get(0).getSdt()==0) textFieldSoDT.setText("");
	else textFieldSoDT.setText(String.valueOf(list.get(0).getSdt()));
	
	textFieldDiaChi.setText(list.get(0).getDiachi());
	
	
}
public void actionPerformed(ActionEvent e) {
		
        if (e.getSource() == btnCapNhatTT) {
        	
        	String taikhoan = "20154136";
        	String email = "";
        	String diachi = "";
        	int sdt = 0;
        	int error = 0;
        	
        	lblerrorDiaChi.setText("");
        	lblerrorSoDT.setText("");
        	lblerrorEmail.setText("");
        	lbDisplaySuccess.setText("");
        	
        	//validate input data
        	if(textFieldEmail.getText().equals("")) 
        		{lblerrorEmail.setText("Email không được để trống"); error =1;}
        	if(textFieldDiaChi.getText().equals(""))
        		{lblerrorDiaChi.setText("Địa chỉ không được để trống"); error = 1;}
        	if(textFieldSoDT.getText().equals(""))
        		{lblerrorSoDT.setText("Số điện thoại không được để trống"); error = 1;}
        	//
        	
        	
        	if(error==0) {
        	email = textFieldEmail.getText();
        	sdt = Integer.parseInt(textFieldSoDT.getText());
        	diachi = textFieldDiaChi.getText();
        	
        	Connection conn = ConnectionHelper.connectDb();
        	UpdateUserController update = new UpdateUserController();
        	boolean kt = update.updateUser(email, sdt, diachi, taikhoan, conn);
	        	if(kt) {
	        		lbDisplaySuccess.setText("Cập nhật thông tin thành công");
	        		JOptionPane.showMessageDialog(null, "Thành công");

	        	}
	        	
        }
        	
        else {
        
        		lbDisplaySuccess.setText("Cập nhật không thành công");
        	
        }
        
        }
}

}

