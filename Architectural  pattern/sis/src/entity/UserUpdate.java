package entity;

import java.sql.*;
import java.util.*;


public class UserUpdate {

private int mssv,sdt;
private String taikhoan,password,hoten,gioitinh,ngaysinh,lop,diachi,chuongtrinh,
				hehoc,trangthai,role,email;





public  List<UserUpdate> searchUser(String taikhoan,Connection conn){
	
	List<UserUpdate> list = new ArrayList<UserUpdate>();
	String sql="select * from user where taikhoan='"+taikhoan+"'";
	
	try 
	{
		System.out.println(sql);
		PreparedStatement ptmt = conn.prepareStatement(sql);

		ResultSet rs = ptmt.executeQuery();
		
		while (rs.next())
		{
			UserUpdate user = new UserUpdate();
			
			int mssv = rs.getInt("mssv");
			int sdt = rs.getInt("sdt");
			String hoten = rs.getString("hoten");
			String lop = rs.getString("lop");
			String diachi = rs.getString("diachi");
			String chuongtrinh = rs.getString("chuongtrinh");
			String hehoc = rs.getString("hehoc");
			String ngaysinh = rs.getString("ngaysinh");
			String email = rs.getString("email");
			String trangthai = rs.getString("trangthai");
			
			user.setMssv(mssv);
			user.setSdt(sdt);
			user.setHoten(hoten);
			user.setLop(lop);
			user.setDiachi(diachi);
			user.setChuongtrinh(chuongtrinh);
			user.setHehoc(hehoc);
			user.setNgaysinh(ngaysinh);
			user.setEmail(email);
			user.setTrangthai(trangthai);
			
			
			list.add(user);
			
		}
			
	} 
	catch (SQLException e) 
	{
		System.out.println(sql);
		System.out.println("Error search User: "+e);
	}
	
	return list;
}

public boolean UpdateUser(String email,int sdt,String diachi,String taikhoan,Connection conn) {
	String sql = "update he_thong_sis.user set email='"+email+"' , sdt ="+sdt+" , diachi='"+diachi+"'"
			+ " where taikhoan = '"+taikhoan+"'";
	try {
		
		PreparedStatement ptmt = conn.prepareStatement(sql);

		int result =  ptmt.executeUpdate();
		 System.out.println("sql = "+sql);
		 if(result > 0) return true;
		
	} catch (SQLException e) {
		System.out.println("String sql = "+sql);
		System.out.println("Error updateUser: "+e);
	}
	return false;
	
}


public int getMssv() {
	return mssv;
}
public void setMssv(int mssv) {
	this.mssv = mssv;
}
public int getSdt() {
	return sdt;
}
public void setSdt(int sdt) {
	this.sdt = sdt;
}
public String getTaikhoan() {
	return taikhoan;
}
public void setTaikhoan(String taikhoan) {
	this.taikhoan = taikhoan;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getHoten() {
	return hoten;
}
public void setHoten(String hoten) {
	this.hoten = hoten;
}
public String getGioitinh() {
	return gioitinh;
}
public void setGioitinh(String gioitinh) {
	this.gioitinh = gioitinh;
}
public String getNgaysinh() {
	return ngaysinh;
}
public void setNgaysinh(String ngaysinh) {
	this.ngaysinh = ngaysinh;
}
public String getLop() {
	return lop;
}
public void setLop(String lop) {
	this.lop = lop;
}
public String getDiachi() {
	return diachi;
}
public void setDiachi(String diachi) {
	this.diachi = diachi;
}
public String getChuongtrinh() {
	return chuongtrinh;
}
public void setChuongtrinh(String chuongtrinh) {
	this.chuongtrinh = chuongtrinh;
}
public String getHehoc() {
	return hehoc;
}
public void setHehoc(String hehoc) {
	this.hehoc = hehoc;
}
public String getTrangthai() {
	return trangthai;
}
public void setTrangthai(String trangthai) {
	this.trangthai = trangthai;
}
public String getRole() {
	return role;
}
public void setRole(String role) {
	this.role = role;
}
public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

	
}
