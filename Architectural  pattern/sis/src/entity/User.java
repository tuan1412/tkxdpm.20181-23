package entity;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

import common.ConnectionHelper;

public class User {
	private String maso;
	private String fullname;
	private String taikhoan;
	private String password;
	private String gioitinh;
	private String sdt;
	private String email;
	private LocalDate ngaysinh;
	private String role;

	private final String SEARCH_LOGIN = "SELECT * FROM User WHERE taikhoan=? AND password=?";
	private final String UPDATE_PASS = "update User set password = ? where taikhoan = ?";

	public User doLogin(String taikhoan, String password) throws SQLException {
		Connection conn = ConnectionHelper.connectDb();
		PreparedStatement ps = conn.prepareStatement(SEARCH_LOGIN);
		ps.setString(1, taikhoan);
		ps.setString(2, password);
		ResultSet rs = ps.executeQuery();
		if (rs != null && rs.next()) {
			String fullname = rs.getString("fullname");
			String ms = rs.getString("ms");
			String role = rs.getString("role");
			User user = new User();
			user.setFullname(fullname);
			user.setMaso(ms);
			user.setRole(role);
			ConnectionHelper.connectDb();
			return user;
		}
		ConnectionHelper.connectDb();
		return null;
			
	}

	
	public void updatePassword(String taikhoan,String password) throws SQLException, ClassNotFoundException{
		Connection conn = ConnectionHelper.connectDb();
        PreparedStatement ps = conn.prepareStatement(UPDATE_PASS);
        ps.setString(1, taikhoan);
        ps.setString(2, password);
        ps.executeUpdate();
//        closeDB();
    }

	public String getMaso() {
		return maso;
	}

	public void setMaso(String maso) {
		this.maso = maso;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getTaikhoan() {
		return taikhoan;
	}

	public void setTaikhoan(String taikhoan) {
		this.taikhoan = taikhoan;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGioitinh() {
		return gioitinh;
	}

	public void setGioitinh(String gioitinh) {
		this.gioitinh = gioitinh;
	}

	public String getSdt() {
		return sdt;
	}

	public void setSdt(String sdt) {
		this.sdt = sdt;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDate getNgaysinh() {
		return ngaysinh;
	}

	public void setNgaysinh(LocalDate ngaysinh) {
		this.ngaysinh = ngaysinh;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
