package controller;

import java.sql.SQLException;

import entity.User;

public class UserController {
	User user = new User();

	private static UserController intance;

	private UserController() {
	}

	public static UserController getInstance() {
		if (intance == null)
			intance = new UserController();
		return intance;
	}
	
	public User doLogin(String taikhoan, String password) throws SQLException {
		return user.doLogin(taikhoan, password);
	}
	

	public void  changePasswordByRequest(String taikhoan,String newPassword) throws SQLException, ClassNotFoundException{
        User user = new User();
        user.updatePassword(taikhoan, newPassword);
    }
}
