package controller;

import java.sql.Connection;
import java.util.List;

import common.ConnectionHelper;
import entity.UserUpdate;


public class UpdateUserController {
	
	public List<UserUpdate> getListUser(String taikhoan,Connection conn){
			UserUpdate  user = new UserUpdate();
		return user.searchUser(taikhoan, conn);
	}
	
	
	public boolean updateUser(String email,int sdt,String diachi,String taikhoan,Connection conn) {
		UserUpdate user = new UserUpdate();
		return user.UpdateUser(email, sdt, diachi, taikhoan, conn);
	}
}
