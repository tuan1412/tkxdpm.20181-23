package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionHelper {
	private static String dbName = "he_thong_sis";
	private static String url = "localhost:3306";
	private static String username = "root";
	private static String password = "";
	private static String dbPath = "jdbc:mysql://" + url + "/" + dbName;
	private static Connection conn = null;
	
	public static Connection connectDb() throws SQLException {
		conn = DriverManager.getConnection(dbPath, username, password);
		return conn;
	}
	
	public void closeDB() throws SQLException {
        if (conn != null)
        	conn.close();
    }
}

