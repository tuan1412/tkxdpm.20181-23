-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: he_thong_sis
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bangdiem`
--

DROP TABLE IF EXISTS `bangdiem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bangdiem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSinhVien` int(11) NOT NULL,
  `hocki` int(11) NOT NULL,
  `lophoc` varchar(45) NOT NULL,
  `diemQT` float NOT NULL,
  `diemThi` float NOT NULL,
  `diemChu` varchar(1) NOT NULL,
  `idHocPhan` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk1_idx` (`idSinhVien`),
  KEY `fk2_idx` (`idHocPhan`),
  CONSTRAINT `fk1` FOREIGN KEY (`idSinhVien`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk2` FOREIGN KEY (`idHocPhan`) REFERENCES `hocphan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bangdiem`
--

LOCK TABLES `bangdiem` WRITE;
/*!40000 ALTER TABLE `bangdiem` DISABLE KEYS */;
/*!40000 ALTER TABLE `bangdiem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bangdiemtoeic`
--

DROP TABLE IF EXISTS `bangdiemtoeic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bangdiemtoeic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idSinhVien` int(11) NOT NULL,
  `hocKy` int(11) NOT NULL,
  `ngayThi` datetime NOT NULL,
  `diemNghe` int(11) NOT NULL,
  `diemDoc` int(11) NOT NULL,
  `diemTong` int(11) NOT NULL,
  `ghiChu` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk5_idx` (`idSinhVien`),
  CONSTRAINT `fk5` FOREIGN KEY (`idSinhVien`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bangdiemtoeic`
--

LOCK TABLES `bangdiemtoeic` WRITE;
/*!40000 ALTER TABLE `bangdiemtoeic` DISABLE KEYS */;
/*!40000 ALTER TABLE `bangdiemtoeic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hocphan`
--

DROP TABLE IF EXISTS `hocphan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hocphan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maHp` varchar(45) NOT NULL,
  `tenHp` varchar(45) NOT NULL,
  `soTinChi` int(11) NOT NULL,
  `tinChiHocPhi` float NOT NULL,
  `thoiLuong` varchar(45) NOT NULL,
  `trongSo` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hocphan`
--

LOCK TABLES `hocphan` WRITE;
/*!40000 ALTER TABLE `hocphan` DISABLE KEYS */;
/*!40000 ALTER TABLE `hocphan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lichhoc`
--

DROP TABLE IF EXISTS `lichhoc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lichhoc` (
  `idSinhVien` int(11) NOT NULL,
  `kihoc` int(11) NOT NULL,
  `malop` varchar(45) NOT NULL,
  `nhom` varchar(45) NOT NULL,
  `idHocPhan` int(11) NOT NULL,
  `thoiGian` varchar(45) NOT NULL,
  `phongHoc` varchar(45) NOT NULL,
  KEY `fk3_idx` (`idSinhVien`),
  KEY `fk4_idx` (`idHocPhan`),
  CONSTRAINT `fk3` FOREIGN KEY (`idSinhVien`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk4` FOREIGN KEY (`idHocPhan`) REFERENCES `hocphan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lichhoc`
--

LOCK TABLES `lichhoc` WRITE;
/*!40000 ALTER TABLE `lichhoc` DISABLE KEYS */;
/*!40000 ALTER TABLE `lichhoc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ms` varchar(45) NOT NULL,
  `taikhoan` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `gioitinh` varchar(45) DEFAULT NULL,
  `noisinh` varchar(45) DEFAULT NULL,
  `sdt` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `ngaysinh` datetime DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `fullname` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taikhoan_UNIQUE` (`taikhoan`),
  UNIQUE KEY `ms_UNIQUE` (`ms`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-18 22:50:39
